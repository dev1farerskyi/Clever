<?php
/**
 * Single News template
 */

get_header();

$header_description = get_the_date();
?>

<?php get_template_part(
    'template-parts/blocks/simple-text-header',
    null,
    array(
        'style' => 'all-width',
        'subtitle' => 'News',
        'title' => get_the_title(),
        'description' => $header_description,
        'show_share_links' => true,
    )
); ?>

    <div class="cle-content-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-text">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php get_template_part(
    'template-parts/blocks/cpt-blocks-section',
    null,
    array(
        'style' => 'blog-events',
        'blue_background' => true,
        'decor_position' => 'top-left',
    )
); ?>

<?php get_template_part('template-parts/blocks/subscribe'); ?>


<?php
get_footer();