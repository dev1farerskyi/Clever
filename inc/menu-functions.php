<?php

function vil_acf_location_rules_types($choices) {
    $choices['Menu']['menu_level'] = 'Menu Depth';
    return $choices;
}
add_filter('acf/location/rule_types', 'vil_acf_location_rules_types');


function vil_acf_location_rule_values_level($choices) {
    $choices[0] = '0';
    $choices[1] = '1';
    $choices[2] = '2';
    $choices[3] = '3';
    $choices[4] = '4';

    return $choices;
}
add_filter('acf/location/rule_values/menu_level', 'vil_acf_location_rule_values_level');


function vil_acf_location_rule_match_level($match, $rule, $options, $field_group) {
    $current_screen = get_current_screen();
    if ( isset( $current_screen->base ) && $current_screen->base == 'nav-menus') {
        if ($rule['operator'] == "==") {
            $match = ($options['nav_menu_item_depth'] == $rule['value']);
        }
    }
    return $match;
}
add_filter('acf/location/rule_match/menu_level', 'vil_acf_location_rule_match_level', 10, 4);


class CLE_Walker_Nav_Menu extends Walker_Nav_Menu {
    public $tree_type = array( 'post_type', 'taxonomy', 'custom' );

    public $db_fields = array(
        'parent' => 'menu_item_parent',
        'id'     => 'db_id',
    );

    public function start_lvl( &$output, $depth = 0, $args = null ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat( $t, $depth );

        // Default class.
        $classes = array( 'sub-menu' );

        $class_names = implode( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $output .= "{$n}{$indent}<ul$class_names>{$n}";
    }

    public function end_lvl( &$output, $depth = 0, $args = null ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent  = str_repeat( $t, $depth );
        $output .= "$indent</ul>{$n}";
    }

    public function start_el( &$output, $data_object, $depth = 0, $args = null, $current_object_id = 0 ) {
        $menu_item = $data_object;

        $extra_left_space = get_field('extra_left_space', $menu_item);

        $submenu_style = get_field('submenu_style', $menu_item);

        // depth > 0
        $selected_menu_type = get_field('selected_menu_type', $menu_item);
        $resources = get_field('resources', $menu_item);

        // Sub menu lower level
        $icon = get_field('icon', $menu_item);
        $show_free = get_field('show_free', $menu_item);

        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
        } else {
            $t = "\t";
        }
        $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

        $classes   = empty( $menu_item->classes ) ? array() : (array) $menu_item->classes;
        $classes[] = 'menu-item-' . $menu_item->ID;
        if(!empty($extra_left_space)) $classes[] = 'menu-item-left-space';
        if(!empty($submenu_style)) $classes[] = 'menu-item_' . $submenu_style;
        if(!empty($selected_menu_type)) $classes[] = 'menu-item_' . $selected_menu_type;

        $args = apply_filters( 'nav_menu_item_args', $args, $menu_item, $depth );

        $class_names = implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $menu_item, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $menu_item->ID, $menu_item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        if (!empty($selected_menu_type) && $selected_menu_type === 'popular-resources') {
            $arr_posts = [];
            foreach ($resources as $pst) {
                $arr_posts[] = [
                    'title' => $pst->post_title,
                    'img' => wp_get_attachment_image_url(get_post_thumbnail_id($pst->ID)),
                    'url' => get_the_permalink($pst->ID)
                ];
            }

            $output .= $indent . "<li" . $id . $class_names . " data-posts='" . json_encode($arr_posts) . "'>";
        } else {
            $output .= $indent . '<li' . $id . $class_names . '>';
        }

        $atts           = array();
        $atts['title']  = ! empty( $menu_item->attr_title ) ? $menu_item->attr_title : '';
        $atts['target'] = ! empty( $menu_item->target ) ? $menu_item->target : '';
        if ( '_blank' === $menu_item->target && empty( $menu_item->xfn ) ) {
            $atts['rel'] = 'noopener';
        } else {
            $atts['rel'] = $menu_item->xfn;
        }
        if(!empty($menu_item->description) && $depth === 2) $atts['class'] = 'is-description';

        if ( ! empty( $menu_item->url ) ) {
            if ( get_privacy_policy_url() === $menu_item->url ) {
                $atts['rel'] = empty( $atts['rel'] ) ? 'privacy-policy' : $atts['rel'] . ' privacy-policy';
            }

            $atts['href'] = $menu_item->url;
        } else {
            $atts['href'] = '';
        }

        $atts['aria-current'] = $menu_item->current ? 'page' : '';

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $menu_item, $args, $depth );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( is_scalar( $value ) && '' !== $value && false !== $value ) {
                $value       = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $title = apply_filters( 'the_title', $menu_item->title, $menu_item->ID );

        $title = apply_filters( 'nav_menu_item_title', $title, $menu_item, $args, $depth );

        $item_output = '';
        if($depth === 0) {
            $item_output .= '<a' . $attributes . '>';
            $item_output .= '<span>' . $title . '</span>';
            if ($args->walker->has_children) $item_output .= '<svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M0.499901 1.00005L4.83003 4.5L9.16016 1.00005" stroke="#0A1E46"/></svg>';
            $item_output .= '</a>';
        } elseif ($depth === 1) {
            $item_output .= '<div class="menu-item-two-level">';
            $item_output .= '<a' . $attributes . '>';
            $item_output .= $title;
            $item_output .= '</a>';
            $item_output .= '</div>';
        } elseif ($depth === 2) {
            if(!empty($menu_item->description)) {
                $description_class = !empty($icon) ? 'menu-item-description is-icon' : 'menu-item-description';

                $item_output .= '<a' . $attributes . '>';
                $item_output .= '<span class="menu-item-wrap">';
                if(!empty($icon)) $item_output .= wp_get_attachment_image($icon);
                $item_output .= '<span>' . $title . '</span>';
                if(!empty($show_free)) $item_output .= '<div class="menu-item-free">Free</div>';
                $item_output .= '</span>';
                $item_output .= '<span class="' . $description_class . '">' . esc_html($menu_item->description) . '</span>';
                $item_output .= '</a>';
            } else {
                $item_output .= '<a' . $attributes . '>';
                if(!empty($icon)) $item_output .= wp_get_attachment_image($icon);
                $item_output .= '<span>' . $title . '</span>';
                if(!empty($show_free)) $item_output .= '<div class="menu-item-free">Free</div>';
                $item_output .= '</a>';
            }
        } else {
            $item_output .= '<a' . $attributes . '>';
            $item_output .= $title;
            $item_output .= '</a>';
        }

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $menu_item, $depth, $args );
    }

    public function end_el( &$output, $data_object, $depth = 0, $args = null ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $output .= "</li>{$n}";
    }

}

class CLE_Mobile_Walker_Nav_Menu extends Walker_Nav_Menu {
    public $tree_type = array( 'post_type', 'taxonomy', 'custom' );

    public $db_fields = array(
        'parent' => 'menu_item_parent',
        'id'     => 'db_id',
    );

    public function start_lvl( &$output, $depth = 0, $args = null ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = str_repeat( $t, $depth );

        // Default class.
        $classes = array( 'sub-menu' );

        $class_names = implode( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        if($depth === 0) {
            $output .= "{$n}{$indent}<ul$class_names>{$n}";
            $output .= '<button class="close-sub-menu"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="15" cy="15" r="14.25" transform="rotate(-180 15 15)" fill="white" stroke="#1464FF" stroke-width="1.5"/><path d="M9.46967 14.4697C9.17678 14.7626 9.17678 15.2374 9.46967 15.5303L14.2426 20.3033C14.5355 20.5962 15.0104 20.5962 15.3033 20.3033C15.5962 20.0104 15.5962 19.5355 15.3033 19.2426L11.0607 15L15.3033 10.7574C15.5962 10.4645 15.5962 9.98959 15.3033 9.6967C15.0104 9.40381 14.5355 9.40381 14.2426 9.6967L9.46967 14.4697ZM21 14.25L10 14.25L10 15.75L21 15.75L21 14.25Z" fill="#1464FF"/></svg><span>Back to Menu</span></button>';
        } else {
            $output .= "{$n}{$indent}<ul$class_names>{$n}";
        }
    }

    public function end_lvl( &$output, $depth = 0, $args = null ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent  = str_repeat( $t, $depth );
        $output .= "$indent</ul>{$n}";
    }

    public function start_el( &$output, $data_object, $depth = 0, $args = null, $current_object_id = 0 ) {
        $menu_item = $data_object;

        $submenu_style = get_field('submenu_style', $menu_item);

        // depth > 0
        $selected_menu_type = get_field('selected_menu_type', $menu_item);
        $resources = get_field('resources', $menu_item);

        // Sub menu lower level
        $icon = get_field('icon', $menu_item);
        $show_free = get_field('show_free', $menu_item);

        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

        $classes   = empty( $menu_item->classes ) ? array() : (array) $menu_item->classes;
        $classes[] = 'menu-item-' . $menu_item->ID;
        if(!empty($submenu_style)) $classes[] = 'menu-item_' . $submenu_style;
        if(!empty($selected_menu_type)) $classes[] = 'menu-item_' . $selected_menu_type;
        if($depth === 2 && !empty($icon)) $classes[] = 'is-icon-menu-item';

        $args = apply_filters( 'nav_menu_item_args', $args, $menu_item, $depth );

        $class_names = implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $menu_item, $args, $depth ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $menu_item->ID, $menu_item, $args, $depth );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        if (!empty($selected_menu_type) && $selected_menu_type === 'popular-resources') {
            $arr_posts = [];
            foreach ($resources as $pst) {
                $arr_posts[] = [
                    'title' => $pst->post_title,
                    'img' => wp_get_attachment_image_url(get_post_thumbnail_id($pst->ID)),
                    'url' => get_the_permalink($pst->ID)
                ];
            }

            $output .= $indent . "<li" . $id . $class_names . " data-posts='" . json_encode($arr_posts) . "'>";
        } else {
            $output .= $indent . '<li' . $id . $class_names . '>';
        }

        $atts           = array();
        $atts['title']  = ! empty( $menu_item->attr_title ) ? $menu_item->attr_title : '';
        $atts['target'] = ! empty( $menu_item->target ) ? $menu_item->target : '';
        if ( '_blank' === $menu_item->target && empty( $menu_item->xfn ) ) {
            $atts['rel'] = 'noopener';
        } else {
            $atts['rel'] = $menu_item->xfn;
        }
        if(!empty($menu_item->description) && $depth === 2) $atts['class'] = 'is-description';

        if ( ! empty( $menu_item->url ) ) {
            if ( get_privacy_policy_url() === $menu_item->url ) {
                $atts['rel'] = empty( $atts['rel'] ) ? 'privacy-policy' : $atts['rel'] . ' privacy-policy';
            }

            $atts['href'] = $menu_item->url;
        } else {
            $atts['href'] = '';
        }

        $atts['aria-current'] = $menu_item->current ? 'page' : '';

        $atts = apply_filters( 'nav_menu_link_attributes', $atts, $menu_item, $args, $depth );

        $attributes = '';
        foreach ( $atts as $attr => $value ) {
            if ( is_scalar( $value ) && '' !== $value && false !== $value ) {
                $value       = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $title = apply_filters( 'the_title', $menu_item->title, $menu_item->ID );

        $title = apply_filters( 'nav_menu_item_title', $title, $menu_item, $args, $depth );

        $item_output = '';
        if($args->walker->has_children && $depth === 0) {
            $item_output .= '<div class="menu-item-wrap">';
            $item_output .= '<a' . $attributes . '>';
            $item_output .= $title;
            $item_output .= '</a>';
            $item_output .= '<button class="open-sub-menu"><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="15" cy="15" r="14.25" stroke="#1464FF" stroke-width="1.5"/><path d="M20.5303 15.5303C20.8232 15.2374 20.8232 14.7626 20.5303 14.4697L15.7574 9.6967C15.4645 9.40381 14.9896 9.40381 14.6967 9.6967C14.4038 9.98959 14.4038 10.4645 14.6967 10.7574L18.9393 15L14.6967 19.2426C14.4038 19.5355 14.4038 20.0104 14.6967 20.3033C14.9896 20.5962 15.4645 20.5962 15.7574 20.3033L20.5303 15.5303ZM9 15.75L20 15.75L20 14.25L9 14.25L9 15.75Z" fill="#1464FF"/></svg></button>';
            $item_output .= '</div>';
        } elseif ($depth === 1) {
            $item_output .= '<div class="menu-item-two-level">';
            $item_output .= '<a' . $attributes . '>';
            $item_output .= '<span>' . $title . '</span><svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="15" cy="15" r="14.25" stroke="#1464FF" stroke-width="1.5"/><path d="M20.5303 15.5303C20.8232 15.2374 20.8232 14.7626 20.5303 14.4697L15.7574 9.6967C15.4645 9.40381 14.9896 9.40381 14.6967 9.6967C14.4038 9.98959 14.4038 10.4645 14.6967 10.7574L18.9393 15L14.6967 19.2426C14.4038 19.5355 14.4038 20.0104 14.6967 20.3033C14.9896 20.5962 15.4645 20.5962 15.7574 20.3033L20.5303 15.5303ZM9 15.75L20 15.75L20 14.25L9 14.25L9 15.75Z" fill="#1464FF"/></svg>';
            $item_output .= '</a>';
            $item_output .= '</div>';
        } elseif ($depth === 2) {
            if(!empty($menu_item->description)) {
                $description_class = !empty($icon) ? 'menu-item-description is-icon' : 'menu-item-description';

                $item_output .= '<a' . $attributes . '>';
                $item_output .= '<span class="menu-item-wrap">';
                if(!empty($icon)) $item_output .= wp_get_attachment_image($icon);
                $item_output .= '<span>' . $title . '</span>';
                if(!empty($show_free)) $item_output .= '<div class="menu-item-free">Free</div>';
                $item_output .= '</span>';
                $item_output .= '<span class="' . $description_class . '">' . esc_html($menu_item->description) . '</span>';
                $item_output .= '</a>';
            } else {
                $item_output .= '<a' . $attributes . '>';
                if(!empty($icon)) $item_output .= wp_get_attachment_image($icon);
                $item_output .= '<span>' . $title . '</span>';
                if(!empty($show_free)) $item_output .= '<div class="menu-item-free">Free</div>';
                $item_output .= '</a>';
            }
        } else {
            $item_output .= '<a' . $attributes . '>';
            $item_output .= $title;
            $item_output .= '</a>';
        }

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $menu_item, $depth, $args );
    }

    public function end_el( &$output, $data_object, $depth = 0, $args = null ) {
        if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
            $t = '';
            $n = '';
        } else {
            $t = "\t";
            $n = "\n";
        }
        $output .= "</li>{$n}";
    }

}
