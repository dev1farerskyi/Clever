<?php

/**
 * Enqueue scripts.
 */
function vil_enqueue_scripts()
{
    if (is_admin()) return false;

    wp_enqueue_style('montserrat-fonts', '//fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,600;0,700;1,300&display=swap');
    wp_enqueue_style('mulish-fonts', '//fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700;800&display=swap');
    wp_enqueue_style('select2', '//cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
    wp_enqueue_style('clever-style', get_stylesheet_directory_uri() . '/assets/css/style.css');

    wp_enqueue_script('select2', '//cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('clever-script', get_stylesheet_directory_uri() . '/assets/js/script.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('mfp', get_stylesheet_directory_uri() . '/assets/js/mfp.min.js', array('jquery'), '1.0.0', true);

    wp_localize_script('clever-script', 'cle_object', array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'site_url' => site_url('/')
    ));

}

add_action('wp_enqueue_scripts', 'vil_enqueue_scripts');

function sort_posts()
{
    $paged = $_POST['page'];
    $sortby = $_POST['sortby'];
    $category = $_POST['category'];

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => 16,
        'paged' => $paged,
        'order' => 'ASC',
        'orderby' => 'date',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'id',
                'terms' => $category,
            ),
        ),
    );

    if ($sortby === 'newest') {
        $args['order'] = 'DESC';
    }

    if ($sortby === 'quick-reads') {
        $args['orderby'] = 'meta_value_num';
        $args['meta_key'] = 'time_to_read';
    }

    $wp_query = new WP_Query($args);
    ob_start();
    if ($wp_query->have_posts()) :
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php $time_to_read = get_field('time_to_read', get_the_ID()); ?>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="vil-more-posts__card">
                    <div class="vil-more-posts__card-image-wrap">
                        <img class="vil-more-posts__card-image"
                             src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                    </div>
                    <div class="content">
                        <div class="vil-more-posts__card-info">
                            <div class="vil-more-posts__card-info-left">
                            <?php if (!empty(get_the_category())) : ?>
                                <?php foreach ((get_the_category()) as $category): ?>
                                    <div class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </div>
                            <div class="vil-more-posts__card-info-right">
                                <img class="vil-articles__card-clock"
                                     src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/icon-clock-grey.svg"
                                     alt="">
                                <?php if (!empty($time_to_read)) : ?>
                                    <span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <a href="<?php the_permalink(); ?>"
                           class="vil-featured-content__three-posts-card-title"><?php the_title(); ?></a>
                    </div>
                </div>
            </div>
        <?php endwhile;
    endif;
    $content = ob_get_clean();
    wp_reset_query();

    $max_pages = $wp_query->max_num_pages;

    $result = ['content' => $content, 'max_pages' => $max_pages];
    wp_send_json($result);
    wp_die();
}

add_action('wp_ajax_sort_posts', 'sort_posts');
add_action('wp_ajax_nopriv_sort_posts', 'sort_posts');

function load_posts()
{
    $paged = $_POST['page'];
    $sortby = $_POST['sortby'];
    $category = $_POST['category'];

    $args = array(
        'post_type' => 'post',
        'post_status' => 'publish',
        'posts_per_page' => 16,
        'paged' => $paged,
        'order' => 'ASC',
        'orderby' => 'date',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'id',
                'terms' => $category,
            ),
        ),
    );

    if ($sortby === 'newest') {
        $args['order'] = 'DESC';
    }

    if ($sortby === 'quick-reads') {
        $args['orderby'] = 'meta_value_num';
        $args['meta_key'] = 'time_to_read';
    }

    $wp_query = new WP_Query($args);
    ob_start();
    if ($wp_query->have_posts()) :
        while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php $time_to_read = get_field('time_to_read', get_the_ID()); ?>
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="vil-more-posts__card">
                    <div class="vil-more-posts__card-image-wrap">
                        <img class="vil-more-posts__card-image"
                             src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                    </div>
                    <div class="content">
                        <div class="vil-more-posts__card-info">
                            <div class="vil-more-posts__card-info-left">
                            <?php if (!empty(get_the_category())) : ?>
                                <?php foreach ((get_the_category()) as $category): ?>
                                    <div class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </div>
                            <div class="vil-more-posts__card-info-right">
                                <img class="vil-articles__card-clock"
                                     src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/icon-clock-grey.svg"
                                     alt="">
                                <?php if (!empty($time_to_read)) : ?>
                                    <span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <a href="<?php the_permalink(); ?>"
                           class="vil-featured-content__three-posts-card-title"><?php the_title(); ?></a>
                    </div>
                </div>
            </div>
        <?php endwhile;
    endif;
    $content = ob_get_clean();
    wp_reset_query();

    $max_pages = $wp_query->max_num_pages;

    $result = ['content' => $content, 'max_pages' => $max_pages];
    wp_send_json($result);
    wp_die();
}

add_action('wp_ajax_load_posts', 'load_posts');
add_action('wp_ajax_nopriv_load_posts', 'load_posts');

