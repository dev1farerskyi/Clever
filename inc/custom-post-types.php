<?php

function cptui_register_my_cpts() {

	/**
	 * Post Type: Testimonials.
	 */

	$labels = [
		"name" => esc_html__( "Testimonials", "villanova" ),
		"singular_name" => esc_html__( "Testimonials", "villanova" ),
	];

	$args = [
		"label" => esc_html__( "Testimonials", "villanova" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"rest_namespace" => "wp/v2",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => false,
		"rewrite" => [ "slug" => "testimonials", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title" ],
		"show_in_graphql" => false,
	];

	register_post_type( "testimonials", $args );



	register_post_type( 'team-members',
        array(
            'labels' => array(
                'name' => __( 'Team members' ),
                'singular_name' => __( 'Team members' )
            ),
            'supports' => array( 'title' ),
            'public' => true,
            'has_archive' => false,
            'publicly_queryable' => false,
            'show_in_rest' => false,
        )
    );


	register_post_type( 'case-study',
        array(
            'labels' => array(
                'name' => __( 'Case Studies' ),
                'singular_name' => __( 'Case Study' )
            ),
            'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
            'public' => true,
            'has_archive' => true,
            'show_in_rest' => true,
        )
    );

	register_post_type( 'events',
        array(
            'labels' => array(
                'name' => __( 'Events' ),
                'singular_name' => __( 'Event' )
            ),
            'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
            'public' => true,
            'has_archive' => true,
            'show_in_rest' => true,
        )
    );

    register_taxonomy('events_cat', 'events', array(
        'hierarchical' => false,
        'public' => true,
        // 'meta_box_cb' => false,
        'show_in_rest' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'labels' => array(
            'name' => _x( 'Categories', 'taxonomy general name' ),
            'singular_name' => _x( 'Category', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Categories' ),
            'all_items' => __( 'All Categories' ),
            'parent_item' => __( 'Parent Category' ),
            'parent_item_colon' => __( 'Parent Category:' ),
            'edit_item' => __( 'Edit Category' ),
            'update_item' => __( 'Update Category' ),
            'add_new_item' => __( 'Add New Category' ),
            'new_item_name' => __( 'New Category Name' ),
            'menu_name' => __( 'Categories' ),
        ),
        'rewrite' => array(
            'with_front' => false, // Don't display the category base before "/locations/"
            'hierarchical' => false // This will allow URL's like "/locations/boston/cambridge/"
        ),
    ));

    register_taxonomy('case_study_cat', 'case-study', array(
        'hierarchical' => false,
        'public' => true,
        // 'meta_box_cb' => false,
        'show_in_rest' => true,
        'show_ui' => true,
        'show_admin_column' => true,
        'labels' => array(
            'name' => _x( 'Categories', 'taxonomy general name' ),
            'singular_name' => _x( 'Category', 'taxonomy singular name' ),
            'search_items' =>  __( 'Search Categories' ),
            'all_items' => __( 'All Categories' ),
            'parent_item' => __( 'Parent Category' ),
            'parent_item_colon' => __( 'Parent Category:' ),
            'edit_item' => __( 'Edit Category' ),
            'update_item' => __( 'Update Category' ),
            'add_new_item' => __( 'Add New Category' ),
            'new_item_name' => __( 'New Category Name' ),
            'menu_name' => __( 'Categories' ),
        ),
        'rewrite' => array(
            'with_front' => false, // Don't display the category base before "/locations/"
            'hierarchical' => false // This will allow URL's like "/locations/boston/cambridge/"
        ),
    ));
}

add_action( 'init', 'cptui_register_my_cpts' );