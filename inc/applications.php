<?php

function cle_get_application_by_shortname($app_shortname) {
    $apps = cle_get_applications();

    foreach ($apps as $app) {
        if ( $app['App Shortname'] == $app_shortname ) {
            return $app;
        }
    }

    return array();
}

function cle_get_application_by_id($app_id) {
	$apps = cle_get_applications();

    foreach ($apps as $app) {
        if ( $app['App ID'] == $app_id ) {
            return $app;
        }
    }

    return array();
}

function cle_get_applications() {
    $apps_list = get_transient('apps_list');

    if ( empty( $apps_list ) ) {
        $list = file_get_contents('https://assets.clever.com/website/app-gallery-import/app-gallery.json');
        $apps_list = json_decode($list, true);
        set_transient('apps_list', $apps_list, 60 * 60); // one hour
    }

    return $apps_list;
}



function cle_applications_list_values( $field ) {
    $apps = cle_get_applications();
    $field['choices'] = array();
     
    foreach ($apps as $app) {
        $field['choices'][ $app['App ID'] ] = $app['App Name'];
    }
    
    return $field;
}
add_filter('acf/load_field/name=applications_list', 'cle_applications_list_values');



function cle_custom_rewrite_rule() {
    add_rewrite_tag('%app_gallery%', '([^&]+)');
    add_rewrite_rule('^app-gallery/([^/]*)/?','index.php?pagename=app-gallery&app_gallery=$matches[1]','top');
}
add_action('init', 'cle_custom_rewrite_rule', 10, 0);



function cle_get_app_categories($category = '') {
    $list = array(
        'assessment' => 'Assessment',
        'classroom_management' => 'Classroom Management',
        'communication' => 'Communication',
        'engineering' => 'Engineering',
        'intervention' => 'Intervention',
        'it_management' => 'IT Management',
        'language' => 'Language',
        'math' => 'Math',
        'reading' => 'Reading',
        'science' => 'Science',
        'social_studies' => 'Social Studies',
        'typing' => 'Typing',
        'other' => 'Other',
    );

    if ( ! empty( $category ) && isset( $list[ $category ] ) ) {
        return $list[ $category ];
    }

    return $list;
}