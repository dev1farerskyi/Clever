<?php

/**
 * ACF Options page support
 */
if ( function_exists('acf_add_options_page') ) {
	acf_add_options_page();	
}


/**
 * Get button
 *
 * @param $button
 * @param $classes
 * @return void
 */
function cle_get_button($button, $classes = '') {
    if ( ! empty( $button ) ) :
        $link_target = ! empty( $button['target'] ) ? $button['target'] : '_self';
        ?>
        <a class="<?php echo $classes; ?>" href="<?php echo $button['url']; ?>" target="<?php echo esc_attr( $link_target ); ?>">
            <?php echo $button['title']; ?>
        </a>
    <?php endif;
}

/**
 * Get button
 *
 * @param $button
 * @param $classes
 * @return void
 */

function cle_change_form_submit_button($button, $form)
{
    $class_name = 'cle-subscribe__info gform-button';
    $button_text = 'Subscribe';
    $button_element = '<svg width="29" height="12" viewBox="0 0 29 12" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M28.5303 6.53033C28.8232 6.23743 28.8232 5.76256 28.5303 5.46967L23.7574 0.696697C23.4645 0.403804 22.9896 0.403804 22.6967 0.696697C22.4038 0.989591 22.4038 1.46446 22.6967 1.75736L26.9393 6L22.6967 10.2426C22.4038 10.5355 22.4038 11.0104 22.6967 11.3033C22.9896 11.5962 23.4645 11.5962 23.7574 11.3033L28.5303 6.53033ZM6.55671e-08 6.75L28 6.75L28 5.25L-6.55671e-08 5.25L6.55671e-08 6.75Z" fill="white"/>
</svg>
';
    return '<button type="submit" class="gform_button ' . $class_name . '" id="gform_submit_button_' . $form['id'] . '">' . $button_text . $button_element . '</button>';
}
add_filter('gform_submit_button', 'cle_change_form_submit_button', 10, 2);



function cle_get_years( $post_type ) {
	$terms_year = array(
	    'post_type' => $post_type,
	    'posts_per_page' => -1
	);

	$years = array();
	$query_year = new WP_Query( $terms_year );

	if ( $query_year->have_posts() ) {
	    while ( $query_year->have_posts() ) {
	    	$query_year->the_post();
	        $year = get_the_date('Y');
	        
	        if( ! in_array( $year, $years ) ){
	            $years[] = $year;
	        }
	    }
	    wp_reset_postdata();
	}

	return $years;
}


function cle_get_blog_events_html( $args, $posts_to_show = 3 ) {
	$paged = ! isset( $args['paged'] ) ? 1 : $args['paged'];
	
	$args['paged'] = $paged;
	
	if ( ! isset( $args['posts_per_page'] ) ) {
		$args['posts_per_page'] = $posts_to_show;
	}

	$the_query = new WP_Query( $args );

	ob_start();
	if ( $the_query->have_posts() ) {
		echo '<div class="cle-cpt-blocks-section__cards"><div class="row">';
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			
			get_template_part('template-parts/' . $args['post_type'] . '/item');

		}
		echo '</div></div>';
		$big = 999999999; // need an unlikely integer
		$pagination = paginate_links( array(
			'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, $paged ),
			'total' => $the_query->max_num_pages,
			'prev_text' => '<svg width="21" height="12" viewBox="0 0 21 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.805346 6.53033C0.512453 6.23744 0.512453 5.76256 0.805345 5.46967L5.57832 0.6967C5.87121 0.403807 6.34608 0.403807 6.63898 0.6967C6.93187 0.989594 6.93187 1.46447 6.63898 1.75736L2.39634 6L6.63898 10.2426C6.93187 10.5355 6.93187 11.0104 6.63898 11.3033C6.34608 11.5962 5.87121 11.5962 5.57832 11.3033L0.805346 6.53033ZM20.4023 6.75L1.33568 6.75L1.33568 5.25L20.4023 5.25L20.4023 6.75Z" fill="#1464FF" />
                            </svg>',
			'next_text' => '<svg width="21" height="12" viewBox="0 0 21 12" fill="none" xmlns="http://www.w3.org/2000/svg">
	                            <path d="M20.1986 6.53033C20.4915 6.23744 20.4915 5.76256 20.1986 5.46967L15.4256 0.6967C15.1327 0.403807 14.6578 0.403807 14.3649 0.6967C14.072 0.989594 14.072 1.46447 14.3649 1.75736L18.6076 6L14.3649 10.2426C14.072 10.5355 14.072 11.0104 14.3649 11.3033C14.6578 11.5962 15.1327 11.5962 15.4256 11.3033L20.1986 6.53033ZM0.601562 6.75L19.6682 6.75L19.6682 5.25L0.601563 5.25L0.601562 6.75Z" fill="#1464FF" />
	                        </svg>',
		) ); 

		wp_reset_postdata();

		if ( ! empty( $pagination ) ) {
			?>
			<div class="cle-pagination mt-75">
                <div class="cle-pagination__list">
                	<?php echo $pagination; ?>
                </div>
            </div>
			<?php
		}
	} else {
		echo 'Nothing found.';
	}

	return ob_get_clean();
}



function cle_get_blog_events() {
	$args = array(
		'post_type' => $_POST['post_type'],
		'paged' => $_POST['page'],
	);

	if ( ! empty( $_POST['s'] ) ) {
		$args['s'] = $_POST['s'];
	}

	if ( ! empty( $_POST['month'] ) ) {
		$args['date_query'] = array(
			array(
				'month' => $_POST['month']
			)
		);
	}

	if ( ! empty( $_POST['year'] ) ) {
		if ( ! empty( $args['date_query'] ) ) {
			$args['date_query'] = array(
				array(
					'month' => $_POST['month'],
					'year' => $_POST['year']
				)
			);
		} else {
			$args['date_query'] = array(
				array(
					'year' => $_POST['year']
				)
			);
		}
	}

	$html = cle_get_blog_events_html( $args );

	echo $html;
	die;
}
add_action( 'wp_ajax_cle_get_blog_events', 'cle_get_blog_events' );
add_action( 'wp_ajax_nopriv_cle_get_blog_events', 'cle_get_blog_events' ); 



function cle_get_deps( $jobs ) {
	$deps = array();

	if ( ! empty( $jobs ) ) {
		foreach ( $jobs as $job ) {
			if ( ! empty( $job['departments'] ) ) {
			    foreach($job['departments'] as $dep ) {
			        $deps[ $dep['id'] ] = $dep['name'];
			    }
			}
		}
	}

	return $deps;
}

function cle_post_types_list($pt = '') {
	$post_types = array(
		'post' => 'News',
		'events' => 'Events',
		'case-study' => 'Case studies',
	);

	if ( ! empty( $pt ) && isset( $post_types[ $pt ] ) ) {
		return $post_types[ $pt ];
	}

	return $post_types;
}

function cle_post_types_list_values( $field ) {
	$post_types = cle_post_types_list();

    $field['choices'] = array();
     
    foreach ($post_types as $key => $pt) {
        $field['choices'][ $key ] = $pt;
    }
    
    return $field;
}
add_filter('acf/load_field/name=choose_post_types', 'cle_post_types_list_values');
