<?php
/**
 * Footer template.
 *
 * @package villanova
 * @since 1.0.0
 *
 */

$socials = get_field('socials', 'option');
$facebook_url = get_field('facebook_url', 'option');
$twitter_url = get_field('twitter_url', 'option');
$instagram_url = get_field('instagram_url', 'option');
$linkedin_url = get_field('linkedin_url', 'option');
$youtube_url = get_field('youtube_url', 'option');

$copyright_text = get_field('copyright_text', 'option');
$bottom_text = get_field('bottom_text', 'option');
$footer_links = get_field('footer_links', 'option');

?>
</main>
<footer class="cle-footer">
    <div class="container">
        <div class="cle-footer__wrap">
            <div class="row">
                <div class="col-lg-3">
                    <div class="cle-footer__logo">
                        <a href="<?php echo get_home_url(); ?>" class="">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/logo.png" alt="logo">
                        </a>
                    </div>
                    <div class="cle-footer__socials">
                            <h5 class="cle-footer__socials-text"><?php esc_html_e( 'Follow Us:', V_PREFIX); ?></h5>
                            <ul class="cle-footer__socials-links">
                                <?php if (!empty($facebook_url)): ?>
                                    <li>
                                        <a class="cle-footer__socials-link" href="<?php echo $facebook_url; ?>" target="_blank">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/facebook.svg"
                                                 alt="">
                                        </a>
                                    </li>
                                <?php endif ?>
                                <?php if (!empty($twitter_url)): ?>
                                    <li>
                                        <a class="cle-footer__socials-link" href="<?php echo $twitter_url; ?>" target="_blank">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/twitter.svg"
                                                 alt="">
                                        </a>
                                    </li>
                                <?php endif ?>
                                <?php if (!empty($instagram_url)): ?>
                                    <li>
                                        <a class="cle-footer__socials-link" href="<?php echo $instagram_url; ?>" target="_blank">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/instagram.svg" alt="">
                                        </a>
                                    </li>
                                <?php endif ?>
                                <?php if (!empty($linkedin_url)): ?>
                                    <li>
                                        <a class="cle-footer__socials-link" href="<?php echo $linkedin_url; ?>" target="_blank">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/linkedin.svg" alt="">
                                        </a>
                                    </li>
                                <?php endif ?>
                                <?php if (!empty($youtube_url)): ?>
                                    <li>
                                        <a class="cle-footer__socials-link" href="<?php echo $youtube_url; ?>" target="_blank">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/youtube.svg" alt="">
                                        </a>
                                    </li>
                                <?php endif ?>
                            </ul>
                        </div>
                </div>
                <div class="col-lg-9">
                    <div class="cle-footer__menu">
                        <div class="row">
                            <div class="cle-footer__menu-top">
                                <div class="cle-footer__menu-wrap">
                                    <h5 class="cle-footer__menu-list-title"><?php esc_html_e( 'Solutions', V_PREFIX); ?></h5>
                                    <?php wp_nav_menu(
                                        array(
                                            'container'      => '',
                                            'items_wrap'     => '<ul class="cle-footer__menu-list">%3$s</ul>',
                                            'theme_location' => 'footer-menu-1',
                                            'depth'          => 1,
                                            'fallback_cb'    => '__return_empty_string',
                                        )
                                    ); ?>
                                </div>
                                <div class="cle-footer__menu-wrap">
                                    <h5 class="cle-footer__menu-list-title"><?php esc_html_e( 'Data & Access', V_PREFIX); ?></h5>
                                    <?php wp_nav_menu(
                                        array(
                                            'container'      => '',
                                            'items_wrap'     => '<ul class="cle-footer__menu-list">%3$s</ul>',
                                            'theme_location' => 'footer-menu-2',
                                            'depth'          => 1,
                                            'fallback_cb'    => '__return_empty_string',
                                        )
                                    ); ?>
                                </div>
                                <div class="cle-footer__menu-wrap">

                                    <h5 class="cle-footer__menu-list-title"><?php esc_html_e( 'Digital Classroom', V_PREFIX); ?></h5>
                                    <?php wp_nav_menu(
                                        array(
                                            'container'      => '',
                                            'items_wrap'     => '<ul class="cle-footer__menu-list">%3$s</ul>',
                                            'theme_location' => 'footer-menu-3',
                                            'depth'          => 1,
                                            'fallback_cb'    => '__return_empty_string',
                                        )
                                    ); ?>

                                </div>
                                <div class="cle-footer__menu-wrap">
                                    <h5 class="cle-footer__menu-list-title"><?php esc_html_e( 'Identity & Security', V_PREFIX); ?></h5>
                                    <?php wp_nav_menu(
                                        array(
                                            'container'      => '',
                                            'items_wrap'     => '<ul class="cle-footer__menu-list">%3$s</ul>',
                                            'theme_location' => 'footer-menu-4',
                                            'depth'          => 1,
                                            'fallback_cb'    => '__return_empty_string',
                                        )
                                    ); ?>
                                </div>
                            </div>
                            <div class="cle-footer__menu-bottom">
                                <div class="cle-footer__menu-wrap">
                                    <h5 class="cle-footer__menu-list-title"><?php esc_html_e( 'Other Products', V_PREFIX); ?></h5>
                                    <?php wp_nav_menu(
                                        array(
                                            'container'      => '',
                                            'items_wrap'     => '<ul class="cle-footer__menu-list">%3$s</ul>',
                                            'theme_location' => 'footer-menu-5',
                                            'depth'          => 1,
                                            'fallback_cb'    => '__return_empty_string',
                                        )
                                    ); ?>
                                </div>
                                <div class="cle-footer__menu-wrap">
                                    <h5 class="cle-footer__menu-list-title"><?php esc_html_e( 'Services', V_PREFIX); ?></h5>
                                    <?php wp_nav_menu(
                                        array(
                                            'container'      => '',
                                            'items_wrap'     => '<ul class="cle-footer__menu-list">%3$s</ul>',
                                            'theme_location' => 'footer-menu-6',
                                            'depth'          => 1,
                                            'fallback_cb'    => '__return_empty_string',
                                        )
                                    ); ?>
                                </div>
                                <div class="cle-footer__menu-wrap">
                                    <h5 class="cle-footer__menu-list-title"><?php esc_html_e( 'Platform', V_PREFIX); ?></h5>
                                    <?php wp_nav_menu(
                                        array(
                                            'container'      => '',
                                            'items_wrap'     => '<ul class="cle-footer__menu-list">%3$s</ul>',
                                            'theme_location' => 'footer-menu-7',
                                            'depth'          => 1,
                                            'fallback_cb'    => '__return_empty_string',
                                        )
                                    ); ?>
                                </div>
                                <div class="cle-footer__menu-wrap">
                                    <h5 class="cle-footer__menu-list-title"><?php esc_html_e( 'Our Company', V_PREFIX); ?></h5>
                                    <?php wp_nav_menu(
                                        array(
                                            'container'      => '',
                                            'items_wrap'     => '<ul class="cle-footer__menu-list">%3$s</ul>',
                                            'theme_location' => 'footer-menu-8',
                                            'depth'          => 1,
                                            'fallback_cb'    => '__return_empty_string',
                                        )
                                    ); ?>
                                </div>
                                <div class="cle-footer__menu-wrap">
                                    <h5 class="cle-footer__menu-list-title"><?php esc_html_e( 'Help', V_PREFIX); ?></h5>
                                    <?php wp_nav_menu(
                                        array(
                                            'container'      => '',
                                            'items_wrap'     => '<ul class="cle-footer__menu-list">%3$s</ul>',
                                            'theme_location' => 'footer-menu-9',
                                            'depth'          => 1,
                                            'fallback_cb'    => '__return_empty_string',
                                        )
                                    ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="cle-footer__socials-mobile">
                            <h5 class="cle-footer__socials-text"><?php esc_html_e( 'Follow Us:', V_PREFIX); ?></h5>
                            <ul class="cle-footer__socials-links">
                                <?php if (!empty($facebook_url)): ?>
                                    <li>
                                        <a class="cle-footer__socials-link" href="<?php echo $facebook_url; ?>" target="_blank">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/facebook.svg"
                                                 alt="">
                                        </a>
                                    </li>
                                <?php endif ?>
                                <?php if (!empty($twitter_url)): ?>
                                    <li>
                                        <a class="cle-footer__socials-link" href="<?php echo $twitter_url; ?>" target="_blank">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/twitter.svg"
                                                 alt="">
                                        </a>
                                    </li>
                                <?php endif ?>
                                <?php if (!empty($instagram_url)): ?>
                                    <li>
                                        <a class="cle-footer__socials-link" href="<?php echo $instagram_url; ?>" target="_blank">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/instagram.svg" alt="">
                                        </a>
                                    </li>
                                <?php endif ?>
                                <?php if (!empty($linkedin_url)): ?>
                                    <li>
                                        <a class="cle-footer__socials-link" href="<?php echo $linkedin_url; ?>" target="_blank">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/linkedin.svg" alt="">
                                        </a>
                                    </li>
                                <?php endif ?>
                                <?php if (!empty($youtube_url)): ?>
                                    <li>
                                        <a class="cle-footer__socials-link" href="<?php echo $youtube_url; ?>" target="_blank">
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/youtube.svg" alt="">
                                        </a>
                                    </li>
                                <?php endif ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="cle-footer__copyright">
        <div class="container">
            <div class="cle-footer__copyright-wrap">
                <?php if ( ! empty( $copyright_text ) ): ?>
                    <div class="cle-footer__copyright-copy"><?php echo $copyright_text; ?></div>
                <?php endif ?>

                <div class="cle-footer__copyright-main">
                    <?php if ( ! empty( $bottom_text ) ): ?>
                        <p class="cle-footer__copyright-main-text"><?php echo $bottom_text; ?></p>
                    <?php endif ?>
                    
                    <?php if ( ! empty( $footer_links ) ): ?>
                        <?php foreach ($footer_links as $el): ?>
                            <?php cle_get_button($el['link'], 'cle-footer__copyright-main-link'); ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
                <a href="https://www.takeoffnyc.com/" target="_blank" rel="nofolow">
                    <img class="cle-footer__copyright-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/copyright-logo.svg" alt="logo">
                </a>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
