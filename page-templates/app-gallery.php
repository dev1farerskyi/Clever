<?php 
/* 
 * Template Name: App Gallery
 */ 

get_header();

$app_gallery_page = get_field('app_gallery_page', 'option');
$app_shortname = get_query_var('app_gallery');
$app_data = cle_get_application_by_shortname( $app_shortname );
$block_list = array();


$app_features = array(
	'secure_sync' => array(
		'description' => $app_data['Title'] . ' rosters and provisions accounts through Clever Secure Sync',
		'icon' => array(
			'url' => get_stylesheet_directory_uri(). '/assets/img/app-features/secure_sync.png',
			'alt' => $app_data['Title'] . ' rosters and provisions accounts through Clever Secure Sync',
		),
	),
	'secure_sync_lite' => array(
		'description' => $app_data['Title'] . ' provisions accounts through Clever Secure Sync Lite',
		'icon' => array(
			'url' => get_stylesheet_directory_uri(). '/assets/img/app-features/secure_sync.png',
			'alt' => $app_data['Title'] . ' provisions accounts through Clever Secure Sync Lite',
		),
	),
	'instant_login' => array(
		'description' => $app_data['Title'] . ' offers SSO through Clever Instant Login',
		'icon' => array(
			'url' => get_stylesheet_directory_uri(). '/assets/img/app-features/instant_login.png',
			'alt' => $app_data['Title'] . ' offers SSO through Clever Instant Login',
		),
	),
	'ios_support' => array(
		'description' => $app_data['Title'] . ' is compatible with the Clever Portal iOS app',
		'icon' => array(
			'url' => get_stylesheet_directory_uri(). '/assets/img/app-features/ios_support.png',
			'alt' => $app_data['Title'] . ' is compatible with the Clever Portal iOS app',
		),
	),
	'library_support' => array(
		'description' => $app_data['Title'] . ' is compatible with Clever Library',
		'icon' => array(
			'url' => get_stylesheet_directory_uri(). '/assets/img/app-features/library_support.png',
			'alt' => $app_data['Title'] . ' is compatible with Clever Library',
		),
	),
);



if ( ! empty( $app_data['App Features'] ) ) {
	foreach( $app_data['App Features'] as $feature ) {
		if ( isset( $app_features[ $feature ] ) ) {
			$block_list[] = $app_features[ $feature ];
		}
	}
}


if ( ! empty( $app_gallery_page ) ) {
	echo '<div class="cle-app-gallery-top">
            <span class="app_gallery_btn_back"><a class="cle-btn cle-btn_third-flip" href="' . $app_gallery_page . '" target="_self">
			    <span>Back to App Gallery</span>
			    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
				    <circle cx="15" cy="15" r="14.25" fill="white" stroke="#1464FF" stroke-width="1.5"></circle>
				    <path d="M20.5303 15.5303C20.8232 15.2374 20.8232 14.7626 20.5303 14.4697L15.7574 9.6967C15.4645 9.40381 14.9896 9.40381 14.6967 9.6967C14.4038 9.98959 14.4038 10.4645 14.6967 10.7574L18.9393 15L14.6967 19.2426C14.4038 19.5355 14.4038 20.0104 14.6967 20.3033C14.9896 20.5962 15.4645 20.5962 15.7574 20.3033L20.5303 15.5303ZM9 15.75L20 15.75L20 14.25L9 14.25L9 15.75Z" fill="#1464FF"></path>
				</svg>
			</a></span>
		</div>';
}


$body_args = array(
	'title' => $app_data['Title'],
	'description' => $app_data['App Description'],
	'image' => array(
		'url' => $app_data['App Logo URL'],
		'alt' => $app_data['Title'],
	),
	'list' => $block_list,
	'primary_button' => array(
		'url' => 'https://clever.com/signup/' . $app_data['App Shortname'],
		'title' => 'Install now',
		'target' => '_blank',
	),
);

if ( ! empty( $app_data['App Edsurge URL'] ) ) {
	$body_args['third_button'] = array(
		'url' => $app_data['App Edsurge URL'],
		'title' => 'Visit Edsurge writeup',
		'target' => '_blank',
	);
}

if ( ! empty( $app_data['App Website URL'] ) ) {
	$body_args['second_button'] = array(
		'url' => $app_data['App Website URL'],
		'title' => 'Visit ' . $app_data['Title'],
		'target' => '_blank',
	);
}


get_template_part('template-parts/blocks/app-detail-section', null, $body_args);


get_footer();