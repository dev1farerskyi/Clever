<?php
$terms = get_the_terms(get_the_ID(), 'case_study_cat');
?>
<div class="col-lg-4 col-md-6 col-sm-12">
	<div class="cle-cpt-blocks-section__card">
		<?php if ( has_post_thumbnail() ): ?>
			<div class="cle-cpt-blocks-section__card-img">
				<?php the_post_thumbnail(); ?>
				
				<?php if ( ! empty( $terms ) ): ?>
					<p class="cle-cpt-blocks-section__card-img-category"><?php echo $terms[0]->name; ?></p>
				<?php endif ?>
			</div>
		<?php endif ?>
		<div class="cle-cpt-blocks-section__card-content">
			<p class="cle-cpt-blocks-section__card-date"><?php echo get_the_date(); ?></p>
			<a class="cle-cpt-blocks-section__card-title h4" href="<?php the_permalink() ?>">
				<?php the_title(); ?>
			</a>
			<p class="cle-cpt-blocks-section__card-text"><?php echo get_the_excerpt(); ?></p>
		</div>
	</div>
</div>