<?php 
$job = $args['job'];
$departments = array();
if ( ! empty( $job['departments'] ) ) {
    foreach($job['departments'] as $dep ) {
        $departments[] = 'dep-' . $dep['id'];
    }
}
?>
<div class="cle-open-positions__card <?php echo implode(' dep-', $departments); ?>">
    <div class="cle-open-positions__card-wrap">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="cle-accordion min_accordion">
                    <?php if ( ! empty( $job['metadata'] ) && ! empty( $job['metadata'][0]['value'] ) ): ?>
                        <span class="cle-open-positions__card-title-link">
                            <?php echo $job['metadata'][0]['value']; ?>
                        </span>
                    <?php endif ?>
                    <h5 class="cle-open-positions__card-title">
                        <?php echo $job['title']; ?>
                    </h5>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12">
                <p class="cle-open-positions__card-location">
                    <?php echo $job['location']['name']; ?>
                </p>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-12">
                <div class="cle-open-positions__card-link">
                    <a href="<?php echo $job['absolute_url']; ?>" target="_blank" rel="nofolow">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/circle-link-blue.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>