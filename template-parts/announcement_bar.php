<?php
$is_page = is_singular('page') || is_singular('certificates');
$page_block = false;
$show_page_block = false;
$today 		= date('Ymd');

$enable_announcement_bar = get_field('enable_announcement_bar', 'option');

if ( $is_page ) {
	if( get_field('enable_announcement_bar') ) {
		$enable_announcement_bar = true;
		$page_block = true;

		$start_date = get_field('start_date', false, false);
		$end_date   = get_field('end_date', false, false);

		$show_page_block = $today >= $start_date && $today < $end_date;
	}
}

if ( $enable_announcement_bar ):
	if ( $page_block && $show_page_block ) {
		$ab_text 	= get_field('text');
		$ab_link 	= get_field('link');
		$new_tab    = get_field('open_in_new_tab');

		$target = $new_tab ? '_blank' : '_self';
	} else {
		if ( get_field('enable_announcement_bar', 'option' ) ) {
			$ab_text 	= get_field('ab_text', 'option');
			$ab_link 	= get_field('ab_link', 'option');
			$start_date = get_field('ab_start_date', 'option', false);
			$end_date   = get_field('ab_end_date', 'option', false);
			$new_tab    = get_field('ab_open_in_new_tab', 'option');

			$target = $new_tab ? '_blank' : '_self';
		}
	}

	if ( $today >= $start_date && $today < $end_date): ?>
		
		<div class="vil-message-popup show">
			<div class="vil-message-popup__wrap">
				<a href="<?php echo esc_url( $ab_link ) ?>" target="<?php echo $target; ?>">
					<?php echo $ab_text; ?>
				</a>
				<button class="vil-btn vil-message-popup__close">
					<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M8.27455 6L11.686 2.58852C12.1047 2.16989 12.1047 1.49114 11.686 1.07216L10.9278 0.313977C10.5092 -0.104659 9.83045 -0.104659 9.41148 0.313977L6 3.72545L2.58852 0.313977C2.16989 -0.104659 1.49114 -0.104659 1.07216 0.313977L0.313977 1.07216C-0.104659 1.4908 -0.104659 2.16955 0.313977 2.58852L3.72545 6L0.313977 9.41148C-0.104659 9.83011 -0.104659 10.5089 0.313977 10.9278L1.07216 11.686C1.4908 12.1047 2.16989 12.1047 2.58852 11.686L6 8.27455L9.41148 11.686C9.83011 12.1047 10.5092 12.1047 10.9278 11.686L11.686 10.9278C12.1047 10.5092 12.1047 9.83045 11.686 9.41148L8.27455 6Z" fill="#555555" />
					</svg>
				</button>
			</div>
		</div>
	<?php endif ?>
<?php endif ?>