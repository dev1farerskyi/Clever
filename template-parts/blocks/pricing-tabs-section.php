<?php
/*
 * Block Name: Pricing tabs section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$partners_title = get_field('partners_title');
$partners_subtitle = get_field('partners_subtitle');
$partners_description = get_field('partners_description');
$partners_table_head = get_field('partners_table_head');
$partners_table_body = get_field('partners_table_body');
$course_block = get_field('theme_course_block', 'option');

$schools_title = get_field('schools_title');
$schools_subtitle = get_field('schools_subtitle');
$schools_description = get_field('schools_description');
$schools_table_head = get_field('schools_table_head');
$schools_table_body = get_field('schools_table_body');
$solutions_blocks = get_field('theme_solutions_blocks', 'option');

$block_name = 'cle-pricing-tabs-section';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section';
?>

<section class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="cle-pricing-tabs-section__content">
        <div class="cle-pricing-tabs-section__tab cle-pricing-tabs-section__tab_active" data-tab="schools">
            <div class="container">
                <div class="cle-pricing-tabs-section__top cle-pricing-tabs-section__top_bot-sm-indent">
                    <?php if (!empty($schools_title)): ?>
                        <div class="cle-pricing-tabs-section__header">
                            <h1><?php echo $schools_title ?></h1>
                        </div>
                    <?php endif; ?>

                    <div class="cle-pricing-tabs-section__toggler">
                        <div class="cle-pricing-tabs-section__toggler-item cle-pricing-tabs-section__toggler-item_active"
                             data-tab="schools">Schools
                        </div>
                        <div class="cle-pricing-tabs-section__toggler-item" data-tab="partners">Partners</div>
                    </div>

                    <div class="cle-pricing-tabs-section__description">
                        <?php if (!empty($schools_subtitle)): ?>
                            <div class="cle-pricing-tabs-section__description-title"><?php echo $schools_subtitle ?></div>
                        <?php endif; ?>

                        <?php if (!empty($schools_description)): ?>
                            <div class="cle-pricing-tabs-section__description-text"><?php echo $schools_description ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="cle-pricing-tabs-section__bot cle-pricing-tabs-section__bot_v2">
                <div class="container">
                    <div class="cle-pricing-tabs-section__accordion">
                        <div class="cle-pricing-tabs-section__accordion-head">
                            <div class="cle-pricing-tabs-section__row">
                                <div class="cle-pricing-tabs-section__row-start"></div>
                                <div class="cle-pricing-tabs-section__row-end">
                                    <?php if (!empty($schools_table_head)): ?>
                                        <?php foreach ($schools_table_head as $head): ?>
                                            <div>
                                                <span><?php echo $head['name'] ?></span>
                                                <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $head['link'])); ?>
                                            </div>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>

                        <div class="cle-pricing-tabs-section__accordion-body">
                            <?php if (!empty($schools_table_body)): ?>
                                <?php foreach ($schools_table_body as $item):
                                    $title = $item['title'];
                                    $label = $item['label'];
                                    $features = $item['features'];
                                    ?>
                                    <div class="cle-pricing-tabs-section__accordion-item">
                                        <div class="cle-pricing-tabs-section__accordion-title">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="10"
                                                 viewBox="0 0 16 10" fill="none">
                                                <path d="M1 1.73438L8 8.73437L15 1.73437" stroke="#0A1E46"
                                                      stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                            </svg>
                                            <?php
                                            if (!empty($title)) {
                                                echo $title;
                                            }
                                            ?>
                                        </div>
                                        <div class="cle-pricing-tabs-section__accordion-content">
                                            <?php if (!empty($label)): ?>
                                                <div class="cle-pricing-tabs-section__label"><?php echo $label ?></div>
                                            <?php endif; ?>

                                            <?php if (!empty($features)): ?>
                                                <?php foreach ($features as $key => $feature):
                                                    $name = $feature['name'];
                                                    $items = $feature['items'];
                                                    $expandable = $feature['expandable'];
                                                    $rowsClasses = ['cle-pricing-tabs-section__row'];

                                                    if ($key % 2 === 0) {
                                                        $rowsClasses[] = 'cle-pricing-tabs-section__row_blue';
                                                    }

                                                    if (!empty($expandable) && $expandable) {
                                                        $rowsClasses[] = 'cle-pricing-tabs-section__row_expandable';
                                                    }
                                                    ?>
                                                    <div class="<?php echo implode(' ', $rowsClasses); ?>">
                                                        <div class="cle-pricing-tabs-section__row-start">
                                                            <?php if (!empty($expandable) && $expandable): ?>
                                                                <svg class="cle-pricing-tabs-section__row-arrow"
                                                                     width="16" height="10" viewBox="0 0 16 10"
                                                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M1 1.5L8 8.5L15 1.5" stroke="#1C1C1C"
                                                                          stroke-width="2" stroke-linecap="round"
                                                                          stroke-linejoin="round"/>
                                                                </svg>
                                                            <?php endif; ?>
                                                            <?php
                                                            if (!empty($name)) {
                                                                echo $name;
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="cle-pricing-tabs-section__row-end">
                                                            <?php if (!empty($items)): ?>
                                                                <?php foreach ($items as $item):
                                                                    $checked = $item['checked'];
                                                                    $text = $item['text'];
                                                                    ?>
                                                                    <div class="<?php echo !empty($text) ? 'cle-accordion-left' : '' ?>">
                                                                        <?php if (!empty($checked) && $checked): ?>
                                                                            <svg width="24" height="25"
                                                                                 viewBox="0 0 24 25" fill="none"
                                                                                 xmlns="http://www.w3.org/2000/svg">
                                                                                <rect class="cle-pricing-tabs-section__checkbox-bg"
                                                                                      x="0.4" y="1.16563" width="23.2"
                                                                                      height="23.2" rx="11.6"
                                                                                      fill="white"/>
                                                                                <path class="cle-pricing-tabs-section__checkbox-checkmark"
                                                                                      d="M17 8.76562L10 16.7656L7 13.7656"
                                                                                      stroke="#1464FF"
                                                                                      stroke-width="0.8"
                                                                                      stroke-miterlimit="10"
                                                                                      stroke-linecap="square"/>
                                                                                <rect x="0.4" y="1.16563" width="23.2"
                                                                                      height="23.2" rx="11.6"
                                                                                      stroke="#1464FF"
                                                                                      stroke-width="0.8"/>
                                                                            </svg>
                                                                        <?php endif; ?>

                                                                        <?php
                                                                        if (!empty($text)) {
                                                                            echo $text;
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <div class="cle-pricing-tabs-section__links">
                                <div class="cle-pricing-tabs-section__row">
                                    <div class="cle-pricing-tabs-section__row-start"></div>
                                    <div class="cle-pricing-tabs-section__row-end">
                                        <?php if (!empty($schools_table_head)): ?>
                                            <?php foreach ($schools_table_head as $head): ?>
                                                <div>
                                                    <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $head['link'])); ?>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cle-solutions-section cle-section-element-pad cle-section">
                <div class="container">
                    <div class="cle-solutions-section__wrap">
                        <h2 class="cle-solutions-section__title mb-75">Solutions for schools with advanced needs</h2>
                        <?php if (!empty($solutions_blocks)) : ?>
                            <div class="cle-solutions-section__cards">
                                <div class="row">
                                    <?php foreach ($solutions_blocks as $row):
                                        $title = $row['title'];
                                        $price_information = $row['price_information'];
                                        $description = $row['description'];
                                        $primary_button = $row['primary_button'];
                                        ?>
                                        <div class="col-lg-3 col-md-6 col-sm-12">
                                            <div class="cle-solutions-section__card">
                                                <div>
                                                    <?php if (!empty($title)): ?>
                                                        <h4 class="cle-solutions-section__card-title"><?php echo $title; ?></h4>
                                                    <?php endif; ?>
                                                    <?php if (!empty($price_information)): ?>
                                                        <h5 class="cle-solutions-section__card-price_information"><?php echo $price_information; ?></h5>
                                                    <?php endif; ?>
                                                    <?php if (!empty($description)): ?>
                                                        <p class="cle-solutions-section__card-description"><?php echo $description; ?></p>
                                                    <?php endif; ?>
                                                </div>
                                                <div>
                                                    <?php if (!empty($primary_button)): ?>
                                                        <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button)); ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="cle-pricing-tabs-section__tab" data-tab="partners">
            <div class="container">
                <div class="cle-pricing-tabs-section__top">
                    <?php if (!empty($partners_title)): ?>
                        <div class="cle-pricing-tabs-section__header">
                            <h1><?php echo $partners_title ?></h1>
                        </div>
                    <?php endif; ?>

                    <div class="cle-pricing-tabs-section__toggler">
                        <div class="cle-pricing-tabs-section__toggler-item" data-tab="schools">Schools</div>
                        <div class="cle-pricing-tabs-section__toggler-item cle-pricing-tabs-section__toggler-item_active"
                             data-tab="partners">Partners
                        </div>
                    </div>

                    <div class="cle-pricing-tabs-section__description">
                        <?php if (!empty($partners_subtitle)): ?>
                            <div class="cle-pricing-tabs-section__description-title"><?php echo $partners_subtitle ?></div>
                        <?php endif; ?>

                        <?php if (!empty($partners_description)): ?>
                            <div class="cle-pricing-tabs-section__description-text"><?php echo $partners_description ?></div>
                        <?php endif; ?>
                    </div>

                    <div class="cle-pricing-tabs-section__plans">
                        <div class="cle-courses-block-section">
                            <div class="container">
                                <?php if (!empty($course_block)) : ?>
                                    <div class="cle-courses-block-section__blocks">
                                        <div class="row">
                                            <?php foreach ($course_block as $row):
                                                $eyebrow = $row['eyebrow'];
                                                $title = $row['title'];
                                                $description = $row['description'];
                                                $description_second = $row['description_second'];
                                                $price_information = $row['price_information'];
                                                $primary_button = $row['primary_button'];
                                                $preferred = $row['preferred'];
                                                ?>
                                                <div class="col-lg-6 col-md-6 col-sm-12">
                                                    <div class="cle-courses-block-section__block <?php echo $preferred ? 'cle-courses-block-section_preferred' : '' ?>">
                                                        <?php if (!empty($eyebrow)): ?>
                                                            <p class="cle-courses-block-section__block-eyebrow"><?php echo $eyebrow; ?></p>
                                                        <?php endif; ?>
                                                        <div class="cle-courses-block-section__block-top-main">
                                                            <?php if (!empty($title)): ?>
                                                                <h4 class="cle-courses-block-section__block-title"><?php echo $title; ?></h4>
                                                            <?php endif; ?>
                                                            <?php if (!empty($description)): ?>
                                                                <p class="cle-courses-block-section__block-description"><?php echo $description; ?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="cle-courses-block-section__bottom-main">
                                                            <?php if (!empty($price_information)): ?>
                                                                <h5 class="cle-courses-block-section__block-price_information"><?php echo $price_information; ?></h5>
                                                            <?php endif; ?>
                                                            <?php if (!empty($description_second)): ?>
                                                                <p class="cle-courses-block-section__block-description"><?php echo $description_second; ?></p>
                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="cle-courses-block-section__block-btn">
                                                            <?php if (!empty($primary_button)): ?>
                                                                <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button)); ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="cle-pricing-tabs-section__bot">
                <div class="container">
                    <div class="cle-pricing-tabs-section__table">
                        <div class="cle-pricing-tabs-section__head">
                            <div class="cle-pricing-tabs-section__head-top">
                                <div class="cle-pricing-tabs-section__row">
                                    <div class="cle-pricing-tabs-section__row-start"></div>
                                    <div class="cle-pricing-tabs-section__row-end">
                                        <?php if (!empty($partners_table_head)): ?>
                                            <?php foreach ($partners_table_head as $head): ?>
                                                <div>
                                                    <?php echo $head['name'] ?>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="cle-pricing-tabs-section__head-bot">
                                Platform Features
                            </div>
                        </div>

                        <div class="cle-pricing-tabs-section__body">
                            <?php if (!empty($partners_table_body)): ?>
                                <?php foreach ($partners_table_body as $item):
                                    $label = $item['label'];
                                    $features = $item['features'];
                                    ?>
                                    <?php if (!empty($label)): ?>
                                    <div class="cle-pricing-tabs-section__label"><?php echo $label ?></div>
                                <?php endif; ?>

                                    <?php if (!empty($features)): ?>
                                    <?php foreach ($features as $key => $feature):
                                        $name = $feature['name'];
                                        $items = $feature['items'];
                                        $rowsClasses = ['cle-pricing-tabs-section__row'];

                                        if ($key % 2 === 0) {
                                            $rowsClasses[] = 'cle-pricing-tabs-section__row_blue';
                                        }
                                        ?>
                                        <div class="<?php echo implode(' ', $rowsClasses); ?>">
                                            <div class="cle-pricing-tabs-section__row-start">
                                                <?php
                                                if (!empty($name)) {
                                                    echo $name;
                                                }
                                                ?>
                                            </div>
                                            <div class="cle-pricing-tabs-section__row-end">
                                                <?php if (!empty($items)): ?>
                                                    <?php foreach ($items as $item):
                                                        $checked = $item['checked'];
                                                        ?>
                                                        <div>
                                                            <?php if (!empty($checked) && $checked): ?>
                                                                <svg width="24" height="25" viewBox="0 0 24 25"
                                                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <rect class="cle-pricing-tabs-section__checkbox-bg"
                                                                          x="0.4" y="1.16563" width="23.2" height="23.2"
                                                                          rx="11.6" fill="white"/>
                                                                    <path class="cle-pricing-tabs-section__checkbox-checkmark"
                                                                          d="M17 8.76562L10 16.7656L7 13.7656"
                                                                          stroke="#1464FF" stroke-width="0.8"
                                                                          stroke-miterlimit="10"
                                                                          stroke-linecap="square"/>
                                                                    <rect x="0.4" y="1.16563" width="23.2" height="23.2"
                                                                          rx="11.6" stroke="#1464FF"
                                                                          stroke-width="0.8"/>
                                                                </svg>
                                                            <?php endif; ?>
                                                        </div>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <div class="cle-pricing-tabs-section__links">
                                <div class="cle-pricing-tabs-section__row">
                                    <div class="cle-pricing-tabs-section__row-start"></div>
                                    <div class="cle-pricing-tabs-section__row-end">
                                        <?php if (!empty($partners_table_head)): ?>
                                            <?php foreach ($partners_table_head as $head): ?>
                                                <div>
                                                    <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $head['link'])); ?>
                                                </div>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
