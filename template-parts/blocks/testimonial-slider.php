<?php

/*
 * Block Name: Testimonial Slider
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-testimonial-slider';

$main_title = isset( $args['main_title'] ) ? $args['main_title'] : get_field('main_title');
$big_image = isset( $args['big_image'] ) ? $args['big_image'] : get_field('big_image');
$decoration_style = isset( $args['decoration_style'] ) ? $args['decoration_style'] : get_field('decoration_style');
$display_type = get_field('display_type');
$posts = get_field('posts');

$args = array(
    'post_type' => 'testimonials',
    'posts_per_page' => 10,
);

if ( $display_type == 'manually' && ! empty( $posts ) ) {
    $args['post__in'] = $posts;
    $args['orderby'] = 'post__in';
}

$the_query = new WP_Query( $args );

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
    $className[] = $block['className'];
}

if (!empty($block['align'])) {
    $className[] = 'align-' . $block['align'];
}

if (!empty($is_preview)) {
    $className[] = $block_name . '_is-preview';
}
$className[] = 'cle-section';
$className[] = 'cle-testimonial-slider_version-2';



 if ( $the_query->have_posts() ): ?>
    <section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr(trim(implode(' ', $className))) ?>">
        <?php if ( ! empty( $main_title ) ): ?>
            <h2 class="cle-testimonial-slider__main-title mb-80"><?php echo $main_title; ?></h2>
        <?php endif ?>
        <div class="cle-testimonial-slider__container">
            <div class="swiper">
                <div class="swiper-wrapper">
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post();
                        $t_id = get_the_ID();
                        $text = get_field( 'text', $t_id );
                        $style = get_field( 'style', $t_id );
                        $author = get_field( 'author', $t_id );
                        $button = get_field( 'button', $t_id );
                        ?>
                        <div class="swiper-slide">
                            <div class="cle-testimonial-slider__item">
                                <div class="cle-testimonial-slider__wrapper">
                                    <div class="container cle-testimonial-slider__item-container">
                                        <div class="cle-testimonial-slider__content">
                                            <div class="cle-testimonial-slider__start">
                                                <?php if ( $decoration_style == 'wave' ): ?>
                                                    <lottie-player
                                                      class="cle-testimonial-slider__img-shape"
                                                      mode="normal"
                                                      speed="1.5"
                                                      src="<?php echo V_TEMP_URL ?>/assets/lottie/line.json"
                                                    >
                                                    </lottie-player>
                                                <?php endif ?>
                                                <?php if ( $decoration_style == 'balls' ): ?>
                                                    <lottie-player
                                                      class="cle-testimonial-slider__img-shape"
                                                      mode="normal"
                                                      speed="1.5"
                                                      src="<?php echo V_TEMP_URL ?>/assets/lottie/ball.json"
                                                    >
                                                    </lottie-player>
                                                <?php endif ?>
                                                <?php if ( $decoration_style == 'quote' ): ?>
                                                    <svg class="cle-testimonial-slider__img-quote" width="135" height="109" viewBox="0 0 135 109" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M62.1592 25.4201L62.5628 25.3586L62.4931 24.9563L58.4424 1.58891L58.3775 1.21505L58.0008 1.26005C43.7735 2.95927 29.4185 7.7214 18.6169 17.8754C7.80483 28.0392 0.6 43.5613 0.6 66.6796V107.657V108.057H1H48.934H49.334V107.657V63.6317V63.2317H48.934H27.0687C27.4481 52.7964 29.6452 44.4041 34.936 38.1087C40.2938 31.7335 48.8792 27.4438 62.1592 25.4201ZM72.501 107.657V108.057H72.901H120.835H121.235V107.657V63.6317V63.2317H120.835H98.9697C99.3492 52.7964 101.546 44.4041 106.837 38.1087C112.195 31.7335 120.78 27.4438 134.06 25.4201L134.464 25.3586L134.394 24.9563L130.343 1.58891L130.279 1.21505L129.902 1.26005C115.674 2.95927 101.319 7.7214 90.5179 17.8754C79.7058 28.0392 72.501 43.5613 72.501 66.6796V107.657Z" stroke="#1464FF" stroke-width="0.8" />
                                                    </svg>
                                                <?php endif ?>
                                            </div>
                                            <div class="cle-testimonial-slider__end">
                                                <div class="cle-testimonial-slider__top">
                                                    <?php if ( $style == 'button' ): ?>
                                                        <h2 class="cle-testimonial-slider__quote"><?php the_title(); ?></h2>
                                                        <?php if ( ! empty( $text ) ): ?>
                                                            <div class="cle-testimonial-slider__description">
                                                                <p><?php echo $text; ?></p>
                                                            </div>
                                                        <?php endif ?>
                                                    <?php endif ?>

                                                    <?php if ( $style == 'author' ): ?>
                                                        <h2 class="cle-testimonial-slider__quote cle-testimonial-slider__quote-big"><?php echo $text; ?></h2>
                                                    <?php endif ?>

                                                    <?php if ( $style == 'button' && ! empty( $button ) ):
                                                        $link_target = ! empty( $button['target'] ) ? $button['target'] : '_self'; ?>
                                                        <div class="cle-testimonial-slider__btns">
                                                            <a href="<?php echo $button['url'] ?>" class="cle-btn cle-btn_primary" target="<?php echo esc_attr( $link_target ); ?>">
                                                                <?php echo $button['title'] ?>
                                                                <span class="icon">
                                                                    <svg width="29" height="12" viewBox="0 0 29 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <path d="M28.5303 6.53033C28.8232 6.23743 28.8232 5.76256 28.5303 5.46967L23.7574 0.696697C23.4645 0.403804 22.9896 0.403804 22.6967 0.696697C22.4038 0.989591 22.4038 1.46446 22.6967 1.75736L26.9393 6L22.6967 10.2426C22.4038 10.5355 22.4038 11.0104 22.6967 11.3033C22.9896 11.5962 23.4645 11.5962 23.7574 11.3033L28.5303 6.53033ZM6.55671e-08 6.75L28 6.75L28 5.25L-6.55671e-08 5.25L6.55671e-08 6.75Z" fill="white" />
                                                                    </svg>
                                                                </span>
                                                            </a>
                                                        </div>
                                                    <?php endif ?>
                                                </div>
                                                <div class="cle-testimonial-slider__bottom">
                                                    <?php if ( $style == 'author' ): ?>
                                                        <div class="cle-testimonial-slider__author">
                                                            <?php if ( ! empty( $author['image'] ) ): ?>
                                                                <div class="cle-testimonial-slider__author-poster">
                                                                    <img src="<?php echo $author['image']['url'] ?>" alt="<?php echo $author['image']['alt'] ?>">
                                                                </div>
                                                            <?php endif ?>
                                                            <div class="cle-testimonial-slider-info">
                                                                <?php if ( ! empty( $author['name'] ) ): ?>
                                                                    <div class="cle-testimonial-slider__author-name"><?php echo $author['name']; ?></div>
                                                                <?php endif ?>

                                                                <?php if ( ! empty( $author['position'] ) ): ?>
                                                                    <div class="cle-testimonial-slider__author-position"><?php echo $author['position']; ?></div>
                                                                <?php endif ?>
                                                            </div>
                                                        </div>
                                                    <?php endif ?>

                                                    <div class="cle-testimonial-slider__navigation">
                                                        <svg class="cle-testimonial-slider__prev" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" stroke="#1464FF" stroke-width="1.5"/>
                                                            <path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF"/>
                                                        </svg>
                                                        <svg class="cle-testimonial-slider__next" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" stroke="#1464FF" stroke-width="1.5"/>
                                                            <path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF"/>
                                                        </svg>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; wp_reset_postdata(); ?>
                </div>
            </div>
            <?php if ( ! empty( $big_image ) ): ?>
                <div class="cle-testimonial-slider__poster">
                    <img src="<?php echo $big_image['url']; ?>" alt="<?php echo $big_image['alt']; ?>" class="cle-testimonial-slider__big-img">
                </div>
            <?php endif ?>
        </div>
    </section>
<?php endif ?>