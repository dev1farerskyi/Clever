<?php

/*
 * Block Name: Tabs with Content Section
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-tabs-with-content-section';

$title = get_field('title');
$text = get_field('text');
$tabs = get_field('tabs');
$add_decoration = get_field('add_decoration');
$blue_style = get_field('blue_style');

// $youtube_video = get_field('youtube_video', false, false);

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
    $className[] = $block['className'];
}
if (!empty($block['align'])) {
    $className[] = 'align-' . $block['align'];
}
if (!empty($is_preview)) {
    $className[] = $block_name . '_is-preview';
}

if ($blue_style) {
    $className[] = 'cle-back-blue';
}

if ($add_decoration) {
    $className[] = 'decor-@@decorPosition';
} else {
    $className[] = 'decor-none';
}

$className[] = 'cle-section-element-pad';
$className[] = 'cle-section';
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr(trim(implode(' ', $className))) ?>">
    <?php if ($add_decoration): ?>
        <img class="cle-decor-1" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/decor-1.svg" alt="">
    <?php endif ?>
    <div class="container container-small">
        <div class="cle-tabs-with-content-section__wrap">
            <?php if (!empty($title) || !empty($text)): ?>
                <div class="cle-tabs-with-content-section__top mb-75">
                    <?php if (!empty($title)): ?>
                        <h2 class="cle-tabs-with-content-section__title"><?php echo $title; ?></h2>
                    <?php endif ?>
                    <?php if (!empty($text)): ?>
                        <div class="cle-tabs-with-content-section__description"><?php echo $text; ?></div>
                    <?php endif ?>
                </div>
            <?php endif ?>

            <?php if (!empty($tabs)):
                $counter = 0;
                $counter2 = 0; ?>
                <?php if (count($tabs) > 1): ?>
                <div class="cle-tabs-with-content-section__categories mb-75 js-tab-buttons">
                    <?php foreach ($tabs as $key => $tab):
                        $el_class = !$counter ? 'active' : ''; ?>
                        <button data-tab="<?php echo $key ?>"
                                class="<?php echo $el_class; ?>"><?php echo $tab['tab_title']; ?></button>
                        <?php
                        $counter++;
                    endforeach ?>
                </div>
            <?php endif ?>

                <div class="cle-tabs-with-content-section__main">
                    <div class="cle-tabs-with-content-section__tabs">
                        <?php while (have_rows('tabs')) : the_row();
                            $image = get_sub_field('image');
                            $text_title = get_sub_field('text_title');
                            $text = get_sub_field('text');
                            $youtube_video = get_sub_field('youtube_video', false);
                            $faq_items = get_sub_field('faq_items');
                            $button = get_sub_field('button');
                            $el_class = !$counter2 ? 'active' : '';
                            $content_class = !empty($image) ? 'col-lg-6 col-md-6' : 'col-lg-12 col-md-12'; ?>
                            <div class="cle-tabs-with-content-section__tab <?php echo $el_class; ?>"
                                 data-tab="<?php echo $counter2; ?>">
                                <div class="row">
                                    <div class="<?php echo $content_class; ?> col-sm-12">
                                        <div class="cle-tabs-with-content-section__main-content">
                                            <div>
                                                <?php if (!empty($text_title)): ?>
                                                    <h4 class="cle-tabs-with-content-section__main-content-title"><?php echo $text_title; ?></h4>
                                                <?php endif ?>
                                                <?php if (!empty($text)): ?>
                                                    <p class="cle-tabs-with-content-section__main-content-description"><?php echo $text; ?></p>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if (!empty($image)): ?>
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <?php if (!empty($youtube_video)): ?>
                                                <a class="cle-tabs-with-content-section__main-media" data-mfp-video
                                                   href="<?php echo $youtube_video; ?>">
                                                    <img src="<?php echo $image['url'] ?>"
                                                         alt="<?php echo $image['alt'] ?>">
                                                    <img class="play-icon"
                                                         src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/play.svg"
                                                         alt="">
                                                </a>
                                            <?php else: ?>
                                                <div class="cle-tabs-with-content-section__main-media">
                                                    <img src="<?php echo $image['url'] ?>"
                                                         alt="<?php echo $image['alt'] ?>">
                                                </div>
                                            <?php endif ?>
                                        </div>
                                    <?php endif ?>
                                </div>
                                <?php if (!empty($faq_items)): ?>
                                    <div class="cle-faq__accordion">
                                        <?php foreach ($faq_items as $item): ?>
                                            <div class="cle-accordion min_accordion">
                                                <div class="cle-accordion__head-wrap">
                                                    <h4 class="cle-accordion__head"><?php echo $item['title']; ?></h4>
                                                    <button type="button" class="cle-accordion__head-btn">
                                                        <span></span>
                                                        <span></span>
                                                    </button>
                                                </div>
                                                <p class="cle-accordion__body"><?php echo $item['text']; ?></p>
                                            </div>
                                        <?php endforeach ?>
                                    </div>
                                <?php endif ?>
                                <?php if ( ! empty( $button ) ): ?>
                                    <div class="cle-tabs-with-content-section__btn-count mt-75">
                                        <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $button)); ?>
                                    </div>
                                <?php endif ?>
                            </div>
                            <?php
                            $counter2++;
                        endwhile; ?>
                    </div>
                </div>
            <?php endif ?>


        </div>
    </div>
</div>