<?php

/*
 * Block Name: Video + Content Section
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-text-and-media';

$title = get_field('title');
$text  = get_field('text');
$poster_image  = get_field('poster_image');
$youtube_video = get_field('youtube_video', false, false);
$decor_position = get_field('decor_position');

$blue_background = get_field('blue_background');
$blue_background = ! empty( $args['blue_background'] ) ? $args['blue_background'] : $blue_background;

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
	$className[] = $block['className'];
}
if (!empty($block['align'])) {
	$className[] = 'align-' . $block['align'];
}
if (!empty($is_preview)) {
	$className[] = $block_name . '_is-preview';
}

$className[] = 'cle-section-element-pad';
$className[] = 'cle-section';
$className[] = 'decor-' . $decor_position;
$background = $blue_background ? 'cle-back-blue' : '';
?>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr(trim(implode(' ', $className))) ?> <?php echo $background; ?>">
    <div class="decor"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/decor-1.svg" alt=""></div>
	<div class="container-small">
		<div class="cle-text-and-media__wrap">
			<?php if ( ! empty( $title ) ): ?>
				<h1 class="cle-text-and-media__title"><?php echo $title; ?></h1>
			<?php endif ?>

			<?php if ( ! empty( $text ) ): ?>
				<h6 class="cle-text-and-media__description"><?php echo $text; ?></h6>
			<?php endif ?>

			<?php if ( ! empty( $poster_image ) && ! empty( $youtube_video ) ): ?>
				<div class="cle-text-and-media__image">
					<a href="<?php echo $youtube_video; ?>" class="cle-text-and-media__link" data-mfp-video></a>
					<img src="<?php echo $poster_image['url']; ?>" alt="<?php echo $poster_image['alt']; ?>">
					<img class="play-icon" src="<?php echo V_TEMP_URL ?>/assets/img/play.svg" alt="">
				</div>
			<?php endif ?>
		</div>
	</div>
</section>
