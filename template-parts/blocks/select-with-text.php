<?php

/*
 * Block Name: Select With Text
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-select-with-text';

$items = get_field('items');

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
	$className[] = $block['className'];
}
if (!empty($block['align'])) {
	$className[] = 'align-' . $block['align'];
}
if (!empty($is_preview)) {
	$className[] = $block_name . '_is-preview';
}

$className[] = 'cle-section';
$className[] = 'cle-section-element-pad';
?>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr(trim(implode(' ', $className))) ?>">
  <div class="container cle-select-with-text__container">
    <div class="cle-select-with-text__content">
      <?php if ( ! empty($items) ): ?>
        <div class="cle-select-with-text__top">
          <div class="cle-select-with-text__select">
            <select class="cle-select-with-text__select-wr">
              <?php foreach ($items as $item): 
                $label = $item['select_label'];
                $value = strtolower(str_replace(' ', '-', trim($label)));
              ?>
                <option value="<?php echo $value ?>"><?php echo $label ?></option>
              <?php endforeach; ?>
            </select>
            <svg class="cle-select-with-text__select-arrow" width="19" height="11" viewBox="0 0 19 11" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" clip-rule="evenodd" d="M2.97765 0.616119C2.48949 0.12796 1.69803 0.12796 1.20987 0.616119C0.72171 1.10428 0.72171 1.89574 1.20987 2.3839L8.70993 9.88396C9.19809 10.3721 9.98955 10.3721 10.4777 9.88396L17.9778 2.3839C18.4659 1.89574 18.4659 1.10428 17.9778 0.616119C17.4896 0.12796 16.6981 0.12796 16.21 0.616119L9.59382 7.23229L2.97765 0.616119Z" fill="#1464FF"/>
            </svg>
          </div>
        </div>

        <div class="cle-select-with-text_content">
          <?php foreach ($items as $key => $item):
            $value = strtolower(str_replace(' ', '-', trim($item['select_label'])));
            $title = $item['title'];
            $text = $item['text'];
            $image = $item['image'];
          ?>
            <div class="cle-select-with-text__tab <?php echo $key === 0 ? 'cle-select-with-text__tab_active' : '' ?>" data-tab="<?php echo $value ?>">
              <?php if ( ! empty($title) ): ?>
                <h2 class="cle-select-with-text__title"><?php echo $title ?></h2>
              <?php endif; ?>

              <?php if ( ! empty($text) ): ?>
                <div class="cle-select-with-text__text"><?php echo $text ?></div>
              <?php endif; ?>

              <?php if ( ! empty($image) ): ?>
                <div class="cle-select-with-text__poster">
                  <?php echo wp_get_attachment_image( $image['ID'], 'full' ); ?>
                </div>
              <?php endif; ?>
            </div>
          <?php endforeach; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</section>
