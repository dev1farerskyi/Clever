<?php

/*
 * Block Name: Aplication Gallery
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-application-gallery';

$title = get_field('title');
$style = ! empty( $args['style'] ) ? $args['style'] : get_field('style');
$decor_position = get_field('decor_position');
$blue_background = get_field('blue_background');
$blue_background = ! empty( $args['blue_background'] ) ? $args['blue_background'] : $blue_background;

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();
$className[] = $block_name;
$className[] = 'cle-section-element-pad';
$className[] = 'cle-section';
$className[] = 'decor-' . $decor_position;
$background = $blue_background ? 'cle-back-blue' : '';

$apps = cle_get_applications();

?>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo implode(' ', $className); ?> <?php echo $background; ?>">
    <div class="cle-application-gallery__wrap">
        <div class="decor"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/decor-1.svg" alt=""></div>
        <div class="container">
            <?php if ( $style == 'style-1' ): ?>
                <?php if ( ! empty( $title ) ): ?>
                    <div class="cle-application-gallery__top">
                        <h2 class="cle-application-gallery__title mb-75"><?php echo $title; ?></h2>
                    </div>
                <?php endif ?>

                <?php if ( ! empty( $apps ) ): ?>
                    <div class="row cle-application-gallery__items">
                        <?php for ($i=0; $i < 9; $i++) { ?>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="cle-application-gallery__item">
                                    <div class="cle-application-gallery__item-icon">
                                        <img src="<?php echo $apps[$i]['App Icon URL']; ?>" alt="<?php echo $apps[$i]['Title'] ?>">
                                    </div>
                                    <h4 class="cle-application-gallery__item-title">
                                        <a href="<?php echo get_home_url() . '/app-gallery/' . $apps[ $i ]['App Shortname'] . '/'; ?>">
                                            <?php echo $apps[$i]['Title'] ?>
                                        </a>
                                    </h4>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php endif ?>

            <?php elseif ( $style == 'style-2' ):
                $cats = cle_get_app_categories(); ?>
                <div class="cle-application-gallery__top">
                    <?php if ( ! empty( $title ) ): ?>
                        <h2 class="cle-application-gallery__title mb-75"><?php echo $title; ?></h2>
                    <?php endif ?>

                    <div class="cle-filters mb-75 js-apps-filter">
                        <div class="cle-filters-search">
                            <input class="cle-filters-search-input" type="search" id="site-search" name="search" placeholder="Search by keyword">
                            <button class="cle-filters-search-btn js-cpt-blocks-submit" type="search">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/search.svg" alt="">
                            </button>
                        </div>
                        <div class="cle-filters-select-wrap cle-filed-form">
                            <select name="select" class="filter-month">
                                <option value="all" selected="selected">Choose a category</option>
                                <?php foreach ($cats as $key => $cat): ?>
                                    <option value="<?php echo $key; ?>"><?php echo $cat; ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                </div>

                <?php if ( ! empty( $apps ) ): ?>
                    <script type="text/javascript">
                        var apps_list = <?php echo json_encode($apps); ?>;
                    </script>
                    <div class="row cle-application-gallery__items js-apps-list"></div>
                <?php endif ?>
            <?php endif ?>
        </div>
    </div>
</section>

