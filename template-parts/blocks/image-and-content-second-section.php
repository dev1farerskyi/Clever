<?php
/*
 * Block Name: Image And Content Second Section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$primary_button_top = get_field('primary_button_top');
$blue_button = get_field('blue_button');
$content_info = get_field('content_info');
$image_main = get_field('image_main');
$decor_position = get_field('decor_position');
$block_name = 'cle-image-and-content-second';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section-element-pad';
$className[] = 'cle-section';
$className[] = 'decor-' . $decor_position;
?>

<div class="<?php echo implode(' ', $className ); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="cle-image-and-content-second__wrap">
        <div class="decor"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/decor-1.svg" alt=""></div>
        <div class="container cle-image-and-content-second__container">
            <div class="cle-image-and-content-second__top">
                <?php if (!empty($title)) : ?>
                    <h2 class="cle-image-and-content-second__title"><?php echo $title; ?></h2>
                <?php endif ?>
                <?php if ( ! empty( $primary_button_top ) ): ?>
                    <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button_top)); ?>
                <?php endif; ?>
            </div>
            <div class="cle-image-and-content-second__content">
                <?php if (!empty($content_info)) : ?>
                    <div class="cle-image-and-content-second__start">
                        <?php echo $content_info; ?>
                    </div>
                <?php endif ?>
                <div class="cle-image-and-content-second__end">
                    <?php if (!empty($image_main)) : ?>
                        <div class="cle-image-and-content-second__image-col">
                            <div class="cle-image-and-content-second__image">
                                <div class="cle-image-and-content-second__el">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/el.svg" alt="">
                                </div>
                                <div class="cle-image-and-content-second__back-img">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/img-back-col.svg" alt="">
                                    <img class="cle-image-and-content-second__main-img" src="<?php echo esc_url($image_main['url']); ?>" alt="<?php echo esc_attr($image_main['alt']); ?>">
                                </div>
                            </div>
                            <?php if ( ! empty( $blue_button ) ): ?>
                                <div class="cle-image-and-content-second__btn">
                                    <?php get_template_part('template-parts/elements/blue-button', null, array('field' => $blue_button)); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>