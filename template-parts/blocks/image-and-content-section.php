<?php
/*
 * Block Name: Image + content section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$main_title = get_field('main_title');
$main_description = get_field('main_description');
$title = get_field('title');
$subtitle = get_field('subtitle');
$description  = get_field('description');
$primary_button = get_field('primary_button');
$third_button = get_field('third_button');
$primary_button_top = get_field('primary_button_top');
$image = get_field('image');
$shapes = get_field('shapes');
$decor_position = get_field('decor_position');
$none_decor_style = get_field('none_decor_style');
$alignment = get_field('alignment');

$decor_style = get_field('decor_style');
$pattern_style = get_field('pattern_style');

$blue_background = get_field('blue_background');
$blue_background = ! empty( $args['blue_background'] ) ? $args['blue_background'] : $blue_background;

$block_name = 'cle-image-and-content';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section-element-pad';
$className[] = 'img-' . $alignment;
$className[] = 'decor-' . $decor_position;
$className[] = 'cle-section';
$none_decor = $none_decor_style ? 'none-decor-style' : '';
$background = $blue_background ? 'cle-back-blue' : '';

$decor_styles = array(
    'style-1' => get_stylesheet_directory_uri() . '/assets/img/shapes-pattern-1.svg',
    'style-2' => get_stylesheet_directory_uri() . '/assets/img/shapes-pattern-2.svg',
    'style-3' => get_stylesheet_directory_uri() . '/assets/img/shapes-pattern-3.svg',
    'style-4' => get_stylesheet_directory_uri() . '/assets/img/shapes-pattern-4.svg',
    'style-5' => get_stylesheet_directory_uri() . '/assets/img/shapes-pattern-5.svg',
    'style-6' => get_stylesheet_directory_uri() . '/assets/img/shapes-pattern-6.svg',
    'style-7' => get_stylesheet_directory_uri() . '/assets/img/shapes-pattern-7.svg',
);

?>

<div class="<?php echo implode(' ', $className ); ?> <?php echo $background; ?> " id="<?php echo esc_attr($id); ?>">
    <div class="decor"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/decor-1.svg" alt=""></div>
    <div class="container">
        <div class="cle-image-and-content__top">
            <?php if (!empty($main_title)) : ?>
                <h2 class="cle-image-and-content__main-title"><?php echo $main_title; ?></h2>
            <?php endif ?>
            <?php if (!empty($main_description)) : ?>
                <h6 class="cle-image-and-content__main-description"><?php echo $main_description; ?></h6>
            <?php endif ?>
            <?php if ( ! empty( $primary_button_top ) ): ?>
                <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button_top)); ?>
            <?php endif; ?>
        </div>
        <div class="cle-image-and-content__wrap">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="cle-image-and-content__content">
                        <div>
                            <?php if (!empty($subtitle)) : ?>
                                <p class="cle-image-and-content__subtitle"><?php echo $subtitle; ?></p>
                            <?php endif ?>
                            <?php if (!empty($title)) : ?>
                            <h3 class="cle-image-and-content__title"><?php echo $title; ?></h3>
                            <?php endif ?>
                            <?php if (!empty($description)) : ?>
                            <p class="cle-image-and-content__description"><?php echo $description; ?></p>
                            <?php endif ?>
                            <?php if ( ! empty( $primary_button ) ): ?>
                                <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button)); ?>
                            <?php endif; ?>
                            <?php if ( ! empty( $third_button ) ): ?>
                                <?php get_template_part('template-parts/elements/third-button', null, array('field' => $third_button)); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="cle-image-and-content__image-wrap <?php echo $none_decor; ?>">
                        <?php if ( ! empty( $decor_style ) ): ?>
                            <img class="cle-image-and-content__image-el" src="<?php echo $decor_styles[ $decor_style ]; ?>" alt="">
                        <?php endif ?>
                        <?php if ( ! empty( $image ) ): ?>
                            <?php get_template_part('template-parts/svg-paterns/' . $pattern_style, null, array( 'img' => $image ) ) ?>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

