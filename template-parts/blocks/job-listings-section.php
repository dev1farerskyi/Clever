<?php
/*
 * Block Name: Job listings section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$style = get_field('style');
$title = get_field('title');
$text = get_field('text');
$button = get_field('button');
$block_name = 'cle-open-positions';

$departments = 'https://api.greenhouse.io/v1/boards/clever/departments';
$offices = 'https://api.greenhouse.io/v1/boards/clever/offices';
$jobs_link = 'https://api.greenhouse.io/v1/boards/clever/embed/jobs?content=true';

$jobs = file_get_contents($jobs_link);
$jobs_array = json_decode( $jobs, true );
// https://api.greenhouse.io/v1/boards/partstech/jobs?content=true

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);

$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$className[] = 'cle-section';
$className[] = 'cle-section-element-pad';

if ($style == 'small') {
    $className[] = 'cle-back-blue';
    $className[] = 'decor-top-right';
}

?>
<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <?php if ( $style == 'small' ): ?>
        <div class="decor"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/decor-1.svg" alt=""></div>
        <div class="container">
            <div class="cle-open-positions__wrap">
                <div class="cle-open-positions__top mb-75">
                    <?php if ( ! empty( $title ) ): ?>
                        <h2 class="cle-open-positions__main-title"><?php echo $title; ?></h2>
                    <?php endif ?>
                    <?php if ( ! empty( $text ) ): ?>
                        <h6 class="cle-open-positions__description"><?php echo $text; ?></h6>
                    <?php endif ?>
                </div>

                <?php if ( ! empty( $jobs_array['jobs'] ) ): ?>
                    <div class="cle-open-positions__cards">
                        <?php for ($i=0; $i < 3; $i++) { ?>
                            <?php if ( isset( $jobs_array['jobs'][ $i ] ) ):?>
                                <?php get_template_part('template-parts/jobs/item', null, array('job' => $jobs_array['jobs'][ $i ] ) ); ?>
                            <?php endif ?>
                        <?php } ?>
                    </div>
                <?php endif ?>

                <?php if ( ! empty( $button ) ):
                    $link_target = ! empty( $button['target'] ) ? $button['target'] : '_self'; ?>
                    <div class="cle-btn_cont mt-75">
                        <a href="<?php echo $button['url']; ?>" class="cle-btn cle-btn_primary" target="<?php echo esc_attr( $link_target ); ?>">
                            <?php echo $button['title']; ?>
                            <span class="icon">
                                <svg width="29" height="12" viewBox="0 0 29 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M28.5303 6.53033C28.8232 6.23743 28.8232 5.76256 28.5303 5.46967L23.7574 0.696697C23.4645 0.403804 22.9896 0.403804 22.6967 0.696697C22.4038 0.989591 22.4038 1.46446 22.6967 1.75736L26.9393 6L22.6967 10.2426C22.4038 10.5355 22.4038 11.0104 22.6967 11.3033C22.9896 11.5962 23.4645 11.5962 23.7574 11.3033L28.5303 6.53033ZM6.55671e-08 6.75L28 6.75L28 5.25L-6.55671e-08 5.25L6.55671e-08 6.75Z" fill="white" />
                                </svg>
                            </span>
                        </a>
                    </div>
                <?php endif ?>
            </div>
        </div>
    <?php endif ?>


    <?php if ( $style == 'big' ):
        $deps = cle_get_deps( $jobs_array['jobs'] );
         ?>
        <div class="container">
            <div class="cle-open-positions__wrap">
                <div class="cle-open-positions__top mb-75">
                    <?php if ( ! empty( $title ) ): ?>
                        <h2 class="cle-open-positions__main-title"><?php echo $title; ?></h2>
                    <?php endif ?>

                    <div class="cle-open-positions__top-wrap  js-jobs-filter width-84">
                        <div class="cle-filters-search">
                            <input class="cle-filters-search-input" type="search" id="site-search" name="search" placeholder="Search by keyword">
                            <button class="cle-filters-search-btn js-cpt-blocks-submit" type="search">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/search.svg" alt="">
                            </button>
                        </div>
                        <?php if ( ! empty( $deps ) ): ?>
                            <div class="cle-open-positions__filters-select cle-filed-form cle-filters-select-wrap">
                                <select name="select">
                                    <option value="all" selected="selected">Departments</option>
                                    <?php foreach ($deps as $key => $dep): ?>
                                        <option value="<?php echo $key ?>"><?php echo $dep ?></option>
                                    <?php endforeach ?>
                                </select>
                            </div>
                        <?php endif ?>
                    </div>
                </div>

                <?php if ( ! empty( $jobs_array['jobs'] ) ): ?>
                    <div class="cle-open-positions__cards">
                        <?php foreach ($jobs_array['jobs'] as $job): ?>
                            <?php get_template_part('template-parts/jobs/item', null, array( 'job' => $job ) ); ?>
                        <?php endforeach ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    <?php endif ?>
</div>