<?php

/*
 * Block Name: App slider
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-app-slider';
$title = get_field('title');
$primary_button = get_field('primary_button');
$applications_list = get_field('applications_list');


// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
$className[] = 'cle-section';
$className[] = 'cle-section-element-pad';
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo implode(' ', $className); ?>">
<!--    --><?php //if ( ! empty( $applications_list ) ): ?>
<!--        <div class="cle-app-slider__header mb-75">-->
<!--            --><?php //print_r($applications_list) ?>
<!--        </div>-->
<!--    --><?php //endif ?>
    <div class="container cle-app-slider-section__container">
        <div class="cle-app-slider__header mb-75">
            <?php if ( ! empty( $title ) ): ?>
                <div class="cle-app-slider__header-content">
                    <h2 class="cle-app-slider__title"><?php echo $title; ?></h2>
                </div>
            <?php endif ?>
            <div class="cle-app-slider__navigation slider-navigation">
                <svg class="cle-app-slider__prev" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" fill="#EDF5FF" stroke="#1464FF" stroke-width="1.5" />
                    <path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF" />
                </svg>
                <svg class="cle-app-slider__next" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" fill="#EDF5FF" stroke="#1464FF" stroke-width="1.5"/>
                    <path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF"/>
                </svg>
            </div>
        </div>

        <div class="cle-app-slider__list">
            <?php if ( ! empty( $applications_list ) ): ?>
                <div class="swiper">
                    <div class="swiper-wrapper">
                        <?php foreach ($applications_list as $app_id):
                            $app = cle_get_application_by_id( $app_id ); ?>
                            <div class="swiper-slide">
                                <div class="cle-app-slider__item">
                                    <?php if ( ! empty( $app['App Logo URL'] ) ): ?>
                                        <div class="cle-app-slider__image">
                                            <img src="<?php echo $app['App Logo URL'] ?>" alt="<?php echo $app['App Name'] ?>">
                                        </div>
                                    <?php endif ?>
                                    <div class="cle-app-slider__text">
                                        <h4 class="cle-app-slider__title">
                                            <a href="<?php echo get_home_url() . '/app-gallery/' . $app['App Shortname'] . '/'; ?>">
                                                <?php echo $app['App Name'] ?>
                                            </a>
                                        </h4>

                                        <?php if ( ! empty( $app['App Categories'] ) ):
                                            $cats = array(); 
                                            foreach ($app['App Categories'] as $cat) {
                                                $cats[] = cle_get_app_categories($cat);
                                            } ?>
                                            <p class="cle-app-slider__description">
                                                <?php echo implode(', ', $cats); ?>
                                            </p>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach ?>
                    </div>
                </div>
            <?php endif ?>
            <div class="cle-app-slider__btn-cont">
                <?php if ( ! empty( $primary_button ) ): ?>
                    <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button)); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

