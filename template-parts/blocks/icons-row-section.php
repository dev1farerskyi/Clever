<?php
/*
 * Block Name: Icons row section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = ! empty ( $args['title'] ) ? $args['title'] : get_field('title');
$items = ! empty ( $args['items'] ) ? $args['items'] : get_field('items');

$style = get_field('style');
$style = ! empty ( $args['style'] ) ? $args['style'] : $style;

$block_name = 'cle-icons-row-section';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section';
$className[] = 'cle-section-element'; ?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="cle-icons-row-section__wrap">
            <?php if (!empty($title)) : ?>
                <h2 class="cle-icons-row-section__title mb-75"><?php echo $title; ?></h2>
            <?php endif ?>
            <?php if ($style === 'dark-blue') : ?>
                <?php if (!empty($items)) : ?>
                    <div class="cle-icons-row-section__items back-dark-blue">
                        <?php foreach ($items as $row):
                            $icon = $row['icon'];
                            $title = $row['title'];
                            ?>
                            <div class="cle-icons-row-section__item">
                                <?php if (!empty($icon)) : ?>
                                    <img class="cle-icons-row-section__item-icon" src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
                                <?php endif ?>
                                <?php if (!empty($title)) : ?>
                                    <h5 class="cle-icons-row-section__item-title"><?php echo $title; ?></h5>
                                <?php endif ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif ?>
            <?php elseif ($style === 'light-blue') : ?>
                <?php if (!empty($items)) : ?>
                    <div class="cle-icons-row-section__items back-light-blue">
                        <?php foreach ($items as $row):
                            $icon = $row['icon'];
                            $title = $row['title'];
                        ?>
                        <div class="cle-icons-row-section__item">
                            <?php if (!empty($icon)) : ?>
                                <img class="cle-icons-row-section__item-icon" src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
                            <?php endif ?>
                            <?php if (!empty($title)) : ?>
                                <h5 class="cle-icons-row-section__item-title"><?php echo $title; ?></h5>
                            <?php endif ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif ?>
            <?php endif ?>
        </div>
    </div>
</div>