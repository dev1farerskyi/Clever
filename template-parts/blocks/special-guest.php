<?php
/*
 * Block Name: Special Guest
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$items = get_field('items');

$block_name = 'cle-special-guest-section';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section-element-pad';
$className[] = 'cle-section';
?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <img class="cle-decor-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/decor-1.svg" alt="">
    <div class="container">
        <div class="cle-special-guest-section__wrap">
            <?php if (!empty($title)) : ?>
                <h2 class="cle-special-guest-section__title mb-80"><?php echo $title; ?></h2>
            <?php endif ?>
            <?php if (!empty($items)) : ?>
                <div class="cle-special-guest-section__cards">
                    <div class="row">
                        <?php foreach ($items as $row):
                        $image = $row['image'];
                        $name = $row['name'];
                        $description = $row['description'];
                        $third_button = $row['third_button'];
                        ?>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="cle-special-guest-section__card">
                                    <?php if (!empty($image)): ?>
                                        <div class="cle-special-guest-section__card-img">
                                            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>">
                                        </div>
                                    <?php endif ?>
                                    <div class="cle-special-guest-section__card-text">
                                        <?php if (!empty($name)) : ?>
                                            <h4 class="cle-special-guest-section__card-name"><?php echo $name; ?></h4>
                                        <?php endif ?>
                                        <?php if (!empty($description)) : ?>
                                            <p class="cle-special-guest-section__card-description"><?php echo $description; ?></p>
                                        <?php endif ?>
                                        <?php if ( ! empty( $third_button) ): ?>
                                            <?php get_template_part('template-parts/elements/third-button', null, array('field' => $third_button)); ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
