<?php

/*
 * Block Name: Quick Links section
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-content-blocks-section';

$title = !empty ($args['title']) ? $args['title'] : get_field('title');
$links = !empty ($args['links']) ? $args['links'] : get_field('links');
$white_background = !empty ($args['white_background']) ? $args['white_background'] : get_field('white_background');


// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
	$className[] = $block['className'];
}
if (!empty($block['align'])) {
	$className[] = 'align-' . $block['align'];
}
if (!empty($is_preview)) {
	$className[] = $block_name . '_is-preview';
}

if ( ! $white_background ) {
	$className[] = 'cle-content-blocks-section_light-blue';
}
$className[] = 'cle-section';
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo implode(' ', $className); ?>">


<!-- 1 -->
	<div class="container cle-content-blocks-section__container">
		<div class="cle-content-blocks-section__header">
			<?php if ( ! empty( $title ) ): ?>
				<div class="cle-content-blocks-section__header-content">
					<h2 class="cle-content-blocks-section__title"><?php echo $title; ?></h2>
				</div>
			<?php endif ?>
			<div class="cle-content-blocks-section__navigation">
				<svg class="cle-content-blocks-section__prev" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
					<circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" fill="#EDF5FF" stroke="#1464FF" stroke-width="1.5" />
					<path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF" />
				</svg>
				<svg class="cle-content-blocks-section__next" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
					<circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" fill="#EDF5FF" stroke="#1464FF" stroke-width="1.5" />
					<path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF" />
				</svg>
			</div>
		</div>
		<?php if ( ! empty( $links ) ): ?>
			<div class="cle-content-blocks-section__list">
				<div class="swiper">
					<div class="swiper-wrapper">
						<?php foreach ($links as $link): ?>
							<div class="swiper-slide">
								<div class="cle-content-blocks-section__item">
									<div class="cle-content-blocks-section__textual">
										<?php if ( ! empty( $link['title'] ) ): ?>
											<h4 class="cle-content-blocks-section__name">
												<?php echo $link['title']; ?>
											</h4>
										<?php endif ?>
										<div class="cle-content-blocks-section__text">
											<?php if ( ! empty( $link['text'] ) ): ?>
												<div class="cle-content-blocks-section__description">
													<p><?php echo $link['text']; ?></p>
												</div>
											<?php endif ?>

											<?php if ( ! empty( $link['button'] ) ):
												$link_target = ! empty( $link['button']['target'] ) ? $link['button']['target'] : '_self'; ?>
												<a href="<?php echo $link['button']['url']; ?>" class="cle-content-blocks-section__link" target="<?php echo esc_attr( $link_target ); ?>">
													<?php echo $link['button']['title']; ?>
													<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
														<circle cx="15" cy="15" r="14.25" fill="white" stroke="#0A1E46" stroke-width="1.5" />
														<path d="M20.5303 15.5303C20.8232 15.2374 20.8232 14.7626 20.5303 14.4697L15.7574 9.6967C15.4645 9.40381 14.9896 9.40381 14.6967 9.6967C14.4038 9.98959 14.4038 10.4645 14.6967 10.7574L18.9393 15L14.6967 19.2426C14.4038 19.5355 14.4038 20.0104 14.6967 20.3033C14.9896 20.5962 15.4645 20.5962 15.7574 20.3033L20.5303 15.5303ZM9 15.75L20 15.75L20 14.25L9 14.25L9 15.75Z" fill="#0A1E46" />
													</svg>
												</a>
											<?php endif ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach ?>
					</div>
					<div class="cle-content-blocks-section__bottom">
						<div class="cle-content-blocks-section__navigation">
							<svg class="cle-content-blocks-section__prev" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
								<circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" fill="#EDF5FF" stroke="#1464FF" stroke-width="1.5" />
								<path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF" />
							</svg>
							<svg class="cle-content-blocks-section__next" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
								<circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" fill="#EDF5FF" stroke="#1464FF" stroke-width="1.5" />
								<path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF" />
							</svg>
						</div>
					</div>
				</div>
			</div>
		<?php endif ?>
	</div>
</div>
