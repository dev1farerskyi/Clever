<?php
/*
 * Block Name: Logo Section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$logos = get_field('logos');
$title = get_field('title');
$description = get_field('description');
$style = get_field('style');
$block_name = 'cle-logo-section';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section';
$className[] = 'cle-section-element'; ?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="cle-logo-section__wrap">
            <div class="cle-logo-section__content">
                <?php if (!empty($title)) : ?>
                    <h2 class="cle-logo-section__title"><?php echo $title; ?></h2>
                <?php endif ?>
                <?php if (!empty($description)) : ?>
                    <h6 class="cle-logo-section__description"><?php echo $description; ?></h6>
                <?php endif ?>
            </div>
            <?php if ( $style == 'all-logos' ): ?>
                <?php if (!empty($logos)) : ?>
                    <div class="cle-logo-section__logos">
                        <?php foreach ($logos as $row):
                        $logo = $row['logo'];
                        $company_name = $row['company_name'];
                        ?>
                        <div class="cle-logo-section__item">
                            <?php if (!empty($logo)): ?>
                                <img class="cle-logo-section__logo" src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>">
                            <?php endif ?>
                            <?php if (!empty($company_name)) : ?>
                                <h5 class="cle-logo-section__name-company"><?php echo $company_name; ?></h5>
                            <?php endif ?>
                        </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
                <!--                                    <div class="cle-logo-section__logos">-->
            <?php elseif ($style === 'slider-logos') : ?>
                <?php if (!empty($logos)) : ?>
                    <div class="cle-logo-section__list">
                        <div class="swiper">
                            <div class="swiper-wrapper">
                                <?php foreach ($logos as $row):
                                    $logo = $row['logo'];
                                    $company_name = $row['company_name'];
                                    ?>
                                    <div class="swiper-slide">
                                        <div class="cle-logo-section__item">
                                            <?php if (!empty($logo)): ?>
                                                <img class="cle-logo-section__logo" src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['alt']); ?>">
                                            <?php endif ?>
                                            <?php if (!empty($company_name)) : ?>
                                                <h5 class="cle-logo-section__name-company"><?php echo $company_name; ?></h5>
                                            <?php endif ?>
                                        </div>
                                    </div>
                               <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            <?php endif ?>
        </div>
    </div>
</div>
