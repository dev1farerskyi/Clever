<?php

/*
 * Block Name: One Post section
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$items = get_field('post_item');
$items = !empty ($args['items']) ? $args['items'] : $items;

$block_name = 'cle-one-post';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$args = array(
    'post_status' => 'publish',
    'posts_per_page' => 1,
    'orderby' => 'title',
    'post__in' => $items,
    'post_type' => array('post', 'events'),
);

$items_query = new WP_Query($args);

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
$className[] = 'cle-section-element-pad-small';
$className[] = 'cle-section';
?>

<?php
if ($items_query->have_posts()) : ?>
    <div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr(trim(implode(' ', $className))) ?>">
        <div class="container">
            <div class="cle-one-post__wrap">
                <?php if ( ! empty( $title ) ): ?>
                    <h2 class="cle-one-post__main-title mb-80"><?php echo $title; ?></h2>
                <?php endif ?>
                <?php while ($items_query->have_posts()) : $items_query->the_post(); ?>
                    <?php get_template_part('template-parts/elements/one-post-element'); ?>
                <?php endwhile;
                wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
<?php endif; ?>