<?php

/*
 * Block Name: Tabs With Text
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-tabs-with-text';

$tabs = get_field('tabs');

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
	$className[] = $block['className'];
}
if (!empty($block['align'])) {
	$className[] = 'align-' . $block['align'];
}
if (!empty($is_preview)) {
	$className[] = $block_name . '_is-preview';
}

$className[] = 'cle-section';
?>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr(trim(implode(' ', $className))) ?>">
    <div class="container cle-tabs-with-text__container">
		<?php if ( ! empty($tabs) ): ?>
			<div class="cle-tabs-with-text__tabs">
				<?php foreach( $tabs as $key => $tab ): 
					$tab_name = $tab['tab_name'];
					$value = strtolower(str_replace(' ', '-', trim($tab_name)));
				?>
					<div class="cle-tabs-with-text__tab <?php echo $key === 0 ? 'cle-tabs-with-text__tab_active' : '' ?>" data-tab="<?php echo $value ?>"><?php echo $tab_name ?></div>
				<?php endforeach; ?>
			</div>

			<div class="cle-tabs-with-text__content">
				<?php foreach( $tabs as $key => $item ): 
					$value = strtolower(str_replace(' ', '-', trim($item['tab_name'])));
					$text = $item['text'];
					$primary_button = $item['primary_button'];
				?>
					<div class="cle-tabs-with-text__item <?php echo $key === 0 ? 'cle-tabs-with-text__item_active' : '' ?>" data-tab="<?php echo $value ?>">
						<?php if ( !empty($text) ): ?>
							<div class="cle-tabs-with-text__textual">
								<?php echo $text ?>
							</div>
						<?php endif; ?>

						<?php if ( !empty($primary_button) ): ?>
							<div class="cle-tabs-with-text__more">
								<?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button)); ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
</section>
