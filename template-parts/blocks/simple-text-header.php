<?php
/*
 * Block Name: Simple Text Header
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

//$title = get_field('title');

$style = get_field('style');
$style = ! empty ( $args['style'] ) ? $args['style'] : $style;

$title = ! empty ( $args['title'] ) ? $args['title'] : get_field('title');
$subtitle = ! empty ( $args['subtitle'] ) ? $args['subtitle'] : get_field('subtitle');
$description = ! empty ( $args['description'] ) ? $args['description'] : get_field('description');
$show_share_links = ! empty ( $args['show_share_links'] ) ? $args['show_share_links'] : get_field('show_share_links');

$primary_button = get_field('primary_button');
$tabs = get_field('tabs');

$block_name = 'cle-simple-text-header';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section';
?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <?php if ($style === 'two-columns') : ?>
        <div class="container">
            <img class="cle-decor-circle-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/decor-circle-1.svg" alt="">
            <img class="cle-decor-circle-2" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/decor-circle-2.svg" alt="">
            <div class="cle-simple-text-header__wrap">
                <div class="row">
                    <div class="col-lg-7 col-md-12 col-sm-12">
                        <?php if (!empty($title)) : ?>
                            <div class="cle-simple-text-header__title-wrap">
                                <h1 class="cle-simple-text-header__title js-title-with-highlight"><?php echo $title; ?></h1>
                            </div>
                        <?php endif ?>
                    </div>
                    <div class="col-lg-5 col-md-12 col-sm-12">
                        <?php if (!empty($description)) : ?>
                            <div class="cle-simple-text-header__text">
                                <p class="cle-simple-text-header__description"><?php echo $description; ?></p>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ($style === 'all-width') : ?>
        <img class="cle-decor-circle-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/decor-circle-1.svg" alt="">
        <img class="cle-decor-circle-2" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/decor-circle-2.svg" alt="">
        <div class="container">
            <div class="cle-simple-text-header__wrap cle-text-center @@contSmall">
                <?php if (!empty($subtitle)) : ?>
                <div class="cle-simple-text-header__subtitle">
                    <span><?php echo $subtitle; ?></span>
                </div>
                <?php endif ?>
                <?php if (!empty($title)) : ?>
                    <h1 class="cle-simple-text-header__title js-title-with-highlight"><?php echo $title; ?></h1>
                <?php endif ?>
                <?php if (!empty($description)) : ?>
                    <p class="cle-simple-text-header__description"><?php echo $description; ?></p>
                <?php endif ?>
                <?php if (!empty($show_share_links) && $show_share_links) : ?>
                    <?php get_template_part('template-parts/elements/share-links'); ?>
                <?php endif ?>
            </div>
        </div>
    <?php elseif ($style === 'all-width-button') : ?>
        <img class="cle-decor-circle-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/decor-circle-1.svg" alt="">
        <img class="cle-decor-circle-2" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/decor-circle-2.svg" alt="">
        <div class="container">
            <div class="cle-simple-text-header__wrap cle-text-center">
                <?php if (!empty($title)) : ?>
                    <h1 class="cle-simple-text-header__title js-title-with-highlight"><?php echo $title; ?></h1>
                <?php endif ?>
                <?php if ( ! empty( $primary_button ) ): ?>
                    <div class="cle-simple-text-header__btn-cont">
                        <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button)); ?>
                    </div>
                <?php endif ?>
            </div>
        </div>
    <?php elseif ($style === 'categories-with-find') : ?>
        <img class="cle-decor-circle-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/decor-circle-1.svg" alt="">
        <img class="cle-decor-circle-2" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/decor-circle-2.svg" alt="">
        <div class="container">
            <div class="cle-simple-text-header__wrap cle-text-center">
                <?php if (!empty($title)) : ?>
                    <h1 class="cle-simple-text-header__title js-title-with-highlight"><?php echo $title; ?></h1>
                <?php endif ?>
                <div class="cle-simple-text-header__filters-search">
                    <input class="cle-simple-text-header__filters-search-input" type="search" id="site-search2" name="search" placeholder="Search by keyword">
                    <a class="cle-simple-text-header__filters-search-btn" type="search">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/search.svg" alt="">
                    </a>
                </div>
            </div>
        </div>
        <?php if (!empty($tabs)) : ?>
            <div class="cle-simple-text-header__categories-cont">
                <div class="cle-simple-text-header__categories">
                    <?php foreach ($tabs as $row):
                        $tab = $row['tab'];
                        ?>
                       <div class="cle-simple-text-header__categories-link">
                           <a href="<?php echo esc_url( $tab['url'] ); ?>"><?php echo $tab['title']; ?></a>
                       </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endif ?>
</div>