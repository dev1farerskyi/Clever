<?php
/*
 * Block Name: App Detail section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = ! empty ( $args['title'] ) ? $args['title'] : get_field('title');
$description = ! empty ( $args['description'] ) ? $args['description'] : get_field('description');
$image = ! empty ( $args['image'] ) ? $args['image'] : get_field('image');
$list = ! empty ( $args['list'] ) ? $args['list'] : get_field('list');
$primary_button = ! empty ( $args['primary_button'] ) ? $args['primary_button'] : get_field('primary_button');
$second_button = ! empty ( $args['second_button'] ) ? $args['second_button'] : get_field('second_button');
$third_button = ! empty ( $args['third_button'] ) ? $args['third_button'] : get_field('third_button');

$block_name = 'cle-app-detail-section';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section-element';
$className[] = 'cle-section';

$id = isset( $block['id'] ) ? $block_name . '-' . $block['id'] : $block_name;
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}
?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="cle-app-detail-section__wrap">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <?php if (!empty($image)) : ?>
                        <div class="cle-app-detail-section__image">
                            <img class="" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>">
                        </div>
                    <?php endif ?>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="cle-app-detail-section__content">
                        <?php if (!empty($title)) : ?>
                            <h3 class="cle-app-detail-section__title"><?php echo $title; ?></h3>
                        <?php endif ?>
                        <?php if (!empty($description)) : ?>
                            <p class="cle-app-detail-section__description"><?php echo $description; ?></p>
                        <?php endif ?>
                        <?php if (!empty($list)) : ?>
                            <div class="cle-app-detail-section__items">
                                <?php foreach ($list as $row):
                                $icon = $row['icon'];
                                $description = $row['description'];
                                ?>
                                    <div class="cle-app-detail-section__item">
                                        <?php if (!empty($icon)): ?>
                                            <img class="cle-app-detail-section__item-logo" src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
                                        <?php endif ?>
                                        <?php if (!empty($description)) : ?>
                                            <p class="cle-app-detail-section__item-description">
                                                <?php echo $description; ?>
                                            </p>
                                        <?php endif ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        <?php endif ?>

                        <div class="cle-app-detail-section__btn-cont">
                            <?php if ( ! empty( $primary_button ) ): ?>
                                <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button)); ?>
                            <?php endif ?>
                            <div class="cle-app-detail-section__btn-cont-col">
                                <?php if ( ! empty( $third_button ) ): ?>
                                    <?php get_template_part('template-parts/elements/third-button', null, array('field' => $third_button)); ?>
                                <?php endif ?>

                                <?php if ( ! empty( $second_button ) ): ?>
                                    <?php get_template_part('template-parts/elements/third-button', null, array('field' => $second_button)); ?>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
