<?php
/*
 * Block Name: Simple content section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$items = get_field('items');

$block_name = 'cle-simple-content-section';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section';
$className[] = 'cle-section-element';

?>

<div class="<?php echo implode(' ', $className ); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if (!empty($items)) : ?>
            <div class="cle-simple-content-section__items">
                <?php foreach ($items as $row):
                $title = $row['title'];
                $text = $row['text'];
                $primary_button = $row['primary_button'];
                ?>
                    <div class="cle-simple-content-section__item mb-75">
                        <?php if (!empty($title)) : ?>
                            <h3 class="cle-simple-content-section__title"><?php echo $title; ?></h3>
                        <?php endif ?>
                        <?php if (!empty($text)) : ?>
                            <p class="cle-simple-content-section__text"><?php echo $text; ?></p>
                        <?php endif ?>
                        <?php if ( ! empty( $primary_button ) ): ?>
                            <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button)); ?>
                        <?php endif ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif ?>
    </div>
</div>

