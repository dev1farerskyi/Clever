<?php

/*
 * Block Name: Brand CTA Section
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-brand-cta-section';

$title = get_field('title');
$text = get_field('text');
$button = get_field('button');
$lottieUrl = get_field('lottie');
$third_button = get_field('third_button');
$background_color = get_field('background_color');
$background_color = ! empty( $args['background_color'] ) ? $args['background_color'] : $background_color;

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
	$className[] = $block['className'];
}
if (!empty($block['align'])) {
	$className[] = 'align-' . $block['align'];
}
if (!empty($is_preview)) {
	$className[] = $block_name . '_is-preview';
}
$className[] = 'cle-section';
$background = '';

if ($background_color === 'light-blue') {
	$background = 'cle-brand-cta-section__wrap_light';
}
?>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo implode(' ', $className); ?>">
  	<div class="cle-brand-cta-section__trigger"></div>
  	<div class="container">
		<div class="cle-brand-cta-section__wrap <?php echo $background; ?>">
			<div class="cle-brand-cta-section__bg"></div>
			<div class="row">
				<?php if ( ! empty( $lottieUrl ) ): ?>
					<div class="col-lg-5 col-md-5 col-sm-12">
						<div class="cle-brand-cta-section__animation">
							<lottie-player
								mode="normal"
								speed="1.5"
								src="<?php echo $lottieUrl ?>"
							>
							</lottie-player>
						</div>
					</div>
				<?php endif ?>
				<div class="col-lg-7 col-md-7 col-sm-12">
					<div class="cle-brand-cta-section__text">
						<?php if ( ! empty( $title ) ): ?>
							<h2 class="cle-brand-cta-section__title"><?php echo $title; ?></h2>
						<?php endif ?>

						<?php if ( ! empty( $text ) ): ?>
							<h6 class="cle-brand-cta-section__description"><?php echo $text; ?></h6>
						<?php endif ?>

						<?php if ( ! empty( $button ) ):
							$link_target = ! empty( $button['target'] ) ? $button['target'] : '_self'; ?>
							<a href="<?php echo $button['url']; ?>" class="cle-btn cle-btn_primary" target="<?php echo $link_target; ?>">
								<?php echo $button['title']; ?>
								<span class="icon">
									<svg width="29" height="12" viewBox="0 0 29 12" fill="none" xmlns="http://www.w3.org/2000/svg">
										<path d="M28.5303 6.53033C28.8232 6.23743 28.8232 5.76256 28.5303 5.46967L23.7574 0.696697C23.4645 0.403804 22.9896 0.403804 22.6967 0.696697C22.4038 0.989591 22.4038 1.46446 22.6967 1.75736L26.9393 6L22.6967 10.2426C22.4038 10.5355 22.4038 11.0104 22.6967 11.3033C22.9896 11.5962 23.4645 11.5962 23.7574 11.3033L28.5303 6.53033ZM6.55671e-08 6.75L28 6.75L28 5.25L-6.55671e-08 5.25L6.55671e-08 6.75Z" fill="white"/>
									</svg>
								</span>
							</a>
						<?php endif ?>
                        <?php if ( ! empty( $third_button ) ): ?>
                            <?php get_template_part('template-parts/elements/third-button-white', null, array('field' => $third_button)); ?>
                        <?php endif; ?>
					</div>
				</div>
			</div>
		</div>
  	</div>
</section>
