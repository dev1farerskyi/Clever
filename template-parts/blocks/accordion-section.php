<?php
/*
 * Block Name: Accordion section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$style = get_field('style');
$style = ! empty ( $args['style'] ) ? $args['style'] : $style;
$decor_position = get_field('decor_position');
$block_name = 'cle-accordion-section';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);

$className[] = 'cle-section-element-pad';

$title = get_field('title');
$description  = get_field('description');
$image = get_field('image');
$image_variants = get_field('image_variants');
$background_shape  = get_field('background_shape');
$accordion = get_field('accordion');
$with_blue_background = get_field('with_blue_background');
$blue_button = get_field('blue_button');

if ($with_blue_background) {
    $className[] = 'cle-accordion-section_with-blue-bg';

}

$className[] = 'cle-section';
$className[] = 'decor-' . $decor_position;
?>

<section class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="decor"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/decor-1.svg" alt=""></div>
    <div class="container">
        <div class="cle-accordion-section__top">
            <?php if ( ! empty($title) ): ?>
                <h2 class="cle-accordion-section__main-title"><?php echo $title ?></h2>
            <?php endif; ?>

            <?php if ( ! empty($description) ): ?>
                <h6 class="cle-accordion-section__main-description"><?php echo $description ?></h6>
            <?php endif; ?>
        </div>
        <div class="cle-accordion-section__wrap">
            <div class="row">
                <?php if ( ! empty($image) ): ?>
                    <?php if ( ! empty($image_variants) ): ?>
                        <?php if ( $image_variants === 'with-big-image' ): ?>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="cle-accordion-section__image">
                                    <img class="cle-accordion-section__image-main none-border" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>">
                                </div>
                            </div>
                        <?php elseif ( $image_variants === 'with-image-and-a-shape' ):
                            $imageClasses = ['cle-accordion-section__image-main'];
                            $shapeClasses = ['cle-accordion-section__image'];

                            if ( ! empty ($background_shape) ) {
                                if ($background_shape === 'variant-1') {
                                    $shapeClasses[] = 'shapes-v1';
                                } elseif ($background_shape === 'variant-2') {
                                    $shapeClasses[] = 'shapes-v2';
                                    $imageClasses[] = 'border-image';
                                }
                            }
                            ?>
                            <div class="col-lg-6 col-md-12 col-sm-12">
                                <div class="<?php echo implode(' ', $shapeClasses); ?>">
                                    <div class="cle-accordion-section__poster">
                                        <img class="<?php echo implode(' ', $imageClasses); ?>" src="<?php echo $image['url'] ?>" alt="<?php echo esc_attr($image['alt']); ?>">
                                    </div>
                                </div>
                            </div>
                        <?php elseif ($image_variants === 'with-image-in-laptop'): ?>
                             <?php if ( ! empty($image) ):
                                $url = esc_url($image['url']);
                                $alt = esc_url($image['alt']); ?>
                                <div class="cle-accordion-section__img-in-laptop">
                                    <img src="<?php echo $url ?>" alt="<?php echo $alt ?>" class="cle-accordion-section__img">
                                    <img class="cle-accordion-section__laptop" src="<?php echo get_template_directory_uri() . '/assets/img/pattern-laptop-light.svg'; ?>" alt="">
                                    <?php if ( ! empty( $blue_button ) ): ?>
                                        <?php get_template_part('template-parts/elements/blue-button', null, array('field' => $blue_button)); ?>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif; ?>

                <div class="col-lg-6 col-md-12 col-sm-12">
                    <?php if ( !empty($accordion) ): ?>
                        <div class="cle-accordion-section__accordion">
                            <?php foreach ( $accordion as $item ):
                                $title = $item['title'];
                                $description = $item['description'];
                                $link = $item['link'];
                                ?>
                                <div class="cle-accordion min_accordion">
                                    <div class="cle-accordion-section__head">
                                        <?php if (  ! empty($title) ): ?>
                                            <h4 class="cle-accordion-section__title"><?php echo $title ?></h4>
                                        <?php endif; ?>
                                        <button type="button" class="cle-accordion-section__head-btn">
                                            <span></span>
                                            <span></span>
                                        </button>
                                    </div>
                                    <div class="cle-accordion-section__body">
                                        <?php if ( ! empty($description) ): ?>
                                            <p class="cle-accordion-section__body-text"><?php echo $description ?></p>
                                        <?php endif; ?>
                                        <?php if ( ! empty($link) ):
                                            $link_url = $link['url'];
                                            $link_title = $link['title'];
                                            $link_target = $link['target'] ? $link['target'] : '_self';
                                            ?>
                                            <div class="cle-accordion-section__body-link">
                                                <a href="<?php echo $link_url ?>" target="<?php echo $link_target ?>">
                                                    <?php echo $link_title ?>
                                                    <img class="cle-accordion-section__body-link-icon" src="<?php echo V_TEMP_URL ?>/assets/img/link-btn.svg" alt="">
                                                </a>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>