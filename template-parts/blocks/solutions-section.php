<?php
/*
 * Block Name: Solutions section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$custom_solutions = get_field('custom_solutions');
$solutions_blocks = get_field('theme_solutions_blocks', 'option');

if ($custom_solutions) {
    $solutions_blocks = get_field('solutions_blocks');
}

$block_name = 'cle-solutions-section';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section-element-pad';
$className[] = 'cle-section';
?>

<div class="<?php echo implode(' ', $className ); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="cle-solutions-section__wrap">
            <?php if (!empty($title)) : ?>
                <h2 class="cle-solutions-section__title mb-75"><?php echo $title; ?></h2>
            <?php endif; ?>
            <?php if (!empty($solutions_blocks)) : ?>
                <div class="cle-solutions-section__cards">
                    <div class="row">
                        <?php foreach ($solutions_blocks as $row):
                        $title = $row['title'];
                        $price_information = $row['price_information'];
                        $description = $row['description'];
                        $primary_button = $row['primary_button'];
                        ?>
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <div class="cle-solutions-section__card">
                                    <div>
                                        <?php if (!empty($title)): ?>
                                            <h4 class="cle-solutions-section__card-title"><?php echo $title; ?></h4>
                                        <?php endif; ?>
                                        <?php if (!empty($price_information)): ?>
                                            <h5 class="cle-solutions-section__card-price_information"><?php echo $price_information; ?></h5>
                                        <?php endif; ?>
                                        <?php if (!empty($description)): ?>
                                            <p class="cle-solutions-section__card-description"><?php echo $description; ?></p>
                                        <?php endif; ?>
                                    </div>
                                    <div>
                                        <?php if ( ! empty( $primary_button ) ): ?>
                                            <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button)); ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>


