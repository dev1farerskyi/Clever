<?php

/*
 * Block Name: Flexible Page Header
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-flexible-page-header';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
	$className[] = $block['className'];
}
if (!empty($block['align'])) {
	$className[] = 'align-' . $block['align'];
}
if (!empty($is_preview)) {
	$className[] = $block_name . '_is-preview';
}

$title = get_field('title');
$description = get_field('description');
$primary_btn = get_field('primary_button');
$secondary_btns = get_field('secondary_buttons');
$btns_variants = get_field('button_variants');
$image_variants = get_field('image_variants');
$image_in_a_shape = get_field('image_in_a_shape');
$image = get_field('image');
$youtube_video = get_field('youtube_video', false, false);
$is_cover_image = get_field('is_cover_image');
$top_left_shape = get_field('top_left_shape');
$top_right_shape = get_field('top_right_shape');
$bottom_left_shape = get_field('bottom_left_shape');
$bottom_right_shape = get_field('bottom_right_shape');
$bottom_shape = get_field('bottom_shape');
$blue_button = get_field('blue_button');

if ( ! empty($bottom_shape) ) {
  if ($bottom_shape === 'bottom-left') {
    $className[] = 'cle-flexible-page-header_with-left-bottom-shape';
  } else if ($bottom_shape === 'bottom-right') {
    $className[] = 'cle-flexible-page-header_with-right-bottom-shape';
  }
}

if ($btns_variants === 'with-big-buttons') {
  $className[] = 'cle-flexible-page-header_with-big-buttons';
}

if ($image_variants === 'with-image-and-a-shape') {
  $className[] = 'cle-flexible-page-header_with-image-and-shape';
}

if ($image_variants === 'with-image-in-laptop') {
    $className[] = 'cle-flexible-page-header_with-image-in-laptop';
}

if ($image_variants === 'with-image-in-laptop-2') {
    $className[] = 'cle-flexible-page-header_with-image-in-laptop';
    $className[] = 'cle-flexible-page-header_with-image-in-laptop-2';
}
$className[] = 'cle-section';
?>
<section id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr(trim(implode(' ', $className))) ?>" start-from-hidden>
<div class="container cle-flexible-page-header__container">
  <div class="cle-flexible-page-header__content">
    <div class="cle-flexible-page-header__start">
        <?php if ( !empty($title) ): ?>
          <h1 class="cle-flexible-page-header__title js-title-with-highlight"><?php echo $title ?></h1>
        <?php endif; ?>

        <?php if ( ! empty($description) ): ?>
          <div class="cle-flexible-page-header__text"><?php echo $description ?></div>
        <?php endif; ?>
        
        <?php if ( ! empty($btns_variants) ): ?>
          <?php if ( $btns_variants === 'with-small-buttons' ): ?>
            <div class="cle-flexible-page-header__btns">
              <?php if ( ! empty($primary_btn) ):
                $link_url = $primary_btn['url'];
                $link_title = $primary_btn['title'];
                $link_target = $primary_btn['target'] ? $link['target'] : '_self';
              ?>
                <a href="<?php echo $link_url ?>" class="cle-btn cle-btn_primary" target="<?php echo $link_target ?>">
                  <?php echo $link_title ?>
                  <span class="icon">
                    <svg width="29" height="12" viewBox="0 0 29 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path d="M28.5303 6.53033C28.8232 6.23743 28.8232 5.76256 28.5303 5.46967L23.7574 0.696697C23.4645 0.403804 22.9896 0.403804 22.6967 0.696697C22.4038 0.989591 22.4038 1.46446 22.6967 1.75736L26.9393 6L22.6967 10.2426C22.4038 10.5355 22.4038 11.0104 22.6967 11.3033C22.9896 11.5962 23.4645 11.5962 23.7574 11.3033L28.5303 6.53033ZM6.55671e-08 6.75L28 6.75L28 5.25L-6.55671e-08 5.25L6.55671e-08 6.75Z" fill="white" />
                    </svg>
                  </span>
                </a>
              <?php endif; ?>

              <?php if ( ! empty($secondary_btns) ): ?>
                <?php foreach($secondary_btns as $btn): 
                  $link_url = $btn['link']['url'];
                  $link_title = $btn['link']['title'];
                  $link_target = $btn['link']['target'] ? $btn['link']['target'] : '_self';
                  $has_white_style = $btn['has_white_style'];
                ?>
                  <a href="<?php echo $link_url ?>" class="<?php echo $has_white_style ? 'cle-btn cle-btn_secondary' : 'cle-secondary-btn'?>" target="<?php echo $link_target ?>">
                    <?php echo $link_title ?>
                    <?php if ($has_white_style): ?>
                      <span class="icon">
                        <svg width="29" height="12" viewBox="0 0 29 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M28.5303 6.53033C28.8232 6.23743 28.8232 5.76256 28.5303 5.46967L23.7574 0.696697C23.4645 0.403804 22.9896 0.403804 22.6967 0.696697C22.4038 0.989591 22.4038 1.46446 22.6967 1.75736L26.9393 6L22.6967 10.2426C22.4038 10.5355 22.4038 11.0104 22.6967 11.3033C22.9896 11.5962 23.4645 11.5962 23.7574 11.3033L28.5303 6.53033ZM6.55671e-08 6.75L28 6.75L28 5.25L-6.55671e-08 5.25L6.55671e-08 6.75Z" fill="#004EE4" />
                        </svg>
                      </span>
                    <?php else: ?>
                      <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="15" cy="15" r="14.25"/>
                        <path d="M20.5303 15.5303C20.8232 15.2374 20.8232 14.7626 20.5303 14.4697L15.7574 9.6967C15.4645 9.40381 14.9896 9.40381 14.6967 9.6967C14.4038 9.98959 14.4038 10.4645 14.6967 10.7574L18.9393 15L14.6967 19.2426C14.4038 19.5355 14.4038 20.0104 14.6967 20.3033C14.9896 20.5962 15.4645 20.5962 15.7574 20.3033L20.5303 15.5303ZM9 15.75L20 15.75L20 14.25L9 14.25L9 15.75Z"/>
                      </svg> 
                    <?php endif; ?>             
                  </a>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
          <?php elseif ($btns_variants === 'with-big-buttons'): ?>
            <div class="cle-flexible-page-header__big-btns">
              <?php if ( ! empty($primary_btn) ):
                  $link_url = $primary_btn['url'];
                  $link_title = $primary_btn['title'];
                  $link_target = $primary_btn['target'] ? $link['target'] : '_self';
                ?>
                <a href="<?php echo $link_url ?>" class="cle-flexible-page-header__link" target="<?php echo $link_target ?>">
                  <?php echo $link_title ?>
                  <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="15" cy="15" r="14.25" />
                    <path d="M20.5303 15.5303C20.8232 15.2374 20.8232 14.7626 20.5303 14.4697L15.7574 9.6967C15.4645 9.40381 14.9896 9.40381 14.6967 9.6967C14.4038 9.98959 14.4038 10.4645 14.6967 10.7574L18.9393 15L14.6967 19.2426C14.4038 19.5355 14.4038 20.0104 14.6967 20.3033C14.9896 20.5962 15.4645 20.5962 15.7574 20.3033L20.5303 15.5303ZM9 15.75L20 15.75L20 14.25L9 14.25L9 15.75Z"/>
                  </svg>              
                </a>
              <?php endif; ?>
              <?php if ( ! empty($secondary_btns) ): ?>
                <?php foreach($secondary_btns as $btn): 
                  $link_url = $btn['link']['url'];
                  $link_title = $btn['link']['title'];
                  $link_target = $btn['link']['target'] ? $btn['link']['target'] : '_self';
                  $has_white_style = $btn['has_whtie_style'];
                ?>
                  <a href="<?php echo $link_url ?>" class="cle-flexible-page-header__link" target="<?php echo $link_target ?>">
                    <?php echo $link_title ?>
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <circle cx="15" cy="15" r="14.25" />
                      <path d="M20.5303 15.5303C20.8232 15.2374 20.8232 14.7626 20.5303 14.4697L15.7574 9.6967C15.4645 9.40381 14.9896 9.40381 14.6967 9.6967C14.4038 9.98959 14.4038 10.4645 14.6967 10.7574L18.9393 15L14.6967 19.2426C14.4038 19.5355 14.4038 20.0104 14.6967 20.3033C14.9896 20.5962 15.4645 20.5962 15.7574 20.3033L20.5303 15.5303ZM9 15.75L20 15.75L20 14.25L9 14.25L9 15.75Z"/>
                    </svg>              
                  </a>
                <?php endforeach; ?>
              <?php endif; ?>
            </div>
          <?php endif; ?>
        <?php endif; ?>
      </div>
      <div class="cle-flexible-page-header__end <?php echo $is_cover_image ? 'cle-flexible-page-header__end_full-size' : '' ?>">
        <?php if ( ! empty($image_variants) ): ?>
          <?php if ($image_variants === 'with-big-image'): ?>
            <?php if ( ! empty($image) ): 
                $url = esc_url($image['url']);
                $alt = esc_url($image['alt']); ?>
                <img src="<?php echo $url ?>" alt="<?php echo $alt ?>" class="cle-flexible-page-header__img">
              <?php endif; ?>
            <?php elseif ($image_variants === 'with-image-in-laptop'): ?>
                <?php if ( ! empty($image) ):
                    $url = esc_url($image['url']);
                    $alt = esc_url($image['alt']); ?>
                    <div class="cle-flexible-page-header__img-in-laptop">
                        <img src="<?php echo $url ?>" alt="<?php echo $alt ?>" class="cle-flexible-page-header__img">
                        <img class="cle-flexible-page-header__laptop" src="<?php echo get_template_directory_uri() . '/assets/img/pattern-monitor-light.svg'; ?>" alt="">
                        <?php if ( ! empty( $blue_button ) ): ?>
                            <?php get_template_part('template-parts/elements/blue-button', null, array('field' => $blue_button)); ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            <?php elseif ($image_variants === 'with-image-in-laptop-2'): ?>
                <?php if ( ! empty($image) ):
                    $url = esc_url($image['url']);
                    $alt = esc_url($image['alt']); ?>
                    <div class="cle-flexible-page-header__img-in-laptop cle-flexible-page-header__img-in-laptop-2">
                        <img src="<?php echo $url ?>" alt="<?php echo $alt ?>" class="cle-flexible-page-header__img">
                        <img class="cle-flexible-page-header__laptop" src="<?php echo get_template_directory_uri() . '/assets/img/pattern-laptop-light.svg'; ?>" alt="">
                        <?php if ( ! empty( $blue_button ) ): ?>
                            <?php get_template_part('template-parts/elements/blue-button', null, array('field' => $blue_button)); ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            <?php elseif ($image_variants === 'with-image-and-a-shape'):
            $shapesClasses = ['cle-flexible-page-header__images'];

            if ( !$image_in_a_shape && !$is_cover_image ) {
              $shapesClasses[] = 'cle-flexible-page-header__images_with-usual-image';
            }

            if ( !$image_in_a_shape && $is_cover_image ) {
              $shapesClasses[] = 'cle-flexible-page-header__images_with-covered-image';
            }
          ?>
            <div class="<?php echo esc_attr(trim(implode(' ', $shapesClasses))) ?>">
              <?php if ( ! empty($top_left_shape) ):
                $url = esc_url($top_left_shape['url']);
                $alt = esc_url($top_left_shape['alt']); ?>
                <div class="cle-flexible-page-header__shape-tl">
                  <svg class="cle-flexible-page-header__starburst" width="95" height="95" viewBox="0 0 95 95" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <line x1="48.194" y1="0.933105" x2="48.194" y2="26.2089" stroke="#0A1E46"/>
                    <line x1="48.1941" y1="69.5391" x2="48.1941" y2="94.8149" stroke="#0A1E46"/>
                    <line x1="24.657" y1="6.97192" x2="37.2949" y2="28.8614" stroke="#0A1E46"/>
                    <line x1="58.9595" y1="66.3862" x2="71.5974" y2="88.2757" stroke="#0A1E46"/>
                    <line x1="7.29175" y1="23.9706" x2="29.1813" y2="36.6085" stroke="#0A1E46"/>
                    <line x1="66.7069" y1="58.2733" x2="88.5964" y2="70.9112" stroke="#0A1E46"/>
                    <line y1="-0.5" x2="25.2758" y2="-0.5" transform="matrix(-0.5 0.866025 0.866025 0.5 71.4476 7.71997)" stroke="#0A1E46"/>
                    <line y1="-0.5" x2="25.2758" y2="-0.5" transform="matrix(-0.5 0.866025 0.866025 0.5 37.1445 67.1345)" stroke="#0A1E46"/>
                    <line y1="-0.5" x2="25.2758" y2="-0.5" transform="matrix(-0.866025 0.5 0.5 0.866025 88.3466 24.4036)" stroke="#0A1E46"/>
                    <line y1="-0.5" x2="25.2758" y2="-0.5" transform="matrix(-0.866025 0.5 0.5 0.866025 28.9314 58.707)" stroke="#0A1E46"/>
                    <line x1="0.753296" y1="47.374" x2="26.0291" y2="47.374" stroke="#0A1E46"/>
                    <line x1="69.3594" y1="47.374" x2="94.6352" y2="47.374" stroke="#0A1E46"/>
                  </svg>
                  <img src="<?php echo $url ?>" alt="<?php echo $alt ?>">            
                </div>
              <?php endif; ?>

              <?php if ( ! empty($top_right_shape) ):
                $url = esc_url($top_right_shape['url']);
                $alt = esc_url($top_right_shape['alt']); ?>
                <img src="<?php echo $url ?>" alt="<?php echo $alt ?>" class="cle-flexible-page-header__shape-tr">
              <?php endif; ?>
              
              <?php if ( ! empty($image) ): 
                $url = esc_url($image['url']);
                $alt = esc_url($image['alt']);
                ?>
                <div class="cle-flexible-page-header__images-poster">
                  <img src="<?php echo $url ?>" alt="<?php echo $alt ?>">
                  <?php if ( ! empty($youtube_video) ): ?>
                    <a href="<?php echo $youtube_video ?>" class="cle-flexible-page-header__images-video" data-mfp-video></a>
                  <?php endif; ?>
                </div>
              <?php endif; ?>

              <?php if ( ! empty($bottom_left_shape) ):
                $url = esc_url($bottom_left_shape['url']);
                $alt = esc_url($bottom_left_shape['alt']); ?>
                <img src="<?php echo $url ?>" alt="<?php echo $alt ?>" class="cle-flexible-page-header__shape-bl">
              <?php endif; ?>

              <?php if ( ! empty($bottom_right_shape) ):
                $url = esc_url($bottom_right_shape['url']);
                $alt = esc_url($bottom_right_shape['alt']); ?>
                <img src="<?php echo $url ?>" alt="<?php echo $alt ?>" class="cle-flexible-page-header__shape-br">
              <?php endif; ?>
            </div>
          <?php endif; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
