<?php

/*
 * Block Name: Trust Content section
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-trust-content-section';

$style = get_field('style');
$title = get_field('title');
$description = get_field('description');
$cards = get_field('cards');
$cards_s2 = get_field('cards_s2');

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
    $className[] = $block['className'];
}
if (!empty($block['align'])) {
    $className[] = 'align-' . $block['align'];
}
if (!empty($is_preview)) {
    $className[] = $block_name . '_is-preview';
}
$className[] = 'cle-section';
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo implode(' ', $className); ?>">
	<?php if ( $style == 'style-1' ): ?>
		<div class="container">
			<div class="cle-trust-content-section__content">
				<?php if ( ! empty( $title ) || ! empty( $description ) ): ?>
					<div class="cle-trust-content-section__header">
						<?php if ( ! empty( $title ) ): ?>
							<h2 class="cle-trust-content-section__title"><?php echo $title; ?></h2>
						<?php endif ?>
						<?php if ( ! empty( $description ) ): ?>
							<div class="cle-trust-content-section__description"><?php echo $description; ?></div>
						<?php endif ?>
					</div>
				<?php endif ?>

				<?php if ( ! empty( $cards ) ): ?>
					<div class="cle-trust-content-section__list">
						<?php foreach ($cards as $card): ?>
							<div class="cle-trust-content-section__item">
								<div class="cle-trust-content-section__textual">
									<?php if ( ! empty( $card['title'] ) ): ?>
										<div class="cle-trust-content-section__name"><?php echo $card['title'] ?></div>
									<?php endif ?>

									<?php if ( ! empty( $card['text'] ) ): ?>
										<div class="cle-trust-content-section__text"><?php echo $card['text']; ?></div>
									<?php endif ?>

									<?php if ( ! empty( $card['icons'] ) ):
										$icons_class = count( $card['icons'] ) == 1 ? '' : 'cle-trust-content-section__icons_three-in-row'; ?>
										<div class="cle-trust-content-section__icons <?php echo $icons_class; ?>">
											<?php foreach ($card['icons'] as $icon): ?>
												<div class="cle-trust-content-section__icon">
													<img src="<?php echo $icon['url'] ?>" alt="<?php echo $icon['alt'] ?>" class="cle-trust-content-section__img">
												</div>
											<?php endforeach ?>
										</div>
									<?php endif ?>
								</div>
								<?php if ( ! empty( $card['link'] ) ):
									$link_target = ! empty( $card['link']['target'] ) ? $card['link']['target'] : '_self'; ?>
									<a href="<?php echo $card['link']['url']; ?>" class="cle-trust-content-section__link" target="<?php echo esc_attr( $link_target ); ?>">
										<div class="cle-trust-content-section__link-text">
											<?php echo $card['link']['title']; ?>
											<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
												<circle cx="15" cy="15" r="14.25" fill="white" stroke="#0A1E46" stroke-width="1.5" />
												<path d="M20.5303 15.5303C20.8232 15.2374 20.8232 14.7626 20.5303 14.4697L15.7574 9.6967C15.4645 9.40381 14.9896 9.40381 14.6967 9.6967C14.4038 9.98959 14.4038 10.4645 14.6967 10.7574L18.9393 15L14.6967 19.2426C14.4038 19.5355 14.4038 20.0104 14.6967 20.3033C14.9896 20.5962 15.4645 20.5962 15.7574 20.3033L20.5303 15.5303ZM9 15.75L20 15.75L20 14.25L9 14.25L9 15.75Z" fill="#0A1E46" />
											</svg>
										</div>
										<div class="cle-trust-content-section__label"></div>
									</a>
								<?php endif ?>
							</div>
						<?php endforeach ?>
					</div>
				<?php endif ?>
			</div>
		</div>
	<?php endif ?>

	<?php if ( $style == 'style-2' && ! empty( $cards_s2 ) ): ?>
		<div class="cle-trust-content-section">
			<div class="container">
				<div class="cle-trust-content-section__content">
					<div class="cle-trust-content-section__header">
					</div>
					<div class="cle-trust-content-section__list">
						<?php foreach ($cards_s2 as $card): ?>
							<div class="cle-trust-content-section__item">
								<div class="cle-trust-content-section__textual">
									<?php if ( ! empty( $card['title'] ) ): ?>
										<div class="cle-trust-content-section__name"><?php echo $card['title']; ?></div>
									<?php endif ?>

									<?php if ( ! empty( $card['text'] ) ): ?>
										<div class="cle-trust-content-section__text"><?php echo $card['text']; ?></div>
									<?php endif ?>

									<?php if ( ! empty( $card['icon'] ) ): ?>
										<div class="cle-trust-content-section__icons">
											<div class="cle-trust-content-section__icon">
												<img src="<?php echo $card['icon']['url']; ?>" alt="<?php echo $card['icon']['alt']; ?>" class="cle-trust-content-section__img">
											</div>
										</div>
									<?php endif ?>
								</div>
							</div>
						<?php endforeach ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif ?>
</div>


