<?php
/*
 * Block Name: Frequently Asked Questions
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$questions = get_field('questions');

$blue_background = get_field('blue_background');
$blue_background = ! empty( $args['blue_background'] ) ? $args['blue_background'] : $blue_background;

$block_name = 'cle-faq';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section-element-pad';
$className[] = 'cle-section';
$background = $blue_background ? 'cle-back-blue' : '';
?>

<div class="<?php echo implode(' ', $className ); ?> <?php echo $background; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="cle-faq__wrap">
            <?php if (!empty($title)) : ?>
                <h2 class="cle-faq__title mb-75"><?php echo $title; ?></h2>
            <?php endif ?>
            <?php if (!empty($questions)) : ?>
                <div class="cle-faq__accordion">
                    <?php foreach ($questions as $row):
                    $title = $row['title'];
                    $text = $row['text'];
                    ?>
                        <div class="cle-accordion min_accordion">
                            <?php if (!empty($title)) : ?>
                                <div class="cle-accordion__head-wrap">
                                    <h4 class="cle-accordion__head"><?php echo $title; ?></h4>
                                    <button type="button" class="cle-accordion__head-btn">
                                        <span></span>
                                        <span></span>
                                    </button>
                                </div>
                            <?php endif ?>
                            <?php if (!empty($text)) : ?>
                            <p class="cle-accordion__body">
                                <?php echo $text; ?>
                            </p>
                            <?php endif ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
