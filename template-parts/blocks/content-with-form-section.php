<?php

/*
 * Block Name: Content With Form Section
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'cle-about-the-webinar-section';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
    $className[] = $block['className'];
}
if (!empty($block['align'])) {
    $className[] = 'align-' . $block['align'];
}
if (!empty($is_preview)) {
    $className[] = $block_name . '_is-preview';
}

$className[] = 'cle-section-element-pad';
$className[] = 'cle-section';

$title = get_field('title');
$description = get_field('description');
$form = get_field('form');
$form_title = get_field('form_title');
$form_description = get_field('form_description');
?>
<section id="<?php echo esc_attr($id); ?>"
         class="<?php echo esc_attr(trim(implode(' ', $className))) ?>">
    <div class="container">
        <div class="cle-about-the-webinar-section__wrap">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="cle-about-the-webinar-section__content">
                        <?php if ( ! empty($title) ): ?>
                            <h2 class="cle-about-the-webinar-section__title js-title-with-highlight"><?php echo $title; ?></h2>
                        <?php endif; ?>

                        <?php if ( ! empty($description) ): ?>
                            <div class="cle-about-the-webinar-section__description">
                                <?php echo $description; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>


                <?php if (!empty($form)): ?>
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <div class="cle-contact-form__wrap cle-filed-form">
                            <?php if ( ! empty($form_title) ): ?>
                                <h4 class="cle-contact-form__title"><?php echo $form_title; ?></h4>
                            <?php endif; ?>
                            <?php if ( ! empty($form_description) ): ?>
                                <p class="cle-contact-form__description"><?php echo $form_description; ?></p>
                            <?php endif; ?>
                            <?php echo do_shortcode('[gravityform id="' . $form['id'] . '" ajax="true" title="false" description="false"]'); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>