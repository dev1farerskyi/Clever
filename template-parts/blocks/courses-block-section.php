<?php
/*
 * Block Name: Courses block section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$custom_courses = get_field('custom_courses');
$course_block = get_field('theme_course_block', 'option');

if ($custom_courses) {
    $course_block = get_field('course_block');
}


$block_name = 'cle-courses-block-section';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section-element-pad';
$className[] = 'cle-section';

?>

<div class="<?php echo implode(' ', $className ); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if (!empty($title)) : ?>
            <h2 class="cle-courses-block-section__title"><?php echo $title; ?></h2>
        <?php endif; ?>
        <?php if (!empty($course_block)) : ?>
            <div class="cle-courses-block-section__blocks">
                <div class="row">
                    <?php foreach ($course_block as $row):
                        $eyebrow = $row['eyebrow'];
                        $title = $row['title'];
                        $description = $row['description'];
                        $description_second = $row['description_second'];
                        $price_information = $row['price_information'];
                        $primary_button = $row['primary_button'];
                        $preferred = $row['preferred'];
                        ?>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="cle-courses-block-section__block <?php echo $preferred ? 'cle-courses-block-section_preferred' : '' ?>">
                                <?php if (!empty($eyebrow)): ?>
                                    <p class="cle-courses-block-section__block-eyebrow"><?php echo $eyebrow; ?></p>
                                <?php endif; ?>
                                <div class="cle-courses-block-section__block-top-main">
                                    <?php if (!empty($title)): ?>
                                        <h4 class="cle-courses-block-section__block-title"><?php echo $title; ?></h4>
                                    <?php endif; ?>
                                    <?php if (!empty($description)): ?>
                                        <p class="cle-courses-block-section__block-description"><?php echo $description; ?></p>
                                    <?php endif; ?>
                                </div>
                                <div class="cle-courses-block-section__bottom-main">
                                    <?php if (!empty($price_information)): ?>
                                        <h5 class="cle-courses-block-section__block-price_information"><?php echo $price_information; ?></h5>
                                    <?php endif; ?>
                                    <?php if (!empty($description_second)): ?>
                                        <p class="cle-courses-block-section__block-description"><?php echo $description_second; ?></p>
                                    <?php endif; ?>
                                </div>
                                <div class="cle-courses-block-section__block-btn">
                                    <?php if ( ! empty( $primary_button ) ): ?>
                                        <?php get_template_part('template-parts/elements/primary-button', null, array('field' => $primary_button)); ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>