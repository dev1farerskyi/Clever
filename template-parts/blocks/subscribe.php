<?php
/*
 * Block Name: Subscribe
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$subscribe_form = get_field('subscribe_form');

$block_name = 'cle-subscribe';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section';
?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="cle-subscribe__wrap">
            <h3 class="cle-subscribe__title"><?php esc_html_e( 'Subscribe to receive news and updates from Clever.', V_PREFIX); ?></h3>
            <?php if (!empty($subscribe_form)) : ?>
                <div class="cle-subscribe__info">
                    <?php echo do_shortcode('[gravityform id="' . $subscribe_form['id'] . '" ajax="true" title="false" description="false"]'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
