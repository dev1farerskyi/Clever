<?php
/*
 * Block Name: Content blocks section
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$description  = get_field('description');
$decor_position = get_field('decor_position');
$blue_background = get_field('blue_background');
$blue_background = ! empty( $args['blue_background'] ) ? $args['blue_background'] : $blue_background;
$primary_button = get_field('primary_button');
$secondary_button = get_field('secondary_button');


$style = get_field('header_style');
$style = ! empty ( $args['style'] ) ? $args['style'] : $style;

$blocks = get_field('blocks');

$card_style = get_field('card_style');

$swiperClass = '';

$block_name = 'cle-content-blocks-section';

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);

if ( ! empty($card_style ) && $card_style === 'with-hover') {
    $className[] = 'cle-content-blocks-section_items-version-2';
}

if ($style === 'centered') {
    $className[] = 'cle-content-blocks-section_centered-header';
}
$className[] = 'cle-section';
$className[] = 'decor-' . $decor_position;
$background = $blue_background ? 'cle-back-blue' : '';
?>
<div class="<?php echo implode(' ', $className ); ?> <?php echo $background; ?>" id="<?php echo esc_attr($id); ?>">
    <div class="cle-content-blocks-section__wrap">
        <div class="decor"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/decor-1.svg" alt=""></div>
        <div class="container cle-content-blocks-section__container">
            <div class="cle-content-blocks-section__header">
                <div class="cle-content-blocks-section__header-content">
                    <?php if (!empty($title)) : ?>
                        <h2 class="cle-content-blocks-section__title"><?php echo $title; ?></h2>
                    <?php endif ?>
                    <?php if (!empty($description)) : ?>
                        <div class="cle-content-blocks-section__subtitle"><?php echo $description; ?></div>
                    <?php endif ?>
                    <?php if (!empty($primary_button || $secondary_button)) : ?>
                    <div class="cle-content-blocks-section__btns">
                        <?php if ( ! empty( $primary_button ) ):
                            $primary_button_url = $primary_button['url'];
                            $primary_button_title = $primary_button['title'];
                            $primary_button_target = $primary_button['target'] ? $link['target'] : '_self';
                        ?>
                        <a href="<?php echo esc_url( $primary_button_url ); ?>" target="<?php echo $primary_button_target ?>" class="cle-btn cle-btn_primary">
                            <?php echo esc_html( $primary_button_title ); ?>
                            <span class="icon">
                              <svg width="29" height="12" viewBox="0 0 29 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M28.5303 6.53033C28.8232 6.23743 28.8232 5.76256 28.5303 5.46967L23.7574 0.696697C23.4645 0.403804 22.9896 0.403804 22.6967 0.696697C22.4038 0.989591 22.4038 1.46446 22.6967 1.75736L26.9393 6L22.6967 10.2426C22.4038 10.5355 22.4038 11.0104 22.6967 11.3033C22.9896 11.5962 23.4645 11.5962 23.7574 11.3033L28.5303 6.53033ZM6.55671e-08 6.75L28 6.75L28 5.25L-6.55671e-08 5.25L6.55671e-08 6.75Z" fill="white" />
                              </svg>
                            </span>
                        </a>
                        <?php endif ?>
                        <?php if ( ! empty( $secondary_button ) ):
                            $secondary_button_url = $secondary_button['url'];
                            $secondary_button_title = $secondary_button['title'];
                            $secondary_button_target = $secondary_button['target'] ? $link['target'] : '_self';
                        ?>
                        <a href="<?php echo esc_url( $secondary_button_url ); ?>" target="<?php echo $secondary_button_target ?>" class="cle-btn cle-btn_secondary">
                            <?php echo esc_html( $secondary_button_title ); ?>
                            <span class="icon"><svg width="29" height="12" viewBox="0 0 29 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M28.5303 6.53033C28.8232 6.23743 28.8232 5.76256 28.5303 5.46967L23.7574 0.696697C23.4645 0.403804 22.9896 0.403804 22.6967 0.696697C22.4038 0.989591 22.4038 1.46446 22.6967 1.75736L26.9393 6L22.6967 10.2426C22.4038 10.5355 22.4038 11.0104 22.6967 11.3033C22.9896 11.5962 23.4645 11.5962 23.7574 11.3033L28.5303 6.53033ZM6.55671e-08 6.75L28 6.75L28 5.25L-6.55671e-08 5.25L6.55671e-08 6.75Z" fill="#004EE4" />
                              </svg>
                            </span>
                        </a>
                        <?php endif ?>
                    </div>
                    <?php endif ?>
                </div>
                <div class="cle-content-blocks-section__navigation">
                    <svg class="cle-content-blocks-section__prev" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" fill="#EDF5FF" stroke="#1464FF" stroke-width="1.5"/>
                        <path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF"/>
                    </svg>
                    <svg class="cle-content-blocks-section__next" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" fill="#EDF5FF" stroke="#1464FF" stroke-width="1.5"/>
                        <path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF"/>
                    </svg>
                </div>
            </div>

            <div class="cle-content-blocks-section__list">
                <div class="swiper">
                    <?php if (!empty($blocks)) : ?>
                        <div class="swiper-wrapper">
                            <?php foreach ($blocks as $row):
                                $icon = $row['icon'];
                                $title = $row['title'];
                                $description = $row['description'];
                                $link = $row['link'];
                            ?>
                            <div class="swiper-slide">
                                <div class="cle-content-blocks-section__item">
                                    <?php if (!empty($icon)) : ?>
                                        <img src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>" class="cle-content-blocks-section__icon">
                                    <?php endif ?>
                                    <div class="cle-content-blocks-section__textual">
                                        <?php if (!empty($title)) : ?>
                                            <h4 class="cle-content-blocks-section__name"><?php echo $title; ?></h4>
                                        <?php endif ?>
                                        <div class="cle-content-blocks-section__text">
                                            <?php if (!empty($description)) : ?>
                                                <div class="cle-content-blocks-section__description"><?php echo $description; ?></div>
                                            <?php endif ?>
                                            <?php if (!empty($link)) : ?>
                                            <a href="<?php echo esc_url($link['url']); ?>" class="cle-content-blocks-section__link">
                                                <?php echo $link['title']; ?>
                                                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <circle cx="15" cy="15" r="14.25" fill="white" stroke="#0A1E46" stroke-width="1.5"/>
                                                    <path d="M20.5303 15.5303C20.8232 15.2374 20.8232 14.7626 20.5303 14.4697L15.7574 9.6967C15.4645 9.40381 14.9896 9.40381 14.6967 9.6967C14.4038 9.98959 14.4038 10.4645 14.6967 10.7574L18.9393 15L14.6967 19.2426C14.4038 19.5355 14.4038 20.0104 14.6967 20.3033C14.9896 20.5962 15.4645 20.5962 15.7574 20.3033L20.5303 15.5303ZM9 15.75L20 15.75L20 14.25L9 14.25L9 15.75Z" fill="#0A1E46"/>
                                                </svg>
                                            </a>
                                            <?php endif ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif ?>
                    <div class="cle-content-blocks-section__bottom">
                        <div class="cle-content-blocks-section__navigation">
                            <svg class="cle-content-blocks-section__prev" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" fill="#EDF5FF" stroke="#1464FF" stroke-width="1.5"/>
                                <path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF"/>
                            </svg>
                            <svg class="cle-content-blocks-section__next" width="52" height="52" viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <circle cx="26" cy="26" r="25.25" transform="matrix(-1 0 0 1 52 0)" fill="#EDF5FF" stroke="#1464FF" stroke-width="1.5"/>
                                <path d="M16.8014 26.5303C16.5085 26.2374 16.5085 25.7626 16.8014 25.4697L21.5744 20.6967C21.8673 20.4038 22.3422 20.4038 22.6351 20.6967C22.928 20.9896 22.928 21.4645 22.6351 21.7574L18.3924 26L22.6351 30.2426C22.928 30.5355 22.928 31.0104 22.6351 31.3033C22.3422 31.5962 21.8673 31.5962 21.5744 31.3033L16.8014 26.5303ZM36.3984 26.75L17.3318 26.75L17.3318 25.25L36.3984 25.25L36.3984 26.75Z" fill="#1464FF"/>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
