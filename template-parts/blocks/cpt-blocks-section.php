<?php

/*
 * Block Name: CPT blocks section
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$style = ! empty( $args['style'] ) ? $args['style'] : get_field('block_style');

$choose_post_types = ! empty( $args['choose_post_types'] ) ? $args['choose_post_types'] : get_field('choose_post_types');

$title = get_field('title');
$title = ! empty( $args['title'] ) ? $args['title'] : $title;

$button = get_field('button');

$posts = get_field('posts');
$posts = ! empty( $args['posts'] ) ? $args['posts'] : $posts;

$decor_position = ! empty( $args['decor_position'] ) ? $args['decor_position'] :  get_field('decor_position');


$blue_background = get_field('blue_background');
$blue_background = ! empty( $args['blue_background'] ) ? $args['blue_background'] : $blue_background;

if ( $style != 'blog-events' ) {
    $args = array(
        'post_type' => 'any',
        'post__in' => $posts,
        'orderby' => 'post__in',
    );

    $the_query = new WP_Query( $args );
}

$block_name = 'cle-cpt-blocks-section';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'cle-section-element-pad';
$className[] = 'decor-' . $decor_position;
$className[] = 'cle-section';
$background = $blue_background ? 'cle-back-blue' : '';
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr(trim(implode(' ', $className))) ?> <?php echo $background; ?>">
	<?php if ( $style == 'bottom-button' ): ?>
        <div class="decor"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/decor-1.svg" alt=""></div>
		<div class="container">
			<div class="cle-cpt-blocks-section__wrap">
				<?php if ( ! empty( $title ) ): ?>
					<div class="cle-cpt-blocks-section__top mb-80">
						<h2><?php echo $title; ?></h2>
					</div>
				<?php endif ?>

				<?php if ( $the_query->have_posts() ): ?>
					<div class="cle-cpt-blocks-section__cards">
						<div class="row">
							<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
								<?php get_template_part('template-parts/' . get_post_type() . '/item'); ?>
							<?php endwhile; wp_reset_postdata(); ?>
						</div>
					</div>
				<?php endif ?>

				<?php if ( ! empty( $button ) ):
					$link_target = ! empty( $button['target'] ) ? $button['target'] : '_self'; ?>
					<div class="cle-cpt-blocks-section__btn-cont mt-75">
						<a href="<?php echo $button['url']; ?>" class="cle-btn cle-btn_primary" target="<?php echo esc_attr( $link_target ); ?>">
							<?php echo $button['title']; ?>
							<span class="icon">
								<svg width="29" height="12" viewBox="0 0 29 12" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M28.5303 6.53033C28.8232 6.23743 28.8232 5.76256 28.5303 5.46967L23.7574 0.696697C23.4645 0.403804 22.9896 0.403804 22.6967 0.696697C22.4038 0.989591 22.4038 1.46446 22.6967 1.75736L26.9393 6L22.6967 10.2426C22.4038 10.5355 22.4038 11.0104 22.6967 11.3033C22.9896 11.5962 23.4645 11.5962 23.7574 11.3033L28.5303 6.53033ZM6.55671e-08 6.75L28 6.75L28 5.25L-6.55671e-08 5.25L6.55671e-08 6.75Z" fill="white" />
								</svg>
							</span>
						</a>
					</div>
				<?php endif ?>
			</div>
		</div>
    <?php elseif ( $style == 'top-button' ): ?>
        <div class="decor"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/decor-1.svg" alt=""></div>
        <div class="container">
            <div class="cle-cpt-blocks-section__wrap">
                <?php if ( ! empty( $title ) ): ?>
                    <div class="cle-cpt-blocks-section__top mb-80">
                        <h2><?php echo $title; ?></h2>
                        <?php if ( ! empty( $button ) ):
                            $link_target = ! empty( $button['target'] ) ? $button['target'] : '_self'; ?>
                            <div class="cle-cpt-blocks-section__btn-cont">
                                <a href="<?php echo $button['url']; ?>" class="cle-btn cle-btn_primary" target="<?php echo esc_attr( $link_target ); ?>">
                                    <?php echo $button['title']; ?>
                                    <span class="icon">
                                    <svg width="29" height="12" viewBox="0 0 29 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M28.5303 6.53033C28.8232 6.23743 28.8232 5.76256 28.5303 5.46967L23.7574 0.696697C23.4645 0.403804 22.9896 0.403804 22.6967 0.696697C22.4038 0.989591 22.4038 1.46446 22.6967 1.75736L26.9393 6L22.6967 10.2426C22.4038 10.5355 22.4038 11.0104 22.6967 11.3033C22.9896 11.5962 23.4645 11.5962 23.7574 11.3033L28.5303 6.53033ZM6.55671e-08 6.75L28 6.75L28 5.25L-6.55671e-08 5.25L6.55671e-08 6.75Z" fill="white" />
                                    </svg>
                                </span>
                                </a>
                            </div>
                        <?php endif ?>
                    </div>
                <?php endif ?>
                <?php if ( $the_query->have_posts() ): ?>
                    <div class="cle-cpt-blocks-section__cards">
                        <div class="row">
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <?php get_template_part('template-parts/' . get_post_type() . '/item'); ?>
                            <?php endwhile; wp_reset_postdata(); ?>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>
	<?php elseif ( $style == 'blog-events' && ! empty( $choose_post_types ) ): ?>
        <?php get_template_part('template-parts/elements/posts-with-filter',
            null,
            array(
                'choose_post_types' => $choose_post_types
            )
        ); ?>
	<?php endif ?>
</div>