<?php
$image = $args['img'];
?>
<div class="cle-image-and-content__image-cont">
    <img class="cle-image-and-content__image-main border1" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>">
</div>
