<?php
$third_button_flip = $args['field'];
if( $third_button_flip ):
    $third_button_flip_url = $third_button_flip['url'];
    $third_button_flip_title = $third_button_flip['title'];
    $third_button_flip_target = $third_button_flip['target'] ? $third_button_flip['target'] : '_self';
endif;
?>
<a class="cle-btn cle-btn_third-flip" href="<?php echo esc_url( $third_button_flip_url ); ?>" target="<?php echo esc_attr( $third_button_flip_target ); ?>">
    <span><?php echo esc_html( $third_button_flip_title ); ?></span>
    <?php get_template_part('template-parts/elements/circle-in-button'); ?>
</a>
