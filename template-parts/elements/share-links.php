<?php
/**
 * Share links
 */
?>

<div class="cle-socials-single-post">
    <ul class="cle-socials-list">
        <li>
            <a href="#" data-share="https://twitter.com/intent/tweet?text=<?php echo urlencode(get_the_title()); ?>&url=<?php the_permalink(); ?>">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/twitter.svg'; ?>" alt="twitter">
            </a>
        </li>
        <li>
            <a href="#" data-share="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()) ?>">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/facebook.svg'; ?>" alt="facebook">
            </a>
        </li>
        <li>
            <a href="#" data-share="https://www.linkedin.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()) ?>">
                <img src="<?php echo get_template_directory_uri() . '/assets/img/linkedin.svg'; ?>" alt="linkedin">
            </a>
        </li>
    </ul>
</div>
