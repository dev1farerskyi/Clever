<?php
$blue_button = $args['field'];
if( $blue_button ):
    $blue_button_url = $blue_button['url'];
    $blue_button_title = $blue_button['title'];
    $blue_button_target = $blue_button['target'] ? $blue_button['target'] : '_self';
endif;
?>
<a class="cle-btn cle-btn_blue" href="<?php echo esc_url( $blue_button_url ); ?>" target="<?php echo esc_attr( $blue_button_target ); ?>">
    <?php echo esc_html( $blue_button_title ); ?>
</a>
