<?php
$secondary_button = $args['field'];
if( $secondary_button ):
    $secondary_button_url = $secondary_button['url'];
    $secondary_button_title = $secondary_button['title'];
    $secondary_button_target = $secondary_button['target'] ? $secondary_button['target'] : '_self';
endif;
?>
<a class="cle-btn cle-btn_secondary" href="<?php echo esc_url( $secondary_button_url ); ?>" target="<?php echo esc_attr( $secondary_button_target ); ?>">
    <?php echo esc_html( $secondary_button_title ); ?>
    <?php get_template_part('template-parts/elements/icon-rights-blue'); ?>
</a>
