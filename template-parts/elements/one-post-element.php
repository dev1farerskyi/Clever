<?php
$post_id = get_the_ID();
?>
<div class="row">
    <div class="col-lg-8 col-md-6 col-sm-12">
        <?php if ( has_post_thumbnail() ): ?>
        <div class="cle-one-post__img">
            <?php the_post_thumbnail(); ?>
            <div class="cle-one-post__category"><?php the_category(); ?></div>
        </div>
        <?php endif ?>
    </div>
    <div class="col-lg-4 col-md-6 col-sm-12">
        <div class="cle-one-post__content">
            <div>
                <p class="cle-one-post__data"><?php the_date(); ?></p>
                <h4 class="cle-one-post__title"><?php the_title(); ?></h4>
                <p class="cle-one-post__description"><?php the_excerpt(); ?></p>
                <a class="cle-btn cle-btn_primary" href="<?php the_permalink(); ?>">
                    <?php esc_html_e('Read More', 'clever', V_PREFIX); ?>
                    <?php get_template_part('template-parts/elements/icon-right-white'); ?>
                </a>
            </div>
        </div>
    </div>
</div>
