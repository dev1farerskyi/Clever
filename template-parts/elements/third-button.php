<?php
$third_button = $args['field'];
if( $third_button ):
    $third_button_url = $third_button['url'];
    $third_button_title = $third_button['title'];
    $third_button_target = $third_button['target'] ? $third_button['target'] : '_self';
endif;
?>
<a class="cle-btn cle-btn_third" href="<?php echo esc_url( $third_button_url ); ?>" target="<?php echo esc_attr( $third_button_target ); ?>">
    <?php echo esc_html( $third_button_title ); ?>
    <?php get_template_part('template-parts/elements/circle-in-button'); ?>
</a>