<?php
$year = date('Y');
// $post_type = !empty($args['post_type']) ? $args['post_type'] : 'post';
// $post_types = ! empty( $args['choose_post_types'] ) ? array_key_first( $args['choose_post_types'] ) : 'post';
$posts_count = ! empty( $args['posts_count'] ) ? $args['posts_count'] : 3;
$counter = 0;

?>
<div class="decor"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/decor-1.svg" alt=""></div>
<div class="container js-cpt-blocks">
    <div class="cle-cpt-blocks-section__wrap">
        <div class="cle-cpt-blocks-section__top mb-75">
            <?php if (! empty($args['choose_post_types'])): ?>
                <h2><?php echo cle_post_types_list($args['choose_post_types'][0]); ?></h2>
                <div class="cle-cpt-blocks-section__top-categories">
                    <?php foreach ($args['choose_post_types'] as $value):
                        $el_class = ! $counter ? 'active' : ''; ?>

                        <button class="<?php echo $el_class; ?>" data-post_type="<?php echo $value; ?>" href="#"><?php echo cle_post_types_list($value); ?></button>
                        <?php $counter++; ?>
                    <?php endforeach ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="cle-filters mb-75">
            <div class="cle-filters-search">
                <input class="cle-filters-search-input" type="search" id="site-search" name="search" placeholder="Search by keyword">
                <button class="cle-filters-search-btn js-cpt-blocks-submit" type="search">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/search.svg" alt="">
                </button>
            </div>
            <div class="cle-filters-select-wrap cle-filed-form">
                <select name="select" class="filter-month">
                    <option value="" selected="selected">Month</option>
                    <option value="1">January</option>
                    <option value="2">February</option>
                    <option value="3">March</option>
                    <option value="4">April</option>
                    <option value="5">May</option>
                    <option value="6">June</option>
                    <option value="7">July</option>
                    <option value="8">August</option>
                    <option value="9">September</option>
                    <option value="10">October</option>
                    <option value="11">November</option>
                    <option value="12">December</option>
                </select>
                <select name="select2" class="filter-year">
                    <option value="" selected="selected">Year</option>
                    <option value="<?php echo $year - 1; ?>"><?php echo $year - 1; ?></option>
                    <option value="<?php echo $year; ?>"><?php echo $year; ?></option>
                    <option value="<?php echo $year + 1; ?>"><?php echo $year + 1; ?></option>
                </select>
            </div>
        </div>

        <div class="js-cpt-blocks__wrap">
            <?php
            $args = array(
                'post_type' => $args['choose_post_types'][0]
            );
            echo cle_get_blog_events_html( $args, $posts_count ); ?>
        </div>
    </div>
</div>