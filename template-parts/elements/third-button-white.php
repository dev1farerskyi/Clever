<?php
$third_button = $args['field'];
if( $third_button ):
    $third_button_url = $third_button['url'];
    $third_button_title = $third_button['title'];
endif;
?>
<a class="cle-btn cle-btn_third-white" href="<?php echo esc_url( $third_button_url ); ?>">
    <?php echo esc_html( $third_button_title ); ?>
    <?php get_template_part('template-parts/elements/circle-white'); ?>
</a>
