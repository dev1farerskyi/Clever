<?php
$primary_button = $args['field'];
if( $primary_button ):
    $primary_button_url = $primary_button['url'];
    $primary_button_title = $primary_button['title'];
    $primary_button_target = $primary_button['target'] ? $primary_button['target'] : '_self';
endif;
?>
<a class="cle-btn cle-btn_primary" href="<?php echo esc_url( $primary_button_url ); ?>" target="<?php echo esc_attr( $primary_button_target ); ?>">
    <?php echo esc_html( $primary_button_title ); ?>
    <?php get_template_part('template-parts/elements/icon-right-white'); ?>
</a>