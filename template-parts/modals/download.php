<?php
/**
 * Download Modal
 */
?>

<div class="vil-modal" data-modal="download">
    <div class="vil-modal__dialog">
        <div class="vil-modal__wrap">
            <div class="d-flex vil-modal__content">
                <div class="vil-modal__form">
                    <h5 class="vil-modal__form-title d-none d-md-block"><?php echo $args['title']; ?></h5>

                    <?php echo do_shortcode('[gravityform id="' . $args['guide_id'] . '" ajax="true" title="false" description="false"]'); ?>
                </div>

                <div class="vil-modal__media">
                    <div class="d-block d-md-none">
                        <h5 class="vil-modal__media-title"><?php echo $args['title']; ?></h5>
                    </div>

                    <div class="vil-modal__media-content d-none d-md-block">
                        <?php if ( ! empty( $args['image'] ) ) : ?>
                            <div class="vil-modal__media-img">
                                <img src="<?php echo $args['image']['url']; ?>" alt="image">
                            </div>
                        <?php endif; ?>

                        <?php if ( ! empty( $args['subtitle'] ) ) : ?>
                            <h6><?php echo $args['subtitle']; ?></h6>
                        <?php endif; ?>

                        <?php if ( ! empty( $args['text'] ) ) : ?>
                            <p><?php echo wp_kses_post( $args['text'] ); ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <button type="button" class="vil-btn vil-modal__close">
                <?php esc_html_e('CLOSE', V_PREFIX); ?>
                <span class="icon">
                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M8.27455 6L11.686 2.58852C12.1047 2.16989 12.1047 1.49114 11.686 1.07216L10.9278 0.313977C10.5092 -0.104659 9.83045 -0.104659 9.41148 0.313977L6 3.72545L2.58852 0.313977C2.16989 -0.104659 1.49114 -0.104659 1.07216 0.313977L0.313977 1.07216C-0.104659 1.4908 -0.104659 2.16955 0.313977 2.58852L3.72545 6L0.313977 9.41148C-0.104659 9.83011 -0.104659 10.5089 0.313977 10.9278L1.07216 11.686C1.4908 12.1047 2.16989 12.1047 2.58852 11.686L6 8.27455L9.41148 11.686C9.83011 12.1047 10.5092 12.1047 10.9278 11.686L11.686 10.9278C12.1047 10.5092 12.1047 9.83045 11.686 9.41148L8.27455 6Z" fill="#555555" />
                    </svg>
                </span>
            </button>
        </div>
    </div>
</div>
