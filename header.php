<?php
/**
 * Header template.
 *
 * @package villanova
 * @since 1.0.0
 *
 */

$login_button = get_field('login_button', 'option');
$mobile_button_text = get_field('mobile_button_text', 'option');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no"/>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<header class="cle-header">
    <div class="cle-header__container">
        <div class="cle-header__col">
            <button class="cle-header__open-menu js-open-menu"><span></span></button>

            <a href="<?php echo esc_url(home_url('/')); ?>"
               class="cle-header__logo">
                <img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/logo.png'; ?>" alt="logo">
            </a>
        </div>

        <div class="cle-header__col">
            <?php wp_nav_menu(array(
                'theme_location' => 'header-menu',
                'container' => false,
                'menu_class' => 'cle-header__menu',
                'echo' => true,
                'fallback_cb' => '__return_empty_string', // wp_page_menu
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth' => 0,
                'walker' => new CLE_Walker_Nav_Menu()
            )) ?>

            <?php if ( ! empty( $login_button ) ):
                $link_target = ! empty( $button['target'] ) ? $button['target'] : '_self'; ?>
                <a class="cle-header__login" href="<?php echo $login_button['url']; ?>" target="<?php echo $link_target; ?>">
                    <svg width="21" height="20" viewBox="0 0 21 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="10.5" cy="10" r="9.5" stroke="#FFE478"/>
                        <circle cx="10.5" cy="7" r="2.5" stroke="#FFE478"/>
                        <path d="M3 15.8018C4.08346 12.9928 6.80901 11 10 11C13.4495 11 16.3551 13.3288 17.2304 16.5" stroke="#FFE478"/>
                    </svg>
                    <span class="cle-header__login-desktop"><?php echo $login_button['title']; ?></span>
                    <span class="cle-header__login-mobile"><?php echo $mobile_button_text; ?></span>
                </a>
            <?php endif ?>
        </div>
    </div>
</header>

<div class="cle-header-mobile">
    <div class="cle-header-mobile__container">
        <?php wp_nav_menu(array(
            'theme_location' => 'header-menu',
            'container' => false,
            'menu_class' => 'cle-header-mobile__menu',
            'echo' => true,
            'fallback_cb' => '__return_empty_string', // wp_page_menu
            'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth' => 0,
            'walker' => new CLE_Mobile_Walker_Nav_Menu()
        )) ?>
    </div>
</div>

<main class="main-wrapper">
