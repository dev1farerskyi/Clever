<?php
/**
 * Single News template
 */

get_header();

$header_description = get_the_date();
$numbers_title = get_field('numbers_title');
$numbers_description = get_field('numbers_description');
$numbers_items = get_field('numbers_items');
$numbers_style = get_field('numbers_style');
$icons_row_title = get_field('icons_row_title');
$icons_row_items = get_field('icons_row_items');
$icons_row_style = get_field('icons_row_style');
$testimonial_main_title = get_field('testimonial_main_title');
$testimonial_big_image = get_field('testimonial_big_image');
$testimonial_decoration_style = get_field('testimonial_decoration_style');
$testimonial_display_type = get_field('testimonial_display_type');
$testimonial_posts = get_field('testimonial_posts');
$cpt_decor_position = get_field('cpt_decor_position');
$cpt_title = get_field('cpt_title');
$cpt_blue_background = get_field('cpt_blue_background');
$cpt_posts = get_field('cpt_posts');
$cpt_style = get_field('cpt_style');
?>

<?php get_template_part(
    'template-parts/blocks/simple-text-header',
    null,
    array(
        'style' => 'all-width',
        'subtitle' => 'Case Study',
        'title' => get_the_title(),
        'description' => $header_description,
        'show_share_links' => true,
    )
); ?>

<?php get_template_part(
    'template-parts/blocks/numbers-section',
    null,
    array(
        'title' => $numbers_title,
        'description' => $numbers_description,
        'items' => $numbers_items,
        'style' => $numbers_style,
    )
); ?>

<?php get_template_part(
    'template-parts/blocks/icons-row-section',
    null,
    array(
        'title' => $icons_row_title,
        'items' => $icons_row_items,
        'style' => $icons_row_style,
    )
); ?>

    <div class="cle-content-section">
        <div class="d-text">
            <?php the_content(); ?>
        </div>
    </div>

<?php get_template_part(
    'template-parts/blocks/testimonial-slider',
    null,
    array(
        'main_title' => $testimonial_main_title,
        'big_image' => $testimonial_big_image,
        'decoration_style' => $testimonial_decoration_style,
        'display_type' => $testimonial_display_type,
        'posts' => $testimonial_posts,
    )
); ?>

<?php get_template_part(
    'template-parts/blocks/cpt-blocks-section',
    null,
    array(
        'decor_position' => $cpt_decor_position,
        'title' => $cpt_title,
        'blue_background' => $cpt_blue_background,
        'posts' => $cpt_posts,
        'style' => $cpt_style,
    )
); ?>

<?php get_template_part('template-parts/blocks/subscribe'); ?>


<?php
get_footer();
