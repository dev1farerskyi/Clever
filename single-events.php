<?php
/**
 * Single News template
 */

get_header();

$header_description = get_the_date();
?>

<?php get_template_part(
    'template-parts/blocks/simple-text-header',
    null,
    array(
        'style' => 'all-width',
        'subtitle' => 'News',
        'title' => get_the_title(),
        'description' => $header_description,
        'show_share_links' => true,
    )
); ?>

<?php the_content(); ?>

<?php get_template_part('template-parts/blocks/subscribe'); ?>


<?php
get_footer();
