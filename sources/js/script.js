import './app/gutenberg';
import Swiper from 'swiper/swiper-bundle';
import {gsap} from "./app/gsap/gsap";
import {ScrollTrigger} from "./app/gsap/ScrollTrigger";
import {CustomEase} from "./app/gsap/CustomEase";
import {isEven, isjQuery, Coordinates, videoResize} from "./app/functions";

gsap.registerPlugin(ScrollTrigger, CustomEase);

CustomEase.create('bazier1', '.25, .46, .45, .94');

(function ($) {

    $(document).ready(function() {

        //mfp video handler
        $('[data-mfp-video]').magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });

        //custom selects handler
        $('.cle-filed-form select').each(function() {
            $(this).select2({
                minimumResultsForSearch: -1,
                dropdownParent: $(this).parent()
            });

            if ($(this).attr('multiple') !== undefined && $(this).attr('multiple') !== false) {
                $(this).on('select2:opening select2:closing', function( event ) {
                    var $searchfield = $(this).parent().find('.select2-search__field');
                    $searchfield.prop('disabled', true);
                });
            }
        });
    });

    let header = $('.cle-header')

    header.find('.menu-item_popular-resources').each(function () {
        let $self = $(this),
            posts = $self.data('posts')

        let html = '';
        posts.forEach(el => {
            html += `
<a href="${el.url}" class="cle-post-item">
    <div class="cle-post-item__img"><img src="${el.img}" alt="${el.title}"></div>
    <div class="cle-post-item__container">
        <div class="cle-post-item__title">${el.title}</div>
        <div class="cle-post-item__link"><span>Read More</span><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="12" cy="12" r="11.4" fill="white" stroke="#1464FF" stroke-width="1.2"/><path d="M16.4274 12.4243C16.6617 12.19 16.6617 11.8101 16.4274 11.5757L12.609 7.75736C12.3747 7.52305 11.9948 7.52305 11.7605 7.75736C11.5262 7.99167 11.5262 8.37157 11.7605 8.60589L15.1546 12L11.7605 15.3941C11.5262 15.6284 11.5262 16.0083 11.7605 16.2426C11.9948 16.477 12.3747 16.477 12.609 16.2426L16.4274 12.4243ZM7.20312 12.6L16.0031 12.6L16.0031 11.4L7.20313 11.4L7.20312 12.6Z" fill="#1464FF"/></svg></div>
    </div>
</a>`;
        })

        $self.find('.menu-item-two-level').after(html)
    })

    // Mobile
    let headerMobile = $('.cle-header-mobile')
    headerMobile.find('.menu-item-has-children').each(function () {
        let $self = $(this),
            $openSubMenu = $self.find('> .menu-item-wrap .open-sub-menu'),
            $subMenu = $self.find('> .sub-menu')

        $openSubMenu.on('click', function () {
            $subMenu.addClass('active')
        })
        $subMenu.find('> .close-sub-menu').on('click', function () {
            $subMenu.removeClass('active')
        })
    })

    headerMobile.find('.menu-item_popular-resources').each(function () {
        let $self = $(this),
            posts = $self.data('posts')

        let html = '';
        posts.forEach(el => {
            html += `
<a href="${el.url}" class="cle-post-item">
    <div class="cle-post-item__title">${el.title}</div>
    <div class="cle-post-item__link"><span>Read More</span><svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="12" cy="12" r="11.4" fill="white" stroke="#1464FF" stroke-width="1.2"/><path d="M16.4274 12.4243C16.6617 12.19 16.6617 11.8101 16.4274 11.5757L12.609 7.75736C12.3747 7.52305 11.9948 7.52305 11.7605 7.75736C11.5262 7.99167 11.5262 8.37157 11.7605 8.60589L15.1546 12L11.7605 15.3941C11.5262 15.6284 11.5262 16.0083 11.7605 16.2426C11.9948 16.477 12.3747 16.477 12.609 16.2426L16.4274 12.4243ZM7.20312 12.6L16.0031 12.6L16.0031 11.4L7.20313 11.4L7.20312 12.6Z" fill="#1464FF"/></svg></div>
</a>`;
        })

        $self.find('.menu-item-two-level').after(html)
    })


    header.find('.js-open-menu').on('click', function () {
        $(this).toggleClass('active')
        headerMobile.toggleClass('active')
        $('body').toggleClass('overflow-hidden')

        headerMobile.find('.sub-menu').removeClass('active')
    })



    $(".cle-accordion__head-wrap").on("click", function () {
        var $this = $(this).parent();
        $(this).closest('.cle-accordion').toggleClass("active");
        $this.find('.cle-accordion__body').slideToggle();
        ScrollTrigger.refresh(true)
    });

    $(".cle-accordion-section__head").on("click", function () {
        var $this = $(this).parent();
        $(this).closest('.cle-accordion').toggleClass("active");
        $this.find('.cle-accordion-section__body').slideToggle();
        ScrollTrigger.refresh(true)
    });

    // CPT blocks section
    var cpt_data = {
            page: 1,
            action: 'cle_get_blog_events',
            post_type: 'post',
            s: '',
            month: '',
            year: '',
        },
        $cpt_wrap = $('.js-cpt-blocks'),
        $results_wrap = $cpt_wrap.find('.js-cpt-blocks__wrap');

    cpt_data.post_type = $('.cle-cpt-blocks-section__top-categories button.active').attr('data-post_type');

    $cpt_wrap.on('click', '.cle-pagination__list a', function(e) {
        var btn = $(this);

        if ( btn.hasClass('next') ) {
            cpt_data.page++;
        }

        if ( btn.hasClass('prev') ) {
            cpt_data.page--;
        }

        if ( ! btn.hasClass('prev') && ! btn.hasClass('next') ) {
            cpt_data.page = btn.text();
        }

        cpt_load_more();

        e.preventDefault();
    }).on('click', '.cle-cpt-blocks-section__top-categories button:not(.active)', function(e){
        var btn = $(this);

        $('.cle-cpt-blocks-section__top-categories button').removeClass('active');
        btn.addClass('active');

        cpt_data.page = 1;
        cpt_data.post_type = btn.attr('data-post_type');

        $cpt_wrap.find('input, select').val('');

        cpt_data.year = '';
        cpt_data.month = '';
        cpt_data.s = '';

        cpt_load_more();

        e.preventDefault();
    }).on('change', '.filter-month', function(){
        cpt_data.page = 1;
        cpt_data.month = $(this).val();
        cpt_load_more();
    }).on('change', '.filter-year', function(){
        cpt_data.page = 1;
        cpt_data.year = $(this).val();

        cpt_load_more();
    }).on('click', '.js-cpt-blocks-submit', function(){
        cpt_data.page = 1;
        cpt_data.s = $(this).closest('.cle-cpt-blocks-section__filters-search').find('input').val();

        cpt_load_more();
    });


    function cpt_load_more() {
        $.ajax({
            url: cle_object.ajax_url,
            type: 'POST',
            data: cpt_data,
            beforeSend: function( xhr ) {
                $results_wrap.html('<div class="lds-dual-ring"></div>');

                $('body,html').animate({
                    scrollTop: $cpt_wrap.offset().top
                }, 300);
            },
            success: function( resp ) {
                $results_wrap.html(resp);
            }
        });
    }


    // Tabs with Content Section
    $('.js-tab-buttons').on('click', 'button:not(.active)', function() {
        var btn = $(this);

        btn.closest('.js-tab-buttons').find('button').removeClass('active');
        btn.addClass('active');

        btn.closest('.cle-tabs-with-content-section').find('.cle-tabs-with-content-section__tab').removeClass('active');
        btn.closest('.cle-tabs-with-content-section').find('.cle-tabs-with-content-section__tab[data-tab="' + btn.attr('data-tab') + '"]').addClass('active');
    });

    let BCNoSectionElement = () => {
        let $wrapGlob = $('.main-wrapper > *')
        $wrapGlob.each(function () {
            let $self = $(this)

            if (!$self.hasClass('cle-section') && this.tagName !== 'STYLE' && !$self.hasClass('pin-spacer')) {
                $self.addClass('cle-section-default-element')
            }
        });
    }

    if (window.acf) {
        window.acf.addAction('render_block_preview', BCNoSectionElement)
    } else {
        BCNoSectionElement()
    }


    // jobs filter 
    var jobs_filter = {
        dep: 'all',
        s: ''
    };

    function filter_jobs() {
        if ( jobs_filter.dep != 'all' ) {
            $('.cle-open-positions__card').hide();
            $('.cle-open-positions__card.dep-' + jobs_filter.dep).show();
        }

        if ( jobs_filter.s != '' ) {
            $('.cle-open-positions__card').hide();
            
            $('.cle-open-positions__card').each(function(){
                var el = $(this),
                    title = el.find('.cle-open-positions__card-title').text().toLowerCase(),
                    s = jobs_filter.s.toLowerCase();

                if ( title.includes(s) ) {
                    el.show();
                }
            });
        }

        if ( jobs_filter.dep == 'all' && jobs_filter.s == '') {
            $('.cle-open-positions__card').show();
        }
    }

    $('.js-jobs-filter').on('change', 'select', function() {
        var select = $(this);
        select.closest('.js-jobs-filter').find('input').val('');

        jobs_filter.dep = select.val();
        jobs_filter.s = '';

        filter_jobs();
    }).on('keyup', 'input', function(){
        var input = $(this);
        input.closest('.js-jobs-filter').find('select').val('all');
        
        jobs_filter.dep = 'all';
        jobs_filter.s = input.val();

        filter_jobs();
    });


    // Apps list
    $(window).on('load', function(){
        if ( $('.js-apps-list').length ) {
            var wrap = $('.js-apps-list');
            for (var i = 0; i < apps_list.length; i++) {
                var cats = '';
                if ( apps_list[i]['App Categories'].length ) {
                    for (var n = 0; n < apps_list[i]['App Categories'].length; n++) {
                        cats = cats + ' app-cat-' + apps_list[i]['App Categories'][n];
                    }
                }

                wrap.append('<div class="col-lg-4 col-md-6 col-sm-12 ' + cats + ' js-app-list-item">' +
                                '<div class="cle-application-gallery__item">' +
                                    '<div class="cle-application-gallery__item-icon">' +
                                        '<img src="' + apps_list[i]['App Icon URL'] + '" alt="' + apps_list[i]['Title'] + '">' +
                                    '</div>' +
                                    '<h4 class="cle-application-gallery__item-title">' +
                                        '<a href="' + cle_object.site_url + 'app-gallery/' + apps_list[i]['App Shortname'] + '/">' +
                                            apps_list[i]['Title'] +
                                        '</a>' +
                                    '</h4>' +
                                '</div>' +
                            '</div>');
            }
        }

        $('.js-apps-filter').on('change', 'select', function(){
            var select = $(this),
                select_filter = select.val(),
                parent = select.closest('.cle-application-gallery__wrap');

            if ( select_filter == 'all' ) {
                parent.find('.js-app-list-item').show();
            } else {
                parent.find('.js-app-list-item').hide();
                parent.find('.js-app-list-item.app-cat-' + select_filter).show();
            }

            $('.js-apps-filter').find('input').val('');
        }).on('keyup', 'input', function(){
            var input = $(this),
                s = input.val().toLowerCase(),
                parent = input.closest('.cle-application-gallery__wrap');


            if ( s != '' ) {
                parent.find('.js-app-list-item').hide();

                parent.find('.js-app-list-item').each(function(){
                    var el = $(this),
                        title = el.find('.cle-application-gallery__item-title').text().toLowerCase();

                    if ( title.includes(s) ) {
                        el.show();
                    }
                });

            } else {
                parent.find('.js-app-list-item').show();
            }

            $('.js-apps-filter').find('select').val('all');
        });
    });

})(jQuery);

// highlighted line animation

titleHighlightAnimation('.cle-about-the-webinar-section__title');

titleHighlightAnimation('.cle-simple-text-header__title');

titleHighlightAnimation('.cle-brand-cta-section__title');

titleHighlightAnimation('.cle-pricing-tabs-section__header h1');

function titleHighlightAnimation(query) {
    const titles = document.querySelectorAll(query)

    if (titles.length) {
        titles.forEach(title => {
            const highlights = title.querySelectorAll('span')

            title.classList.add('js-title-with-highlight')

            if (highlights.length) {
                highlights.forEach(highlight => {
                    gsap.fromTo(highlight, { backgroundSize: '0% 4px' }, { backgroundSize: '100% 4px', ease: 'bazier1', duration: 1, scrollTrigger: {
                            trigger: title
                        } })
                });
            }
        })
    }
}


// Text with counter block animatioin

if (!window.acf) {
    const textWithCounter = document.querySelectorAll('.cle-text-with-counter')

    if (textWithCounter.length) {
        textWithCounter.forEach(item => {
            const percentage = item.querySelector('.cle-text-with-counter__percentage')
            const rotetable = item.querySelector('.cle-text-with-counter__circle.cle-text-with-counter__circle_rotetable')

            if (!!percentage && rotetable) {
                const num = percentage.querySelector('span')
                let target = { val: 0 }
                const tl = gsap.timeline({
                    scrollTrigger: {
                        trigger: item,
                    }
                })
                const duration = .8;

                gsap.set(target, { opacity: 0 })

                tl
                    .to(rotetable, { rotate: parseInt( (180 * num.innerHTML) / 100 ), ease: 'linear', duration: duration })
                    .to(target, {
                        val: num.innerHTML,
                        opacity: 1,
                        duration: duration,
                        onUpdate: () => {
                            num.innerHTML = target.val.toFixed(0)
                        }
                    }, `-=${duration}`);
            }
        })
    }
}

// 404 page full height script

const page404 = document.querySelector('#page-404')

if (!!page404) {
    const wh = document.documentElement.clientHeight
    const body = document.body.clientHeight;

    if (wh > body) {
        page404.style.height = `${wh - body + page404.clientHeight}px`
    }
}