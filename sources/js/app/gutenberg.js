import {gsap} from "./gsap/gsap";
import {ScrollTrigger} from "./gsap/ScrollTrigger";
import {CustomEase} from "./gsap/CustomEase";
import {isjQuery} from "./functions";
import Swiper from 'swiper/swiper-bundle';
import "@lottiefiles/lottie-player";

gsap.registerPlugin(ScrollTrigger, CustomEase);

CustomEase.create('bazier1', '.25, .46, .45, .94');

if (window.acf) {
    acf.add_filter('color_picker_args', function (args, field) {
        args.palettes = [
            '#1697F3',
            '#D5ECFD',
            '#8BC53F',
            '#253F53',
            '#FDC554',
            '#F3725F',
            '#FCF1E1'
        ]

        return args;
    })
}

const flexiblePageHeader = block => {
    block = isjQuery(block)
    // let elementDirectly = block.parentNode.querySelector('.CLASS-BLOCK')
    const tl = gsap.timeline({
        ease: 'bazier1'
    })
    const duration = .6;
    const withBigButtons = block.classList.contains('cle-flexible-page-header_with-big-buttons');
    const title = block.querySelector('.cle-flexible-page-header__title')
    const text = block.querySelector('.cle-flexible-page-header__text')
    const btns = block.querySelector('.cle-flexible-page-header__btns')
    const bigBtns = block.querySelector('.cle-flexible-page-header__big-btns')
    const image = block.querySelector('.cle-flexible-page-header__img')
    const titleWithHighlight = title.classList.contains('js-title-with-highlight')

    gsap.to(block, {autoAlpha: 1, ease: 'linear'})

    if (document.documentElement.clientWidth <= 991) {
        imageAnimation()
    }

    if (document.documentElement.clientWidth <= 991) {
        imageWithShapeAnimation()
    }

    if (!!title) {
        tl
            .fromTo(title, { y: 20, opacity: 0 }, { y: 0, opacity: 1, duration: duration })
    }

    if (titleWithHighlight) {
        const highligtedWords = title.querySelectorAll('span')

        if (highligtedWords.length) {
            highligtedWords.forEach(word => {
                gsap.fromTo(word, { backgroundSize: '0% 4px' }, { backgroundSize: '100% 4px', ease: 'bazier1', duration: 1 }, `-=${duration / 2}`)
            })
        }
    }

    if (!!text) {
        tl
            .fromTo(text, { y: 20, opacity: 0 }, { y: 0, opacity: 1, duration: duration }, `-=${duration / 2}`)
    }

    if (withBigButtons) {
        if (!!bigBtns) {
            tl
                .fromTo(bigBtns, { y: 20, opacity: 0 }, { y: 0, opacity: 1, duration: duration }, `-=${duration / 2}`)
        }
    } else {
        if (!!btns) {
            tl
                .fromTo(btns, { y: 20, opacity: 0 }, { y: 0, opacity: 1, duration: duration }, `-=${duration / 2}`)
        }
    }

    if (document.documentElement.clientWidth > 991) {
        imageAnimation()
    }

    if (document.documentElement.clientWidth > 991) {
        imageWithShapeAnimation()
    }

    function imageAnimation() {
        if (!!image) {
            tl
                .fromTo(image, { scale: 0, opacity: 0 }, { scale: 1, opacity: 1, duration: duration }, `-=${duration / 2}`)
        }
    }



    function imageWithShapeAnimation() {
        const imageWithShape = block.querySelector('.cle-flexible-page-header__images')

        if (!!imageWithShape) {
            const poster = imageWithShape.querySelector('.cle-flexible-page-header__images-poster')
            const shapeTL = imageWithShape.querySelector('.cle-flexible-page-header__shape-tl')
            const shapeBR = imageWithShape.querySelector('.cle-flexible-page-header__shape-br')
            const shapeTR = imageWithShape.querySelector('.cle-flexible-page-header__shape-tr')
            const shapeBL = imageWithShape.querySelector('.cle-flexible-page-header__shape-bl')
            const starburst = imageWithShape.querySelector('.cle-flexible-page-header__starburst')
            const imageWithUsualImage = imageWithShape.classList.contains('cle-flexible-page-header__images_with-usual-image')
            const play = imageWithShape.querySelector('.cle-flexible-page-header__images-video')

            if (!!poster) {
                tl
                    .fromTo(poster, { scale: 0, opacity: 0 }, { scale: 1, opacity: 1, duration: duration }, `-=${duration / 2}`)
            }

            if (!!shapeTL && !imageWithUsualImage) {
                tl
                    .fromTo(shapeTL, { scale: 0, opacity: 0 }, { scale: 1, opacity: 1, duration: duration }, `-=${duration / 2}`)
            }

            if (!!shapeBR && !imageWithUsualImage) {
                tl
                    .fromTo(shapeBR, { scale: 0, opacity: 0 }, { scale: 1, opacity: 1, duration: duration }, `-=${duration / 2}`)
            }

            if (!!shapeTR && imageWithUsualImage) {
                tl
                    .fromTo(shapeTR, { scale: 0, opacity: 0 }, { scale: 1, opacity: 1, duration: duration }, `-=${duration / 2}`)
            }

            if (!!shapeBL && imageWithUsualImage) {
                tl
                    .fromTo(shapeBL, { scale: 0, opacity: 0 }, { scale: 1, opacity: 1, duration: duration }, `-=${duration / 2}`)
            }

            if (!!starburst) {
                tl
                    .fromTo(starburst, { scale: 0, opacity: 0 }, { scale: 1, opacity: 1, duration: duration }, `-=${duration / 2}`)
            }

            if (!!play) {
                tl
                    .fromTo(play, { opacity: 0 }, { opacity: 1, duration: duration }, `-=${duration / 2}`)
            }
        }
    }
}

const contentBlocksSection = block => {
    block = isjQuery(block)
    // let elementDirectly = block.parentNode.querySelector('.CLASS-BLOCK')
    const swiper = block.querySelector('.swiper')

    if (!!swiper) {
        const items = block.querySelectorAll('.cle-content-blocks-section__item')
        const v2ModificatorClass = 'cle-content-blocks-section_items-version-2'
        const centeredHeaderModificatorClass = 'cle-content-blocks-section_centered-header'

        new Swiper(swiper, {
            spaceBetween: 27,
            navigation: {
                prevEl: !block.classList.contains(centeredHeaderModificatorClass) ? block.querySelector('.cle-content-blocks-section__prev') : block.querySelector('.cle-content-blocks-section__bottom .cle-content-blocks-section__prev'),
                nextEl: !block.classList.contains(centeredHeaderModificatorClass) ? block.querySelector('.cle-content-blocks-section__next') : block.querySelector('.cle-content-blocks-section__bottom .cle-content-blocks-section__next'),
            },
            breakpoints: {
                200: {
                  slidesPerView: 1,
                  spaceBetween: 5,
                },
                575: {
                    slidesPerView: 1,
                    spaceBetween: 27,
                },
                767: {
                    slidesPerView: 1.5,
                },
                991: {
                    slidesPerView: 2,
                },
                1400: {
                    slidesPerView: 3,
                }
            },
            on: {
                init: swiper => {
                    const slides = swiper.slides.length;
                    const maxSlides = swiper.params.breakpoints[swiper.currentBreakpoint].slidesPerView
                    const navigations = document.querySelectorAll('.cle-content-blocks-section__navigation')

                    if (slides <= maxSlides) {

                        if (navigations.length) {
                            navigations.forEach(item => {
                                item.style.display = 'none'
                            })
                        }
                    }
                },
                resize: swiper => {
                    const slides = swiper.slides.length;
                    const maxSlides = swiper.params.breakpoints[swiper.currentBreakpoint].slidesPerView
                    const navigations = document.querySelectorAll('.cle-content-blocks-section__navigation')

                    if (slides <= maxSlides) {
                        if (navigations.length) {
                            navigations.forEach(item => {
                                item.style.display = 'none'
                            })
                        }
                    } else {
                        if (navigations.length) {
                            navigations.forEach(item => {
                                item.style.display = 'flex'
                            })
                        }
                    }
                }
            }
        })

        if (items.length) {
            items.forEach(item => {
                item.addEventListener('mouseenter', () => {
                    itemHoverHandler(item)
                })

                item.addEventListener('mouseleave', () => {
                    itemHoverOutHandler(item)
                })
            });
        }

        function itemHoverHandler(item) {
            const isV2 = block.classList.contains(v2ModificatorClass)

            if (isV2 && document.documentElement.clientWidth > 1200) {
                const text = item.querySelector('.cle-content-blocks-section__text');

                if (!!text) {
                    text.style.height = `${text.scrollHeight}px`
                }
            }
        }

        function itemHoverOutHandler(item) {
            const isV2 = block.classList.contains(v2ModificatorClass)

            if (isV2 && document.documentElement.clientWidth > 1200) {
                const text = item.querySelector('.cle-content-blocks-section__text');

                if (!!text) {
                    text.style.height = 0
                }
            }
        }
    }
}

const testimonialSlider = block => {
    block = isjQuery(block)
    // let elementDirectly = block.parentNode.querySelector('.CLASS-BLOCK')
    const swiper = block.querySelector('.swiper')

    if (!!swiper) {
        const prev = block.querySelectorAll('.cle-testimonial-slider__prev')
        const next = block.querySelectorAll('.cle-testimonial-slider__next')
        const duration = .6

        const swiperInstance = new Swiper(swiper, {
            autoHeight: true,
            effect: "fade",
            // loop: true,
            // autoplay: {
            //     delay: 6000,
            //     disableOnInteraction: false,
            //     pauseOnMouseEnter: true
            // },
            on: {
                init: swiper => {
                    const slides = swiper.slides.length
                    const navigations = block.querySelectorAll('.cle-testimonial-slider__navigation');
                    const firstSlide = swiper.slides[0]
                    const firstSlideLottie = firstSlide.querySelector('lottie-player')

                    if (slides === 1) {
                        navigations.forEach(navigation => {
                            navigation.style.display = 'none';
                        })
                    } else {
                        prev[0].classList.add('js-arrow-disabled')
                        next[next.length - 1].classList.add('js-arrow-disabled')
                    }

                    if (!!firstSlideLottie) {
                        gsap.to(block, { scrollTrigger: {
                            trigger: block,
                            onEnter: (progress, direction, isActive) => {
                                playLottieAniamtion(firstSlideLottie)
                            }
                        }, onComplete: () => {
                            playLottieAniamtion(firstSlideLottie)
                        } })
                    }
                }
            }
        })

        if (prev.length) {
            prev.forEach(item => {
                item.addEventListener('click', () => {
                    swiperInstance.slidePrev()
                })
            })
        }

        if (next.length) {
            next.forEach((item, index) => {
                item.addEventListener('click', () => {
                    swiperInstance.slideNext()
                })
            })
        }

        swiperInstance.on('slideChange', swiper => {
            const currentSlide = swiper.slides[swiper.realIndex];
            const lottie = currentSlide.querySelector('lottie-player')

            playLottieAniamtion(lottie)
        })

        function playLottieAniamtion(item) {
            if (!!item && item.tagName === 'LOTTIE-PLAYER') {
                item.play();
            }
        }
    }
}

const appSliderSection = block => {
    block = isjQuery(block)
    // let elementDirectly = block.parentNode.querySelector('.CLASS-BLOCK')
    const swiper = block.querySelector('.swiper')

    if (!!swiper) {
        const items = block.querySelectorAll('.cle-app-slider__item')

        new Swiper(swiper, {
            spaceBetween: 27,
            navigation: {
                prevEl: block.querySelector('.cle-app-slider__prev'),
                nextEl: block.querySelector('.cle-app-slider__next'),
            },
            breakpoints: {
                200: {
                    slidesPerView: 1,
                    spaceBetween: 5,
                },
                575: {
                    slidesPerView: 1,
                    spaceBetween: 27,
                },
                767: {
                    slidesPerView: 1.5,
                },
                991: {
                    slidesPerView: 2,
                },
                1400: {
                    slidesPerView: 3.2,
                }
            },
            on: {
                init: swiper => {
                    const slides = swiper.slides.length;
                    const maxSlides = swiper.params.breakpoints[swiper.currentBreakpoint].slidesPerView
                    const navigations = document.querySelectorAll('.cle-app-slider__navigation')

                    if (slides <= maxSlides) {

                        if (navigations.length) {
                            navigations.forEach(item => {
                                item.style.display = 'none'
                            })
                        }
                    }
                },
                resize: swiper => {
                    const slides = swiper.slides.length;
                    const maxSlides = swiper.params.breakpoints[swiper.currentBreakpoint].slidesPerView
                    const navigations = document.querySelectorAll('.cle-app-slider__navigation')

                    if (slides <= maxSlides) {
                        if (navigations.length) {
                            navigations.forEach(item => {
                                item.style.display = 'none'
                            })
                        }
                    } else {
                        if (navigations.length) {
                            navigations.forEach(item => {
                                item.style.display = 'flex'
                            })
                        }
                    }
                }
            }
        })

        if (items.length) {
            items.forEach(item => {
                item.addEventListener('mouseenter', () => {
                    itemHoverHandler(item)
                })

                item.addEventListener('mouseleave', () => {
                    itemHoverOutHandler(item)
                })
            });
        }

        function itemHoverHandler(item) {
            const isV2 = block.classList.contains(v2ModificatorClass)

            if (isV2 && document.documentElement.clientWidth > 1200) {
                const text = item.querySelector('.cle-content-blocks-section__text');

                if (!!text) {
                    text.style.height = `${text.scrollHeight}px`
                }
            }
        }

        function itemHoverOutHandler(item) {
            const isV2 = block.classList.contains(v2ModificatorClass)

            if (isV2 && document.documentElement.clientWidth > 1200) {
                const text = item.querySelector('.cle-content-blocks-section__text');

                if (!!text) {
                    text.style.height = 0
                }
            }
        }
    }
}

const logoSection = block => {
    block = isjQuery(block)
    // let elementDirectly = block.parentNode.querySelector('.CLASS-BLOCK')
    const swiper = block.querySelector('.swiper')

    if (!!swiper) {
        const items = block.querySelectorAll('.cle-logo-section__item')

        new Swiper(swiper, {
            observer: true,
            observeParents: true,
            // slidesPerView: "auto",
            spaceBetween: 27,
            speed: 6000,
            autoHeight: false,
            loop: true,
            allowTouchMove: false,
            slidesPerView: 5.5,
            autoplay: {
                delay: 1,
                disableOnInteraction: false,
            },
            breakpoints: {
                200: {
                    slidesPerView: 1,
                    spaceBetween: 5,
                },
                575: {
                    slidesPerView: 1,
                    spaceBetween: 27,
                },
                767: {
                    slidesPerView: 1.5,
                },
                991: {
                    slidesPerView: 2,
                },
                1400: {
                    slidesPerView: 3.2,
                }
            },
        })
    }
}

const brandCTASection = block => {
    block = isjQuery(block)
    // let elementDirectly = block.parentNode.querySelector('.CLASS-BLOCK')
    const header = document.querySelector('.cle-header');
    const bg = block.querySelector('.cle-brand-cta-section__bg');
    const lottie = block.querySelector('.cle-brand-cta-section__animation lottie-player');

    const tl = gsap.timeline({
        scrollTrigger: {
            trigger: block,
            scrub: .5,
            start: !!header ? `top-=${header.offsetHeight}` : 'top top',
            pin: true,
        }
    })

    tl.add('start')

    if (!!bg) {
        tl
            .to(bg, { scaleX: 2, borderRadius: 0, transformOrigin:'bottom center' }, 'start')
    }

    if (!!lottie) {
        tl
            .fromTo(lottie, { scale: 0.5 }, { scale: 1, onStart: () => {
                lottie.play()
            } }, 'start')
    }
}

const imageAndContentSection = block => {
    block = isjQuery(block)
    // let elementDirectly = block.parentNode.querySelector('.CLASS-BLOCK')
    const tl = gsap.timeline({
        ease: 'bazier1',
        scrollTrigger: {
            trigger: block
        }
    })
    const image = block.querySelector('.cle-image-and-content__image-main')
    const shapes = block.querySelector('.cle-image-and-content__image-el')
    const duration = .6;

    if (!!image) {
        tl
            .fromTo(image, { opacity: 0, scale: 0 }, { opacity: 1, scale: 1, duration: duration })
    }

    if (!!shapes) {
        tl
            .fromTo(shapes, { opacity: 0, scale: 0 }, { opacity: 1, scale: 1, duration: duration }, `-=${duration / 2}`)
    }
}

const pricingTabsSection = block => {
    block = isjQuery(block)
    // let elementDirectly = block.parentNode.querySelector('.CLASS-BLOCK')
    const togglers = block.querySelectorAll('.cle-pricing-tabs-section__toggler-item')
    const tabs = block.querySelectorAll('.cle-pricing-tabs-section__tab')
    const accordionTitles = block.querySelectorAll('.cle-pricing-tabs-section__accordion-title')
    const expandableRows = block.querySelectorAll('.cle-pricing-tabs-section__row_expandable')

    if (togglers.length) {
        togglers.forEach(toggler => {
            toggler.addEventListener('click', () => {
                const anchor = toggler.dataset.tab;

                if (tabs.length) {
                    tabs.forEach(tab => {
                        // console.log(tab.dataset.tab)
                        if (tab.dataset.tab == anchor) {
                            tab.classList.add('cle-pricing-tabs-section__tab_active')
                        } else {
                            tab.classList.remove('cle-pricing-tabs-section__tab_active')
                        }
                    })
                }

                ScrollTrigger.refresh(true)
            })
        })
    }


    if (accordionTitles.length) {
        accordionTitles.forEach(title => {
            title.addEventListener('click', () => {
                const parent = title.closest('.cle-pricing-tabs-section__accordion-item');

                if (!!parent) {
                    const content = parent.querySelector('.cle-pricing-tabs-section__accordion-content');

                    if (!!content) {
                        parent.classList.toggle('js-pricing-accordion-active');

                        if (parent.classList.contains('js-pricing-accordion-active')) {
                            content.style.height = `${content.scrollHeight}px`
                        } else {
                            content.style.height = ''
                        }
                    }

                    ScrollTrigger.refresh(true)
                }
            })
        })
    }

    if (expandableRows.length) {
        expandableRows.forEach(row => {
            const initRowHeight = row.offsetHeight;

            row.addEventListener('click', () => {
                if (document.documentElement.clientWidth > 991) {
                    const end = row.querySelector('.cle-pricing-tabs-section__row-end')
                    const content = row.closest('.cle-pricing-tabs-section__accordion-content')

                    row.classList.toggle('js-expandable-open');

                    if (row.classList.contains('js-expandable-open')) {
                        if (!!end) {
                            const rowItems = end.querySelectorAll('div')

                            rowItems.forEach(item => {
                                item.style.height = `${item.scrollHeight}px`

                                if (!!content) {
                                    content.style.height = `${content.scrollHeight + item.scrollHeight - initRowHeight}px`
                                }
                            })
                        }
                    } else {
                        if (!!end) {
                            const rowItems = end.querySelectorAll('div')

                            rowItems.forEach(item => {
                                item.style.height = ''

                                if (!!content) {
                                    content.style.height = `${content.scrollHeight - item.scrollHeight + initRowHeight}px`
                                }
                            })
                        }
                    }

                    ScrollTrigger.refresh(true)
                }
            })
        })
    }
}

const selectWithText = block => {
    block = isjQuery(block)
    // let elementDirectly = block.parentNode.querySelector('.CLASS-BLOCK')
    const select = block.querySelector('.cle-select-with-text__select-wr')

    if (!!select) {
        select.addEventListener('change', () => {
            const value = select.value;
            const tab = block.querySelector(`[data-tab=${value}]`);
            const tabs = block.querySelectorAll('.cle-select-with-text__tab')

            if (!!tab && tab.dataset.tab === value) {
                if (tabs.length) {
                    tabs.forEach(tab => {
                        tab.classList.remove('cle-select-with-text__tab_active')
                    })
                }

                tab.classList.add('cle-select-with-text__tab_active')
                ScrollTrigger.refresh(true)
            }
        })
    }
}

const tabsWithText = block => {
    block = isjQuery(block)
    // let elementDirectly = block.parentNode.querySelector('.CLASS-BLOCK')

    const tabs = block.querySelectorAll('.cle-tabs-with-text__tab')
    const items = block.querySelectorAll('.cle-tabs-with-text__item')

    if (tabs.length && items.length) {
        tabs.forEach(tab => {
            tab.addEventListener('click', () => {
                const tabLabel = tab.dataset.tab;
                const activeItem = block.querySelector(`.cle-tabs-with-text__content [data-tab=${tabLabel}]`);

                tabs.forEach(tab => {
                    tab.classList.remove('cle-tabs-with-text__tab_active')
                })

                items.forEach(item => {
                    item.classList.remove('cle-tabs-with-text__item_active')
                })

                tab.classList.add('cle-tabs-with-text__tab_active')

                if (!!tabLabel) {
                    activeItem.classList.add('cle-tabs-with-text__item_active')
                }
                
                ScrollTrigger.refresh(true)
            })
        })
    }
}


if (window.acf) {
    window.acf.addAction('render_block_preview/type=flexible-page-header', flexiblePageHeader)
    window.acf.addAction('render_block_preview/type=content-blocks-section', contentBlocksSection)
    window.acf.addAction('render_block_preview/type=testimonial-slider', testimonialSlider)
    window.acf.addAction('render_block_preview/type=brand-cta-section', brandCTASection)
    window.acf.addAction('render_block_preview/type=image-and-content-section', imageAndContentSection)
    window.acf.addAction('render_block_preview/type=app-slider', appSliderSection)
    window.acf.addAction('render_block_preview/type=pricing-tabs-section', pricingTabsSection)
    window.acf.addAction('render_block_preview/type=logo-section', logoSection)
    window.acf.addAction('render_block_preview/type=select-with-text', selectWithText)
    window.acf.addAction('render_block_preview/type=tabs-with-text', tabsWithText)
} else {
    document.querySelectorAll('.cle-flexible-page-header').forEach(flexiblePageHeader)
    document.querySelectorAll('.cle-content-blocks-section').forEach(contentBlocksSection)
    document.querySelectorAll('.cle-testimonial-slider').forEach(testimonialSlider)
    document.querySelectorAll('.cle-brand-cta-section').forEach(brandCTASection)
    document.querySelectorAll('.cle-image-and-content').forEach(imageAndContentSection)
    document.querySelectorAll('.cle-app-slider').forEach(appSliderSection)
    document.querySelectorAll('.cle-pricing-tabs-section').forEach(pricingTabsSection)
    document.querySelectorAll('.cle-logo-section').forEach(logoSection)
    document.querySelectorAll('.cle-select-with-text').forEach(selectWithText)
    document.querySelectorAll('.cle-tabs-with-text').forEach(tabsWithText)
}
