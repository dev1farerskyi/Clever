<?php
/**
 * The file includes necessary functions for theme.
 *
 * @package villanova
 * @since 1.0
 */

if (!defined('V_PREFIX')) define('V_PREFIX', 'villanova');
if (!defined('V_TEMP_PATH')) define('V_TEMP_PATH', get_template_directory());
if (!defined('V_TEMP_URL')) define('V_TEMP_URL', get_template_directory_uri());

require_once get_theme_file_path( '/inc/action-config.php' );
require_once get_theme_file_path( '/inc/helper-functions.php' );
require_once get_theme_file_path( '/inc/menu-functions.php' );
require_once get_theme_file_path( '/inc/init-gutenberg.php' );
require_once get_theme_file_path( '/inc/custom-post-types.php' );
require_once get_theme_file_path( '/inc/applications.php' );

// Register ACF Gravity Forms field
add_action( 'init', function () {
    if ( class_exists( 'ACF' ) ) {
        require_once get_stylesheet_directory() . '/inc/class-acf-field-gravity-v5.php';
        require_once get_stylesheet_directory() . '/inc/nav-menu-field.php';
    }
} );

function cle_after_theme_setup() {

    register_nav_menus(
        array(
            'header-menu' => esc_html__( 'Header menu', V_PREFIX ),
            // 'help-menu' => esc_html__( 'Help menu', V_PREFIX ),
            'helpful-links-menu' => esc_html__( 'Helpful Links menu', V_PREFIX ),
            'footer-menu-1' => esc_html__( 'Footer menu 1', V_PREFIX ),
            'footer-menu-2' => esc_html__( 'Footer menu 2', V_PREFIX ),
            'footer-menu-3' => esc_html__( 'Footer menu 3', V_PREFIX ),
            'footer-menu-4' => esc_html__( 'Footer menu 4', V_PREFIX ),
            'footer-menu-5' => esc_html__( 'Footer menu 5', V_PREFIX ),
            'footer-menu-6' => esc_html__( 'Footer menu 6', V_PREFIX ),
            'footer-menu-7' => esc_html__( 'Footer menu 7', V_PREFIX ),
            'footer-menu-8' => esc_html__( 'Footer menu 8', V_PREFIX ),
            'footer-menu-9' => esc_html__( 'Footer menu 9', V_PREFIX ),
            'copyright-menu' => esc_html__( 'Copyright menu', V_PREFIX ),
        )
    );

    add_theme_support( 'custom-header' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'title-tag' );

    // Editor styles.
    add_theme_support('editor-styles');

    $editor_styles = array(
        'assets/css/style-editor.css'
    );
    add_editor_style($editor_styles);
}

function cc_mime_types($mimes) {
    $mimes['json'] = 'application/json';
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
    }

add_filter('upload_mimes', 'cc_mime_types');
add_action( 'after_setup_theme', 'cle_after_theme_setup' );
