<?php
/**
 * 404 Page template.
 *
 * @package villanova
 * @since 1.0.0
 *
 */

get_header(); ?>
<div class="cle-simple-text-header cle-section" id="page-404">
    <img class="cle-decor-circle-1" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/decor-circle-1.svg" alt="">
    <img class="cle-decor-circle-2" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/decor-circle-2.svg" alt="">
    <div class="container">
        <div class="cle-simple-text-header__wrap cle-text-center @@contSmall">
            <h1 class="cle-simple-text-header__title js-title-with-highlight">404 Page not found</h1>
            <p class="cle-simple-text-header__description">Something went wrong. We can’t seem to find the page you’re looking for. </p>
            <a class="cle-btn cle-btn_primary" href="/">
                Back to home
                <?php get_template_part('template-parts/elements/icon-right-white'); ?>
            </a>
        </div>
    </div>
</div>


<?php get_footer(); ?>
