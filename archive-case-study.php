<?php
/**
 * Case Study archive template file
 */

get_header();

$one_post_post_item = get_field('cs_one_post_post_item', 'options');
$title_quick_links_section = get_field('cs_title_quick_links_section', 'options');
$links_quick_links_section = get_field('cs_links_quick_links_section', 'options');
$links_quick_white_background = get_field('cs_links_quick_white_background', 'options');
?>

<?php get_template_part(
    'template-parts/blocks/one-post',
    null,
    array(
        'items' => $one_post_post_item,
    )
); ?>

<div class="cle-cpt-blocks-section cle-section cle-section-element-pad decor-top-left cle-back-blue">
    <?php get_template_part('template-parts/elements/posts-with-filter',
        null,
        array(
            'posts_count' => 6,
            'choose_post_types' => array( 'case-study', 'events',),
        )
    ); ?>
</div>

<?php get_template_part(
    'template-parts/blocks/quick-links-section',
    null,
    array(
        'title' => $title_quick_links_section,
        'links' => $links_quick_links_section,
        'white_background' => $links_quick_white_background,
    )
);

get_footer();


